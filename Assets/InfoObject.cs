﻿using UnityEngine;
using System.Collections;
using Base;

public class InfoObject : MonoBehaviour {

	public UILabel CardName;
	
	public UILabel CardCost;
	public UILabel CardGraveCost;
	
	public UILabel CardAp;
	public UILabel CardHp;
	
	public UIButton Frame;
	public UI2DSprite Image;
	public UI2DSprite ManaOrb;
	public UI2DSprite GraveOrb;
	
	public FieldObject PlayerField;
	public Card PlayerHand;
	public UILabel CardText;
	public float RemainTime = 2.0f;

	public GameObject Cover;

	
	public void LabelChange()
	{
		if (PlayerHand != null) 
		{
			if ( PlayerHand.CardType == CardTypes.monster )
			{
				if (CardAp.text != PlayerHand.LinkedFieldObject.CurrentAp.ToString())
				{
					CardAp.text = PlayerHand.LinkedFieldObject.CurrentAp.ToString();
				}
				if (CardHp.text != PlayerHand.LinkedFieldObject.CurrentHp.ToString())
				{
					CardHp.text = PlayerHand.LinkedFieldObject.CurrentHp.ToString();
				}
			}
			else if ( PlayerHand.CardType == CardTypes.magic )
			{
				CardAp.text = "";
				CardHp.text = "";
			}
			if ( CardGraveCost.text != PlayerHand.CurrentGraveCost.ToString() )
			{
				if ( PlayerHand.Recyclable == false ) 
				{
					CardGraveCost.fontSize = 40;
					CardGraveCost.text = "∞";
				}
				else
				{
					CardGraveCost.fontSize = 25;
					CardGraveCost.text = PlayerHand.CurrentGraveCost.ToString();
				}
			}
			if ( CardCost.text != PlayerHand.CurrentCost.ToString() )
			{
				CardCost.text = PlayerHand.CurrentCost.ToString();
			}
			if ( CardName.text != PlayerHand.Name )
			{
				CardName.text = PlayerHand.Name;
			}
			if ( PlayerHand.Text == null )
			{
				
			}
			else if ( CardText.text != PlayerHand.Text )
			{
				CardText.text = PlayerHand.Text;
			}
			if ( PlayerHand.CardType != CardTypes.empty )
			{
				CardAp.gameObject.SetActive( true );
				CardHp.gameObject.SetActive( true );
				CardGraveCost.gameObject.SetActive( true );
				CardCost.gameObject.SetActive( true );
				CardName.gameObject.SetActive( true );
				ManaOrb.gameObject.SetActive(true);
				GraveOrb.gameObject.SetActive(true);
				Cover.SetActive ( false );
			}
			if ( PlayerHand.CardType == CardTypes.empty || PlayerHand.CardType == CardTypes.mystery )
			{
				CardAp.gameObject.SetActive( false );
				CardHp.gameObject.SetActive( false );
				CardGraveCost.gameObject.SetActive( false );
				CardCost.gameObject.SetActive( false );
				CardName.gameObject.SetActive( false );
				ManaOrb.gameObject.SetActive(false);
				GraveOrb.gameObject.SetActive(false);
				if ( PlayerHand.CardType == CardTypes.mystery )
				{
					Cover.SetActive( true );
				}
			}
		}
		if (PlayerField != null) 
		{
			if ( CardAp.text != PlayerField.CurrentAp.ToString() )
			{
				CardAp.text = PlayerField.CurrentAp.ToString();
			}
			if ( CardHp.text != PlayerField.CurrentHp.ToString() )
			{
				CardHp.text = PlayerField.CurrentHp.ToString();
			}
			if ( CardGraveCost.text != PlayerField.CurrentGraveCost.ToString() )
			{
				if ( PlayerField.Recyclable == false )
				{
					CardGraveCost.fontSize = 40;
					CardGraveCost.text = "∞";
				}
				else
				{
					CardGraveCost.fontSize = 25;
					CardGraveCost.text = PlayerField.CurrentGraveCost.ToString();
				}
			}
			if ( CardCost.text != PlayerField.CurrentCost.ToString() )
			{
				CardCost.text = PlayerField.CurrentCost.ToString();
			}
			if ( CardName.text != PlayerField.Name )
			{
				CardName.text = PlayerField.Name;
			}
			if ( PlayerField.Text == null )
			{

			}
			else if ( CardText.text != PlayerField.Text )
			{
                if (PlayerField.Text == "" && PlayerField.LinkedCard != null) PlayerField.Text = PlayerField.LinkedCard.Text;
				CardText.text = PlayerField.Text;
			}
			if ( PlayerField.CardType != CardTypes.empty && ( PlayerField.CardType != CardTypes.mystery || ( PlayerField.CardType == CardTypes.mystery && PlayerField.Controller == Base.GameAction.MainSystem.MePlayer ) ) )
			{
				CardAp.gameObject.SetActive( true );
				CardHp.gameObject.SetActive( true );
				CardGraveCost.gameObject.SetActive( true );
				CardCost.gameObject.SetActive( true );
				CardName.gameObject.SetActive( true );
				ManaOrb.gameObject.SetActive(true);
				GraveOrb.gameObject.SetActive(true);
				Cover.SetActive ( false );
			}
			if ( PlayerField.CardType == CardTypes.empty || ( PlayerField.CardType == CardTypes.mystery && PlayerField.Controller != Base.GameAction.MainSystem.MePlayer)  )
			{
				CardAp.gameObject.SetActive( false );
				CardHp.gameObject.SetActive( false );
				CardGraveCost.gameObject.SetActive( false );
				CardCost.gameObject.SetActive( false );
				CardName.gameObject.SetActive( false );
				ManaOrb.gameObject.SetActive(false);
				GraveOrb.gameObject.SetActive(false);
				if ( PlayerField.CardType == CardTypes.mystery && PlayerField.Controller != Base.GameAction.MainSystem.MePlayer )
				{
					Cover.SetActive ( true );
				}
			}
		}
	}
	
	
	public void ImageChange()
	{
		UnityEngine.Sprite TempSprite;
		if (PlayerHand != null) 
		{
			string ImagePath = PlayerHand.ImagePath;
			if ( ImagePath != Image.name )
			{
				TempSprite = Resources.Load<Sprite> ("Cards/" + ImagePath);
				if ( TempSprite == null )
				{
					TempSprite = Resources.Load<Sprite>("Cards/dummy");
				}
				Image.sprite2D = TempSprite;
			}
			if ( PlayerHand.CardType == CardTypes.empty )
			{
				TempSprite = Resources.Load<Sprite> ("Cards/empty");
				if ( TempSprite == null )
				{
					TempSprite = Resources.Load<Sprite>("Cards/dummy");
				}
				var but = Frame.GetComponent<UIButton>();
				but.normalSprite2D = TempSprite;
				but.hoverSprite2D = TempSprite;
				but.pressedSprite2D = TempSprite;
				but.disabledSprite2D = TempSprite;
			}
			else if ( PlayerHand.CardType == CardTypes.magic )
			{
				if ( PlayerHand.Recyclable == true )
				{
					TempSprite = Resources.Load<Sprite> ("Cards/LightMagic");
					var but = Frame.GetComponent<UIButton>();
					but.normalSprite2D = TempSprite;
					but.hoverSprite2D = TempSprite;
					but.pressedSprite2D = TempSprite;
					but.disabledSprite2D = TempSprite;
				}
				if ( PlayerHand.Recyclable == false )
				{
					TempSprite = Resources.Load<Sprite> ("Cards/GoldMagic");
					var but = Frame.GetComponent<UIButton>();
					but.normalSprite2D = TempSprite;
					but.hoverSprite2D = TempSprite;
					but.pressedSprite2D = TempSprite;
					but.disabledSprite2D = TempSprite;
				}
			}
			else
			{
				if ( PlayerHand.Recyclable == true )
				{
					TempSprite = Resources.Load<Sprite> ("Cards/Light");
					var but = Frame.GetComponent<UIButton>();
					but.normalSprite2D = TempSprite;
					but.hoverSprite2D = TempSprite;
					but.pressedSprite2D = TempSprite;
					but.disabledSprite2D = TempSprite;
				}
				if ( PlayerHand.Recyclable == false )
				{
					TempSprite = Resources.Load<Sprite> ("Cards/Gold");
					var but = Frame.GetComponent<UIButton>();
					but.normalSprite2D = TempSprite;
					but.hoverSprite2D = TempSprite;
					but.pressedSprite2D = TempSprite;
					but.disabledSprite2D = TempSprite;
				}
			}
		}
		if (PlayerField != null) 
		{
			if ( PlayerField.CardType == CardTypes.mystery && PlayerField.Controller != GameAction.MainSystem.MePlayer )
			{
				return;
			}
			string ImagePath = PlayerField.ImagePath;
			if ( ImagePath != Image.name )
			{
				TempSprite = Resources.Load<Sprite> ("Cards/" + ImagePath);
				if ( TempSprite == null )
				{
					TempSprite = Resources.Load<Sprite>("Cards/dummy");
				}
				Image.sprite2D = TempSprite;
			}
			/*if ( ImagePath != Image.normalSprite2D.name )
			{
				TempSprite = Resources.Load<Sprite> ("Cards/" + ImagePath);
				if ( TempSprite == null )
				{
					TempSprite = Resources.Load<Sprite>("Cards/dummy");
				}
				var but = Image.GetComponent<UIButton>();
				but.normalSprite2D = TempSprite;
				but.hoverSprite2D = TempSprite;
				but.pressedSprite2D = TempSprite;
				but.disabledSprite2D = TempSprite;
				return;
			}*/
			if ( PlayerField.CardType == CardTypes.empty )
			{
				TempSprite = Resources.Load<Sprite> ("Cards/empty");
				if ( TempSprite == null )
				{
					TempSprite = Resources.Load<Sprite>("Cards/dummy");
				}
				var but = Frame.GetComponent<UIButton>();
				but.normalSprite2D = TempSprite;
				but.hoverSprite2D = TempSprite;
				but.pressedSprite2D = TempSprite;
				but.disabledSprite2D = TempSprite;
			}
			else if ( PlayerField.CardType == CardTypes.magic )
			{
				if ( PlayerField.Recyclable == true )
				{
					TempSprite = Resources.Load<Sprite> ("Cards/LightMagic");
					var but = Frame.GetComponent<UIButton>();
					but.normalSprite2D = TempSprite;
					but.hoverSprite2D = TempSprite;
					but.pressedSprite2D = TempSprite;
					but.disabledSprite2D = TempSprite;
				}
				if ( PlayerField.Recyclable == false )
				{
					TempSprite = Resources.Load<Sprite> ("Cards/GoldMagic");
					var but = Frame.GetComponent<UIButton>();
					but.normalSprite2D = TempSprite;
					but.hoverSprite2D = TempSprite;
					but.pressedSprite2D = TempSprite;
					but.disabledSprite2D = TempSprite;
				}
			}
			else
			{
				if ( PlayerField.Recyclable == true )
				{
					TempSprite = Resources.Load<Sprite> ("Cards/Light");
					var but = Frame.GetComponent<UIButton>();
					but.normalSprite2D = TempSprite;
					but.hoverSprite2D = TempSprite;
					but.pressedSprite2D = TempSprite;
					but.disabledSprite2D = TempSprite;
				}
				if ( PlayerField.Recyclable == false )
				{
					TempSprite = Resources.Load<Sprite> ("Cards/Gold");
					var but = Frame.GetComponent<UIButton>();
					but.normalSprite2D = TempSprite;
					but.hoverSprite2D = TempSprite;
					but.pressedSprite2D = TempSprite;
					but.disabledSprite2D = TempSprite;
				}
			}
		}
	}
	
	
	void Update()
	{
		if ( PlayerField != null ) 
		{
			if ( PlayerField.CardType == CardTypes.player )
			{
				PlayerField = new EmptyField();
			}
		}
		ImageChange ();
		LabelChange ();
	}
}
