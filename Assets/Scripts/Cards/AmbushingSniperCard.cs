using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class AmbushingSniperCard : Card
	{

        public AmbushingSniperCard()
            : base(4, 2, "매복한 저격수", CardTypes.monster, new AmbushingSniperField(),"AmbushingSniper", "자신을 제외한 체력이 6 이하인 하수인이 소환되면 그것을 파괴한다.") 
		{ 

		}


	}
}
