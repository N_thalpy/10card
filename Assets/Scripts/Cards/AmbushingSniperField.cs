using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class AmbushingSniperField : FieldObject
	{

		public AmbushingSniperField(): base(4, 2, CardTypes.monster, "AmbushingSniper", "매복한 저격수", 7, 3) 
		{
            Summon += (Unit, Me) =>
            {
				if(Me.CardType != CardTypes.mystery)
				{
					if (GameAction.IsFieldObjectMonster(Unit) && Unit != Me && Unit.CurrentHp <= 6)
					{
						float ExplosionTime = Effect.EFC.EffectLastTime();
						Effect ExplosionEffect = new Effect( 1.0f, ExplosionTime , Unit,Unit,"Explosion" );
						Effect.AddEffect( ExplosionEffect );
						GameAction.DestroyFieldObject(Unit);
					}
				}
            };
		}
	}
}
