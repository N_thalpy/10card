using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class AncestorsSpiritCard : Card
	{

        public AncestorsSpiritCard()
            : base(7, 4, "선조의 영혼", CardTypes.monster, new AncestorsSpiritField(),"AncestorsSpirit","전투의 함성: 아군 하수인 하나에게 +3/+3을 부여한다.") 
		{ 

		}
	}
}
