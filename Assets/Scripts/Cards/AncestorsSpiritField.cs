using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class AncestorsSpiritField : FieldObject
	{

		public AncestorsSpiritField(): base(7, 4, CardTypes.monster, "AncestorsSpirit", "선조의 영혼", 9, 13) 
		{
            BattleCryTargetValidityCheck += AncestorsSpiritTargetValidityCheck;    
            TargetBattleCry = (Target, thisObject) =>
                {
                    if (GameAction.IsFieldObjectOnField(Target))
					{					
						Effect HappyEffect = new Effect( 1.0f, Effect.EFC.EffectLastTime() , Target, Target, "Holy" );
						Effect.AddEffect( HappyEffect );
						new StatBuff(Target, 3, 3).Initialize();
					}
				};
		}

        public bool AncestorsSpiritTargetValidityCheck(FieldObject Target, FieldObject thisObject)
        {
            return (Target.CardType == CardTypes.monster || GameAction.IsFieldObjectMonster(Target) && Target.Controller == Controller);
        }

	}
}
