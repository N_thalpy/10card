using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class AngelleadingsoldierCard : Card
	{

        public AngelleadingsoldierCard()
            : base(4, 0, "이끄는 천사", CardTypes.monster, new AngelleadingsoldierField(), "Angelleadingsoldier", "전투의 함성: 하수인 하나에게 +4/+4를 준다. 이 하수인이 죽을 때, 무작위 아군 하수인 하나에게 +4/+4를 준다.", false, false) 
		{ 

		}


	}
}
