using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class AngelleadingsoldierField : FieldObject
	{

		public AngelleadingsoldierField(): base(4, 0, CardTypes.monster, "Angelleadingsoldier", "이끄는 천사", 7, 10, false, "", false) 
		{ 
			TargetBattleCry += AtHpBuffCry;
			Death += AtHpBuffDeath;
            BattleCryTargetValidityCheck += (target, thisObject) =>
                {
                    return VChecks.IsMonsterNotMyetery(target);
                };
		}

		public void AtHpBuffCry(FieldObject target, FieldObject Me)
		{
			StatBuff AHBuff = new StatBuff (target, 4, 4);
			Effect HappyEffect = new Effect( 1.0f, Effect.EFC.EffectLastTime() , Me, Me, "Holy" );
			Effect.AddEffect( HappyEffect );
			AHBuff.Initialize ();
		}

		public void AtHpBuffDeath(FieldObject Unit, FieldObject Me)
		{
			if (Unit == Me)
			{
				List<FieldObject> AllMonsterList = new List<FieldObject>();
				int RndNum = 0;

				for(int i = 0; i < Me.Controller.Field.Count; i++)
				{
					if(Me.Controller.Field[i].CardType == CardTypes.monster)
						AllMonsterList.Add(Me.Controller.Field[i]);
				}

				if(AllMonsterList.Count > 0)
				{
					RndNum = GameAction.RandomInt(0,AllMonsterList.Count);
					FieldObject target = AllMonsterList[RndNum];
					StatBuff AHBuff = new StatBuff (target, 4, 4);
					Effect HappyEffect = new Effect( 1.0f, Effect.EFC.EffectLastTime() , target, target, "Holy" );
					Effect.AddEffect( HappyEffect );
					AHBuff.Initialize ();
				}
			}
		}


	}
}
