using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class AngelsHornCard : Card
	{
		public AngelsHornCard()
			: base(2, 2, "천사의 뿔피리", CardTypes.magic, new MagicBox(), "AngelsHorn", "아군 하수인 전체에게 +2/+2를 부여한다. 무덤에 가면서 이 카드를 악마의 뿔피리 카드로 바꾼다.")
		{
			NonTargetEvent = (thisCard) =>
			{
				thisCard.Recyclable = false;

				float effectTime = Effect.EFC.EffectLastTime();
				for (int i = 0; i < 5; i++)
				{
					FieldObject fo = thisCard.Controller.Field[i];
					if (fo.CardType == CardTypes.monster)
					{
						Buff AllStBuff = new StatBuff(fo, 2, 2);
						Effect HappyEffect = new Effect( 1.0f, effectTime , fo, fo, "Holy" );
						Effect.AddEffect( HappyEffect );
						AllStBuff.Initialize();
					}
				}
				
				Card newCard = new DemonsHornCard ();
				newCard.Controller = thisCard.Controller;
				newCard.Opponent = thisCard.Opponent;
				newCard.LinkedFieldObject.Controller = thisCard.Controller;
				newCard.LinkedFieldObject.Opponent = thisCard.Opponent;
				
				thisCard.Controller.Grave.Add(newCard);
			};
		}
	}
}
