using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ArchangelofHealCard : Card
	{

        public ArchangelofHealCard()
            : base(7, 3, "치유의 대천사", CardTypes.monster, new ArchangelofHealField(),"ArchangelofHeal", "매 턴 종료마다 내 체력을 4 회복시킨다.") 
		{ 

		}


	}
}
