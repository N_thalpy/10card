using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ArchangelofHealField : FieldObject
	{

		public ArchangelofHealField(): base(7, 3, CardTypes.monster, "ArchangelofHeal", "치유의 대천사", 9, 14) 
		{
            TurnEnd += (meObject) =>
			{
				int changeHp = Math.Min(meObject.Controller.CurrentHp + 4, meObject.Controller.CurrentMaxHp);
			//	if (meObject.Controller.CurrentHp != changeHp)
				{
					meObject.Controller.CurrentHp = changeHp;
					Effect HappyEffect = new Effect( 5.0f, Effect.EFC.EffectLastTime() , meObject, meObject, "Heal" );
					Effect.AddEffect( HappyEffect );
				}
			};
		}

	}
}
