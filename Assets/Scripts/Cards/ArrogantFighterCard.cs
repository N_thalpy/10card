using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ArrogantFighterCard : Card
	{

        public ArrogantFighterCard()
            : base(4, 4, "오만한 투사", CardTypes.monster, new ArrogantFighterField(), "ArrogantFighter","이 하수인은 영웅 및 공격력 7 이하의 하수인을 공격할 수 없다.") 
		{ 

		}


	}
}
