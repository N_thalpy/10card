using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ArrogantFighterField : FieldObject
	{

		public ArrogantFighterField(): base(4, 4, CardTypes.monster, "ArrogantFighter", "오만한 투사", 8, 13) 
		{ 
			AttackValidityCheck += ArrogantFighterAttackValidityCheck;
		}


        public bool ArrogantFighterAttackValidityCheck(FieldObject Target, FieldObject thisObject)
        {
            return !(Target.CardType == CardTypes.player || (Target.CardType == CardTypes.monster && Target.CurrentAp <= 7));
        }

	}
}
