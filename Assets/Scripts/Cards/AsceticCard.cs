using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class AsceticCard : Card
	{

        public AsceticCard()
            : base(1, 1, "수행자", CardTypes.monster, new AsceticField(),"Ascetic", "내 턴 시작에 5/7 달인으로 변신한다.") 
		{ 

		}


	}
}
