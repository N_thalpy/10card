using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class AsceticField : FieldObject
	{

		public AsceticField(): base(1, 1, CardTypes.monster, "Ascetic", "수행자", 1, 2) 
		{
            TurnStart += BecomeAMaster;
		}

        void BecomeAMaster(FieldObject me)
        {
			if (me.CardType != CardTypes.mystery) 
			{
				if (me.Controller.Turn)
				{
                    Card masterCard = new MasterCard();
                    masterCard.Controller = me.LinkedCard.Controller;
                    masterCard.Opponent = me.LinkedCard.Opponent;
					FieldObject master = new MasterCard().LinkedFieldObject;
					me.TurnStart -= BecomeAMaster;
					me.FieldObjectCopy(master);

					Effect HappyEffect = new Effect( 0.5f, Effect.EFC.EffectLastTime() , me, me, "Vision" );
					Effect.AddEffect( HappyEffect );
				}
			}
            
        }
	}
}
