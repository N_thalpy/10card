using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class AssassinCard : Card
	{

        public AssassinCard()
            : base(1, 1, "암살자", CardTypes.monster, new AssassinField(),"Assassin","이 하수인이 플레이어에게 피해를 주는 경우 피해를 7 더한다. 앞면이 된 턴에 행동 가능이 된다.") 
		{ 
		}


	}
}
