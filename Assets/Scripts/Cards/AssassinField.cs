using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class AssassinField : FieldObject
	{

		public AssassinField(): base(1, 1, CardTypes.monster, "Assassin", "암살자", 3, 3) 
		{
            /*
            Attack += (attacker, defender, me) =>
			{
				if (me == attacker && GameAction.IsFieldObjectOnField(defender) && defender.CardType != CardTypes.player)
				{
					GameAction.DestroyFieldObject(defender);
				
					
				}
			};
             */
            Attack += (giver, taker, me) =>
                {
                    if (giver == me && taker.CardType == CardTypes.player)
                    {
                        Effect HappyEffect = new Effect(1.0f, Effect.EFC.EffectLastTime(), taker, taker, "Skull");
                        Effect.AddEffect(HappyEffect);
						GameAction.DealDamage(giver,taker, 7);
                    }
                };

            Reverse += (unit, me, attacker) =>
            {
                if (unit == me)
                    unit.Activity = true;
            };
		}


	}
}
