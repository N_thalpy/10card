﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class AvatarofHatredCard : Card
    {
        public AvatarofHatredCard()
            : base(8, 5, "증오의 화신", CardTypes.monster, new AvatarofHatredField(), "avatarofhatred", "나온 턴 행동 가능이 된다. 매 턴 끝마다 자신 외의 무작위 플레이어/앞면 하수인에게 피해를 10 주고 그 편이 된다.")
        {
        }
    }
}
