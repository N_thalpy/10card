﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class AvatarofHatredField : FieldObject
	{
		public AvatarofHatredField()
			: base(8, 5, CardTypes.monster, "avatarofhatred", "증오의 화신", 10, 10)
		{
			NonTargetBattleCry += (FieldObject Me) =>
			{
				Me.Activity = true;
			};
			
			TurnEnd += (FieldObject Me) =>
			{
				if(Me.CardType != CardTypes.mystery)
				{
					List<FieldObject> TargetCandidates = new List<FieldObject>();
					
					Player P1 = Me.Controller;
					TargetCandidates.Add(P1);
					for (int i = 0; i < 5; i++)
					{
						if (P1.Field[i].CardType == CardTypes.monster && P1.Field[i] != Me)
							TargetCandidates.Add(P1.Field[i]);
					}
					Player P2 = Me.Opponent;
					TargetCandidates.Add(P2);
					for (int i = 0; i < 5; i++)
					{
						if (P2.Field[i].CardType == CardTypes.monster && P2.Field[i] != Me)
							TargetCandidates.Add(P2.Field[i]);
					}
					
					//Trigger.RandomCaller(1,2,todo);
					
					/*
                    if (GameAction.MainSystem.MePlayer.Turn)
                        GameAction.MainSystem.ClientConnect.SendRandom(0, TargetCandidates.Count - 1, ++GameAction.MainSystem.NetworkOrder);
                    */
					
					FieldObject Target = TargetCandidates[GameAction.RandomInt(0, TargetCandidates.Count)];
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , Target, Target, "Explosion" ) );
					GameAction.DealDamage(Me, Target, 10);
					newowner = Target.Controller;
					Me.TurnStart += ChangeOwnerAtTurnStart;
					
					/*
                    GameAction.MainSystem.RandomActionQueue.Add(++GameAction.MainSystem.NetworkOrder, 
                        (int randVal) => 
                        {
                            GameAction.DamageTrigger(Me, TargetCandidates[randVal], 8);
                        });
                     */
					//yield return GameAction.MainSystem.StartCoroutine("Wai");
				}
				
			};
			
		}
		
		Player newowner = null;
		
		void ChangeOwnerAtTurnStart(FieldObject thisObject)
		{
			GameAction.GiveControl(thisObject, newowner.Controller);
			thisObject.TurnStart -= ChangeOwnerAtTurnStart;
		}
	}
}
