using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class BalanceCard : Card
    {
        public BalanceCard()
            : base(7, 4, "균형", CardTypes.magic, new MagicBox(), "Balance", "전장의 각 하수인의 공격력은 그 중 최대와 같아진다. 체력에 대해서도 마찬가지를 수행한다.")
        {
            NonTargetEvent = (thisCard) =>
			{
				int ApMax = 0;
				int HpMax = 0;
				
				for (int i = 0; i < 5; i++)
				{
					if (GameAction.IsFieldObjectMonster(thisCard.Controller.Field[i]))
						ApMax = Math.Max(ApMax, thisCard.Controller.Field[i].CurrentAp);
					if (GameAction.IsFieldObjectMonster(thisCard.Controller.Field[i]))
						HpMax = Math.Max(HpMax, thisCard.Controller.Field[i].CurrentHp);
					if (GameAction.IsFieldObjectMonster(thisCard.Opponent.Field[i]))
						ApMax = Math.Max(ApMax, thisCard.Opponent.Field[i].CurrentAp);
					if (GameAction.IsFieldObjectMonster(thisCard.Opponent.Field[i]))
						HpMax = Math.Max(HpMax, thisCard.Opponent.Field[i].CurrentHp);
				}
				
				float effectTime = Effect.EFC.EffectLastTime();
				for (int i = 0; i < 5; i++)
				{
					if (GameAction.IsFieldObjectMonster(thisCard.Controller.Field[i]))
					{
						FieldObject target = thisCard.Controller.Field[i];
						target.CurrentAp = ApMax;
						target.CurrentMaxHp = thisCard.Controller.Field[i].CurrentHp = HpMax;
						Effect HappyEffect = new Effect( 1.0f, effectTime , target, target, "Smoke" );
						Effect.AddEffect( HappyEffect );
					}
					if (GameAction.IsFieldObjectMonster(thisCard.Opponent.Field[i]))
					{
						FieldObject target = thisCard.Controller.Field[i];
						target.CurrentAp = ApMax;
						target.CurrentMaxHp = thisCard.Opponent.Field[i].CurrentHp = HpMax;
						Effect HappyEffect = new Effect( 1.0f, effectTime , target, target, "Smoke" );
						Effect.AddEffect( HappyEffect );
					}
				}
			};
        }
    }
}
