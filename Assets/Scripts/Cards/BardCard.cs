using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class BardCard : Card
	{

        public BardCard()
            : base(1, 2, "음유시인", CardTypes.monster, new BardField(),"Bard", "적 전장의 카드가 파괴될 때마다 아군 하수인 전체에게 +1/+1을 부여한다.") 
		{ 

		}


	}
}
