using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class BardField : FieldObject
	{

		public BardField(): base(1, 2, CardTypes.monster, "Bard", "음유시인", 3, 5, false) 
		{
            Death += (Unit, Me) =>
			{
				if (Me.CardType != CardTypes.mystery && (Unit.Controller != Me.Controller))
				{
					float effectTime = Effect.EFC.EffectLastTime();
					for (int i = 0; i < 5; i++)
					{
						FieldObject target = Me.Controller.Field[i];
						if (GameAction.IsFieldObjectMonster(target) && GameAction.IsFieldObjectOnField(target))
						{
							new StatBuff(target, 1, 1).Initialize();
							Effect HappyEffect = new Effect( 1.0f, effectTime , target, target, "Holy" );
							Effect.AddEffect( HappyEffect );
						}
					}
				}
			};
		}


	}
}
