﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class BelieveroftheTruthCard : Card
    {
        public BelieveroftheTruthCard()
            : base(5, 5, "진실의 신봉자", CardTypes.monster, new BelieveroftheTruthField(), "BelieveroftheTruth", "뒷면 카드를 공격하면 +4/+4를 받는다.")
        {

        }
    }
}
