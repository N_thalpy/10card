﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class BelieveroftheTruthField : FieldObject
	{
		public BelieveroftheTruthField() 
			: base(5, 5, CardTypes.monster, "BelieveroftheTruth", "진실의 신봉자", 7, 10)
		{
			Attack += (FieldObject Attacker, FieldObject Defender, FieldObject Me) =>
			{
				if (Attacker == Me && Defender.CardType == CardTypes.mystery && GameAction.IsFieldObjectOnField(Me))
				{
					new StatBuff(Me, 4, 4, false).Initialize();
					Effect HappyEffect = new Effect( 1.0f, Effect.EFC.EffectLastTime() , Defender, Defender, "Holy" );
					Effect.AddEffect( HappyEffect );
				}
			};
		}
	}
}
