using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class BelovedGirlCard : Card
	{

        public BelovedGirlCard()
            : base(1, 1, "사랑받는 소녀", CardTypes.monster, new BelovedGirlField(),"BelovedGirl", "이 하수인이 죽을 때, 아군 하수인 전체에게 턴 끝까지 +4/+0을 부여한다.") 
		{ 

		}


	}
}
