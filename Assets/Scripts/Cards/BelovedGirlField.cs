using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class BelovedGirlField : FieldObject
	{

		public BelovedGirlField(): base(1, 1, CardTypes.monster, "BelovedGirl", "사랑받는 소녀", 0, 5, false) 
		{
            Death += GiveAtkBuffToTeam;
		}

        public void GiveAtkBuffToTeam(FieldObject Unit, FieldObject Me)
        {
			if (Unit == Me) 
			{
				float effectTime = Effect.EFC.EffectLastTime();
				for (int i = 0; i < 5; i++)
				{
					FieldObject target = Me.Controller.Field[i];
					if (GameAction.IsFieldObjectMonster(target))
					{
						Buff atkBuff = new StatBuff(target, 4, 0, true);
						Effect HappyEffect = new Effect( 1.0f, effectTime , target, target,  "Brokenheart");
						Effect.AddEffect( HappyEffect );
						atkBuff.Initialize();
					}
				}
			}

        }
	}
}
