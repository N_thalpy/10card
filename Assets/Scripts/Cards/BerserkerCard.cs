using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class BerserkerCard : Card
	{

        public BerserkerCard()
            : base(1, 1, "광전사", CardTypes.monster, new BerserkerField(),"Berserker", "공격 대상을 선택할 수 없고, 내 턴 끝마다 무작위 적을 공격한다.") 
		{ 

		}


	}
}
