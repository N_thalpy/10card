using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class BerserkerField : FieldObject
	{
		public BerserkerField(): base(1, 1, CardTypes.monster, "Berserker", "광전사", 4, 6) 
		{
			AttackValidityCheck += BerserkerAttackValidityCheck;

            TurnEnd += (meObject) =>
                {
				if(meObject.CardType != CardTypes.mystery && meObject.Controller.Turn)
				{
					List<FieldObject> targetCandidates = new List<FieldObject>();
					targetCandidates.Add(meObject.Opponent);
					for (int i = 0; i < 5; i++)
					{
						if (meObject.Opponent.Field[i].CardType == CardTypes.monster || meObject.Opponent.Field[i].CardType == CardTypes.mystery)
						{
							targetCandidates.Add(meObject.Opponent.Field[i]);
						}
					}
					
					GameAction.Attack(meObject, targetCandidates[GameAction.RandomInt(0, targetCandidates.Count)]);
				}
                    
                };
		}

        public bool BerserkerAttackValidityCheck(FieldObject AttackTarget, FieldObject thisObject)
        {
            return false;
        }

	}
}
