using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class BlackKnightCard : Card
	{

        public BlackKnightCard()
            : base(6, 2, "흑기사", CardTypes.monster, new BlackKnightField(),"BlackKnight", "전투의 함성: 상대 전장에 이 하수인보다 비용이 높은 하수인 한 장당 +2/+2를 받는다.") 
		{ 

		}


	}
}
