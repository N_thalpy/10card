using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class BlackKnightField : FieldObject
	{

		public BlackKnightField(): base(6, 2, CardTypes.monster, "BlackKnight", "흑기사", 8, 12) 
		{
            NonTargetBattleCry += ( Me) =>
			{
				
				if(Me.CardType != CardTypes.mystery)
				{
					float effectTime = Effect.EFC.EffectLastTime();
					for (int i = 0; i < 5; i++)
					{
						FieldObject target = Me.Opponent.Field[i];
						if (target.CardType == CardTypes.monster&&GameAction.IsFieldObjectOnField(Me) && target.CurrentCost > Me.CurrentCost)
						{
							new StatBuff(Me, 2, 2).Initialize();
							Effect HappyEffect = new Effect( 1.0f, effectTime , target, target,  "Holy");
							Effect.AddEffect( HappyEffect );
						}
					}
					
				}
				
			};
		}


	}
}
