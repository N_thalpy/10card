﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class BlindSeerCard : Card
    {
        public BlindSeerCard()
            : base(4, 3, "눈먼 예언자", CardTypes.monster, new BlindSeerField(), "BlindSeer", "전투의 함성: 뒷면 카드 하나를 뒤집는다.")
        {

        }
    }
}
