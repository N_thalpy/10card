﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class BlindSeerField : FieldObject
    {
        public BlindSeerField() 
            : base(4, 3, CardTypes.monster, "BlindSeer", "눈먼 예언자", 5, 8)
        {
			TargetBattleCry += (target, thisObject) =>
			{
				GameAction.Reverse(target);
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , target, target,  "Smoke") );
			};

			BattleCryTargetValidityCheck += (target, thisObject) =>
			{
				return target.CardType == CardTypes.mystery;
			};

        }


    }

}
