﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class BlindingFlareCard : Card
    {
        public BlindingFlareCard()
            : base(1, 2, "눈머는 광채", CardTypes.magic, new MagicBox(), "BlindingFlare", "상대의 무작위 뒷면 카드 하나를 뒤집는다. 하수인이면 행동 불가가 된다.")
        {
            NonTargetEvent += (Me) =>
			{
				List<FieldObject> TargetCandidates = new List<FieldObject>();
				
				Player P2 = Me.Opponent;
				
				for (int i = 0; i < 5; i++)
				{
					if (P2.Field[i].CardType == CardTypes.mystery)
						TargetCandidates.Add(P2.Field[i]);
				}
				
				if (TargetCandidates.Count == 0) 
					return;
				FieldObject rand = TargetCandidates[GameAction.RandomInt(0, TargetCandidates.Count)];
				TargetCandidates.Remove(rand);
				
				if (rand.CardType == CardTypes.monster)
					rand.Activity = false;
				
				if (rand.CardType == CardTypes.monster)
				{
					rand.Activity = false;
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , rand, rand,  "Smoke") );
				}
				else
				{
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , rand, rand,  "Smoke") );
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , rand, rand,  "Frost" ));
				}

				GameAction.Reverse(rand);
			};
        }
    }
}
