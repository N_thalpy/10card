﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class BloodWraithCard : Card
    {
        public BloodWraithCard()
            : base(1, 0, "피의 망령", CardTypes.monster, new BloodWraithField(), "BloodWraith", "매 턴 시작마다 내 영웅에게 피해를 3 준다.")
        {

        }
    }
}
