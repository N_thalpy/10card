﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class BloodWraithField : FieldObject
    {
        public BloodWraithField()
            : base(1, 0, CardTypes.monster, "BloodWraith", "피의 망령", 4, 8)
        {
            TurnStart += (FieldObject Me) =>
			{
				if(Me.CardType != CardTypes.mystery)
				{
					GameAction.DealDamage(Me, Me.Controller, 3);
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , Me.Controller, Me.Controller,  "Explosion") );
				}
			};
        }
    }
}
