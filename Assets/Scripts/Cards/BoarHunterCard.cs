using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class BoarHunterCard : Card
	{

        public BoarHunterCard()
            : base(4, 3, "멧돼지 사냥꾼", CardTypes.monster, new BoarHunterField(),"BoarHunter", "잠복 : 상대가 하수인을 소환하면 그 공격력을 1로 바꾼다.",true,true) 
		{ 

		}


	}
}
