using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class BoarHunterField : FieldObject
	{
		
		public BoarHunterField(): base(4, 3, CardTypes.monster, "BoarHunter", "멧돼지 사냥꾼", 5, 9) 
		{
			
			/*
            Reverse += (unit, thisCard, attacker) =>
                {
                    if (unit != thisCard) return;
                    int ApMax = 0;

                    for (int i = 0; i < 5; i++)
                    {
                        if (GameAction.IsFieldObjectMonster(thisCard.Opponent.Field[i]))
                            ApMax = Math.Max(ApMax, thisCard.Opponent.Field[i].CurrentAp);
                    }


                    for (int i = 0; i < 5; i++)
                    {
                        if (GameAction.IsFieldObjectMonster(thisCard.Opponent.Field[i]) && thisCard.Opponent.Field[i].CurrentAp == ApMax)
                            new StatBuff(thisCard.Opponent.Field[i], -thisCard.Opponent.Field[i].CurrentAp + 1, 0).Initialize();

                    }
                };
            */
			Summon += (unit, thisCard) =>
			{
				if (unit.Controller != thisCard.Controller  && thisCard.CardType == CardTypes.mystery)
				{
					if (GameAction.IsFieldObjectOnField(thisCard)) GameAction.Reverse(thisCard);
					StatBuff atkToOne = new StatBuff(unit, -unit.CurrentAp + 1, 0);
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , unit, unit,  "Smoke") );
					atkToOne.Initialize();
				}
			};
		}
		
		
	}
}
