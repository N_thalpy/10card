using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class BoymagicianCard : Card
	{

        public BoymagicianCard()
            : base(4, 0, "소년 마법사", CardTypes.monster, new BoymagicianField(), "Boymagician", "전투의 함성: 내 패에 있는 모든 마법의 비용을 2만큼 낮춘다.또한 패에 있는 마법 카드만큼 공/체가 증가한다.", false, false) 
		{ 

		}


	}
}
