
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class BoymagicianField : FieldObject
	{
		
		public BoymagicianField(): base(4, 0, CardTypes.monster, "Boymagician", "소년 마법사", 8, 12, false, "", false) 
		{
			NonTargetBattleCry += MagicTwoCostDown;
		}
		
		public void MagicTwoCostDown(FieldObject Me)
		{
			if (Me.CardType != CardTypes.mystery)
			{				
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , Me, Me,  "Heal") );
				int Buff = 0;
				for(int i = 0; i < Me.Controller.Hand.Count; i++)
				{
					if(Me.Controller.Hand[i].CardType == CardTypes.magic)
					{
						Buff++;
						Me.Controller.Hand[i].CurrentCost -= 2;
						if(Me.Controller.Hand[i].CurrentCost < 0)
							Me.Controller.Hand[i].CurrentCost = 0;
					}
				}
				new StatBuff( Me, Buff, Buff, true ).Initialize();
			}
		}
	}
}
