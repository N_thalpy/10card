using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ChaliceofOblivionCard : Card
	{
		public ChaliceofOblivionCard()
			: base(2, 2, "망각의 잔", CardTypes.magic, new MagicBox(), "ChaliceofOblivion", "하수인 하나의 능력과 효과를 모두 제거한다.")
		{
			TargetEvent = Silence;
			TargetValidityCheck += ChaliceofOblivionTargetValidityCheck;
		}
		
		public void Silence(FieldObject Target, Card Me)
		{
			if (Target.CardType == CardTypes.monster) 
			{
				Target.Attack = delegate {};
				Target.Defend = delegate {};
				Target.Move = delegate { };
				Target.Summon = delegate { };
				Target.Leave = delegate { };
				Target.Death = delegate { };
				Target.Reverse = delegate { };
				Target.SpellTarget = new FieldObject.SpellTargetEventHandler(
					(Card Spell_, FieldObject Me_, FieldObject Target_) =>
					{
					return Target_;
				});
				Target.Spell = delegate { };
				Target.Active = new FieldObject.ActiveTargetEventHandler(
					(FieldObject User_, FieldObject Me_, FieldObject Target_) =>
					{
					return Target_;
				});
				Target.Damaging = new FieldObject.DamagingEventHandler(
					(FieldObject DamageGiver_, FieldObject DamageTaker_, int DamageAmount_, FieldObject Me_) =>
					{
					return DamageAmount_;
				});
				Target.Damaged = new FieldObject.DamagedEventHandler(
					(FieldObject DamageTaker_, FieldObject DamageGiver_, int DamageAmount_, FieldObject Me_) =>
					{
					return DamageAmount_;
				});
				Target.TurnStart = delegate { };
				Target.TurnEnd = delegate { };
				Target.CardWentToGrave = delegate { };
				Target.Retrieve = delegate { };
				Target.DeleteAllBuffs();
			}

		}
		
		public bool ChaliceofOblivionTargetValidityCheck(FieldObject Target, Card thisCard)
		{
			return Target.CardType == CardTypes.monster;
		}
	}
}
