using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ChaosCard : Card
	{
		public ChaosCard()
			: base(5, 2, "혼란", CardTypes.magic, new MagicBox(), "Chaos", "전장의 카드 하나를 이번 턴 동안 우리 편으로 만든다. 그 카드는 활동 가능 상태가 된다.")
		{
			TargetEvent = ChangeMind;
			TargetValidityCheck += ChaosTargetValidityCheck;
		}
		
		Player myController = null;
		Player origController = null;
		
		public void ChangeMind(FieldObject Target, Card Me)
		{
			if (Target.CardType != CardTypes.empty) 
			{
				myController = Me.Controller;
				origController = Target.Controller;
				Buff ChangeMindBuff = new Buff (Target, ChangeMindinit, ChangeMindEnd, true);
				ChangeMindBuff.Initialize ();
			}
		}
		
		public void ChangeMindinit(Buff thisBuff)
		{
			Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , thisBuff.BuffTarget, thisBuff.BuffTarget,  "Smoke") );
			GameAction.GiveControl(thisBuff.BuffTarget, myController);
			thisBuff.BuffTarget.Activity = true;
		}
		
		public void ChangeMindEnd(Buff thisBuff)
		{
			Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , thisBuff.BuffTarget, thisBuff.BuffTarget,  "Smoke") );
			if (!GameAction.IsFieldObjectOnField(thisBuff.BuffTarget)) return;
			GameAction.GiveControl(thisBuff.BuffTarget, origController);
			thisBuff.BuffTarget.Activity = true;
		}
		
		public bool ChaosTargetValidityCheck(FieldObject Target, Card thisCard)
		{
			bool check = false;
			for (int i = 0; i < Target.Opponent.Field.Count; i++) 
			{
				if(Target.Opponent.Field[i].CardType == CardTypes.empty)
					check = true;
			}
			
			if (check && !VChecks.IsMine(this, Target) && VChecks.IsMonsterNotMyetery(Target))
				return true;
			else
				return false;
		}
	}
}
