using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class ChivalryCard : Card
    {
        public ChivalryCard()
            : base(1, 2, "기사도", CardTypes.magic, new MagicBox(), "Chivalry", "하수인 하나에 \"적을 공격할 때 +x/+x를 받는다. x는 (적의 비용-자신의 비용)으로, 음수일 수 있다.\"능력을 준다.")
        {
			TargetEvent = TargetSelect;
			TargetValidityCheck += ChivalryTargetValidityCheck;
		}

		void TargetSelect(FieldObject Target, Card Me)
		{
			if (Target.CardType != CardTypes.empty) 
			{
				Effect.AddEffect (new Effect (1.0f, Effect.EFC.EffectLastTime (), Target, Target, "Holy"));
				Target.Attack += ChangeStatMagic;
			}
		}

		void ChangeStatMagic(FieldObject Attacker, FieldObject Defender, FieldObject Me)
		{
			if ((Attacker == Me) && GameAction.IsFieldObjectOnField(Defender) && Defender.CardType!=CardTypes.player)
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , Attacker, Attacker,  "Holy") );

				int dif = -Attacker.CurrentCost + Defender.CurrentCost;
				Buff StBuff = new StatBuff(Attacker, dif, dif, false);
				StBuff.Initialize();
			}
		}

		public bool ChivalryTargetValidityCheck(FieldObject Target,Card thisCard)
		{
			return Target.CardType == CardTypes.monster;
		}
    }
}
