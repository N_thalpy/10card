using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ColdsnapCard : Card
	{
		public ColdsnapCard()
			: base(4, 4, "매서운 한파", CardTypes.magic, new MagicBox(), "Coldsnap", "상대의 각 앞면 및 뒷면 카드를 행동 불가로 만든다.")
		{
			NonTargetEvent = AttackAllSeal;
		}
		
		void AttackAllSeal(Card Me)
		{
			float IceEffectTime = Effect.EFC.EffectLastTime();
			for(int i = 0; i < Me.Opponent.Field.Count; i++)
			{
				Effect ColdEffect = new Effect( 1.0f, IceEffectTime , Me.Opponent.Field[i],Me.Opponent.Field[i],"Frost" );
				Effect.AddEffect( ColdEffect );
				if(Me.Opponent.Field[i].CardType != CardTypes.empty)
					Me.Opponent.Field[i].Activity = false;
			}
		}
	}
}
