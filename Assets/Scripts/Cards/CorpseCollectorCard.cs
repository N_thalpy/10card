using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class CorpseCollectorCard : Card
	{

        public CorpseCollectorCard()
			: base(1, 2, "시체 수집가", CardTypes.monster, new CorpseCollectorField() ,"CorpseCollector", "전장의 하수인이 파괴될 때마다 +1/+1을 얻는다.") 
		{ 

		}


	}
}
