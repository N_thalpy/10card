using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class CorpseCollectorField : FieldObject
	{

		public CorpseCollectorField(): base(1, 2, CardTypes.monster, "CorpseCollector", "시체 수집가", 2, 5, false) 
		{ 
			Death += StatIncreasebyDeath;
		}

		void StatIncreasebyDeath(FieldObject Unit, FieldObject Me)
		{
			if (Me.CardType != CardTypes.mystery && (Unit != Me))
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , Unit, Unit,  "Holy") );
				Buff StBuff = new StatBuff(Me, 1, 1, false);
				StBuff.Initialize();
			}
		}
	}
}
