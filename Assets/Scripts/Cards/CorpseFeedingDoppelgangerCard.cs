
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class CorpseFeedingDoppelgangerCard : Card
	{

        public CorpseFeedingDoppelgangerCard()
			: base(5, 5, "무덤 도플갱어", CardTypes.monster, new CorpseFeedingDoppelgangerField(), "CorpseFeedingDoppelganger", "잠복: 상대가 하수인을 회수하면, 그것을 다시 무덤으로 보내고 이 하수인이 그 복사본이 된다.",true,true) 
		{ 

		}


	}
}
