using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class CorpseFeedingDoppelgangerField : FieldObject
	{
		public CorpseFeedingDoppelgangerField(): base(5, 5, CardTypes.monster, "CorpseFeedingDoppelganger", "무덤 도플갱어", 0, 3) 
		{ 
			//Reverse += SummonOpponentGraveMonster;
			Retrieve+=(retCard, me)=>
			{
				if (me.CardType == CardTypes.mystery && GameAction.IsFieldObjectOnField(me) && retCard.CardType == CardTypes.monster && retCard.Controller == me.Opponent)
				{
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , me, me,  "Smoke") );
					GameAction.Reverse(me);
					me.FieldObjectCopy(retCard.LinkedFieldObject);
					
					retCard.Controller.Hand.Remove(retCard);
					if (!retCard.Recyclable) 
						Debug.LogError("ERROR: 시체 먹는 도플갱어가 할 일이 뭔가 이상해요!");
					
					Card newCard = (Card)Activator.CreateInstance(retCard.GetType());
					newCard.Controller = retCard.Controller;
					newCard.Opponent = retCard.Opponent;
					newCard.LinkedFieldObject.Controller = newCard.Controller;
					newCard.LinkedFieldObject.Opponent = newCard.Opponent;
					
					retCard.Controller.Grave.Add(newCard);
					
				}
			};
		}
		/*
		public void SummonOpponentGraveMonster(FieldObject Target, FieldObject Me, FieldObject attacker)
		{
			List<Card> Temp_List = new List<Card> ();
			int location = Me.GetPosition ();

			for (int i = 0; i < Me.Opponent.Grave.Count; i++) 
			{
				if(Me.Opponent.Grave[i].CardType == CardTypes.monster)
					Temp_List.Add (Me.Opponent.Grave[i]);
			}

			if (Temp_List.Count > 0) 
			{
				int Rndnum = GameAction.RandomInt(0,Temp_List.Count);
				Card GraveCard = Me.Controller.Grave[Rndnum];
				//FieldObject willSummon = GraveCard.LinkedFieldObject;
				Me.FieldObjectCopy( GraveCard.LinkedFieldObject );
				Me.Activity = false;
			}
        */
		
		
	}
}
