﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class CorpseHarvesterCard : Card
    {
        public CorpseHarvesterCard()
            : base(3, 6, "시체 수확자", CardTypes.monster, new CorpseHarvesterField(), "CorpseHarvester", "하수인이 죽을 때마다 내 자원을 3 더한다.")
        {

        }
    }
}
