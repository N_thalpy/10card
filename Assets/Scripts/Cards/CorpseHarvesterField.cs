﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class CorpseHarvesterField : FieldObject
    {

        public CorpseHarvesterField()
            : base(3, 6, CardTypes.monster, "CorpseHarvester", "시체 수확자", 4, 3)
        {
            Death += (FieldObject Unit, FieldObject Me) =>
			{
				if ((Unit.LinkedCard == null || Unit.LinkedCard.CardType == CardTypes.monster) && (Unit != Me) && (Me.CardType != CardTypes.mystery))
				{
					Controller.Resource += 3;
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , Me, Me,  "Diamond") );
				}
			};
        }
    }
}
