using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class CorpseMarionetteCard : Card
    {
        public CorpseMarionetteCard()
            : base(4, 1, "시체 인형", CardTypes.magic, new MagicBox(), "CorpseMarionette", "내 무덤에 있는 하수인 중 비용이 가장 높은 것을 부활시킨다. 그 유닛은 '마법의 대상이 되면 파괴된다.'를 얻는다.")
        {
			NonTargetEvent += Revive;
        }

		public void Revive(Card Me)
		{
			int location = GameAction.EmptyFieldLocation (Me.Controller);
			if(location != -1)
			{
				int max = -10000;
				int gravelocation = -1;
				for(int i = 0; i < Me.Controller.Grave.Count; i++)
				{
					if(Me.Controller.Grave[i].CurrentCost > max)
					{
						gravelocation = i;
						max = Me.Controller.Grave[i].CurrentCost;
					}
				}
                if (gravelocation == -1) return;
				FieldObject RevivedObject = Me.Controller.Grave[gravelocation].LinkedFieldObject;
				RevivedObject.SpellTarget += Suicide;
				GameAction.SummonAtPosition(Me.Controller.Field[location],RevivedObject,false);
				Me.Controller.Grave.Remove(Me.Controller.Grave[gravelocation]);
			}
		}

		public FieldObject Suicide(Card Spell, FieldObject Me, FieldObject Target)
		{
			if (Me == Target) 
				GameAction.DestroyFieldObject(Me);

			return Target;
		}
    }
}
