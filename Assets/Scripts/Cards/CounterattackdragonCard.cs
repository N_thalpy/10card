using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class CounterattackdragonCard : Card
	{

        public CounterattackdragonCard()
            : base(6, 0, "역습의 용", CardTypes.monster, new CounterattackdragonField(), "Counterattackdragon", "변신하면서 상대의 각 하수인에게 7의 피해를 입힌다. 매 턴 시작마다 체력 5 이하인 상대의 하수인을 전부 파괴한다.", false, false) 
		{ 

		}


	}
}
