
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class CounterattackdragonField : FieldObject
	{

		public CounterattackdragonField(): base(6, 0, CardTypes.monster, "Counterattackdragon", "역습의 용", 15, 9, false, "", false) 
		{ 
			TurnStart += KillFiveUnderHP;
		}

		public void KillFiveUnderHP(FieldObject Me)
		{
			if (Me.CardType != CardTypes.mystery) 
			{
				float effectTime = Effect.EFC.EffectLastTime();
				for(int i = 0; i < Me.Opponent.Field.Count; i++)
				{
					if(Me.Opponent.Field[i].CardType == CardTypes.monster)
					{
						FieldObject target = Me.Opponent.Field[i];
						if(target.CurrentHp < 6)
						{
							Effect.AddEffect( new Effect( 1.0f, effectTime , target, target,  "Smoke") );
							GameAction.DestroyFieldObject(target);
						}
					}
				}
			}

		}

	}
}
