﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class CowardlyThiefCard : Card
	{

        public CowardlyThiefCard()
			: base(1, 1, "겁쟁이 도둑", CardTypes.monster, new CowardlyThiefField(), "CowardThief", "상대가 카드를 사용하면 이 하수인을 손으로 돌린다.상대를 공격할 때, 상대의 자원을 1줄이고 내 자원을 1늘린다.") 
		{ 

		}


	}
}
