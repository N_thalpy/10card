﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class CowardlyThiefField : FieldObject
	{

		public CowardlyThiefField(): base(1, 1, CardTypes.monster, "CowardThief", "겁쟁이 도둑", 4,1) 
		{ 
			Summon += ComeBackbySummon;
			Spell += ComeBackbySpell;
			SpellTarget += ComeBackbySpellTarget;
			Attack += GainResourcebyAttack;
		}

		public void ComeBackbySummon(FieldObject Unit, FieldObject Me)
		{
			if (Me.CardType != CardTypes.mystery) 
			{
				if (Unit.Controller == Me.Opponent) 
				{
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , Me, Me,  "Smoke") );
					GameAction.ReturnToHand(Me);
				}
			}
		}

		public void ComeBackbySpell(Card Spell, FieldObject Me)
		{
			if (Me.CardType != CardTypes.mystery) 
			{
				if (Spell.Controller == Me.Opponent) 
				{
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , Me, Me,  "Smoke") );
					GameAction.ReturnToHand(Me);
				}
			}

		}

		public FieldObject ComeBackbySpellTarget(Card Spell, FieldObject Me, FieldObject Target)
		{
			if (Me.CardType != CardTypes.mystery) 
			{
				if (Spell.Controller == Me.Opponent) 
				{
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , Me, Me,  "Smoke") );
					GameAction.ReturnToHand(Me);
				}
			}

			return Target;
		}

		public void GainResourcebyAttack(FieldObject Attacker, FieldObject Defender, FieldObject Me)
		{
			if (Attacker == Me) 
			{
				Me.Controller.Resource += 1;
				if(Me.Controller.Resource > Me.Controller.MaxResource)
					Me.Controller.Resource = Me.Controller.MaxResource;

				Me.Opponent.Resource -= 1;
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , Me, Me,  "Diamond") );
			}
		}


	}
}
