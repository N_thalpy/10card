using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class CowardlyKnightCard : Card
	{

        public CowardlyKnightCard()
			: base(4, 1, "비겁한 기사", CardTypes.monster, new CowardlyKnightField(), "CowardlyKnight", "전투 시 +x/+x를 받는다. x는 (이 하수인의 비용 - 적의 비용)이며, 음수일 수 있다.") 
		{ 

		}


	}
}
