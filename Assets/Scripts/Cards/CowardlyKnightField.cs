using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class CowardlyKnightField : FieldObject
	{

		public CowardlyKnightField(): base(4, 1, CardTypes.monster, "CowardlyKnight", "비겁한 기사", 6, 8) 
		{ 
			Attack += ChangeStatMagic;
		}

		public void ChangeStatMagic(FieldObject Attacker, FieldObject Defender, FieldObject Me)
		{
            if ((Attacker == Me) && GameAction.IsFieldObjectOnField(Defender) && Defender.CardType != CardTypes.player)
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , Defender, Defender,  "Holy") );
                int dif = Attacker.CurrentCost - Defender.CurrentCost;
                Buff StBuff = new StatBuff(Attacker, dif, dif, false);
                StBuff.Initialize();
            }
            else if ((Defender == Me) && GameAction.IsFieldObjectOnField(Attacker) && Attacker.CardType != CardTypes.player)
            {
                int dif = Defender.CurrentCost - Attacker.CurrentCost;
                Buff StBuff = new StatBuff(Defender, dif, dif, false);
                StBuff.Initialize();
            }
		}
	}
}
