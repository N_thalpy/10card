﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class CurseCard : Card
    {
        public CurseCard()
            : base(4, 2, "저주", CardTypes.magic, new MagicBox(), "Curse", "내 영웅 및 적의 모든 하수인에게 피해를 5 준다.")
        {
            NonTargetEvent += (Card Me) =>
			{
				float effectTime = Effect.EFC.EffectLastTime();
				for (int i = 0; i < Me.Opponent.Field.Count; i++)
				{
					FieldObject target = Me.Opponent.Field[i];

					if (target.CardType == CardTypes.monster)
					{
						Effect.AddEffect( new Effect( 1.0f, effectTime , target, target,  "Smoke") );
						GameAction.DealDamage(Me.Controller, target, 5);
					}
				}
				
				GameAction.DealDamage(Me.Controller,Me.Controller, 5);
			};
        }
    }
}
