using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class CurseofVoidCard : Card
    {
        public CurseofVoidCard()
            : base(6, 3, "공허의 저주", CardTypes.magic, new MagicBox(), "CurseofVoid", "상대 전장의 각 하수인의 체력과 공격력을 상대 전장의 빈 칸 숫자로 나눈다(소수점 버림).  0일 경우 1로 나눈다.")
        {
			NonTargetEvent += DivideEmptyDebuff;
        }

		public void DivideEmptyDebuff(Card Me)
		{
			int EmptyCount = 0;
			List<FieldObject> Temp_List = new List<FieldObject> ();

			for(int i = 0; i < Me.Opponent.Field.Count; i++)
			{
				if(Me.Opponent.Field[i].CardType == CardTypes.empty)
					EmptyCount += 1;
				else
				{
					if(Me.Opponent.Field[i].CardType == CardTypes.monster)
						Temp_List.Add (Me.Opponent.Field[i]);
				}
			}

			float effectTime = Effect.EFC.EffectLastTime();
			for (int i = 0; i < Temp_List.Count; i++) 
			{
				if(EmptyCount > 1)
				{
					FieldObject target = Temp_List[i];

					int ChangedAp = -(target.CurrentAp - (target.CurrentAp / EmptyCount));
					int ChangedHp = -(target.CurrentHp - (target.CurrentHp / EmptyCount));

					if(target.CurrentAp + ChangedAp < 1)
						ChangedAp += 1;
					if(target.CurrentHp + ChangedHp < 1)
						ChangedHp += 1;
					Buff DisBuff = new StatBuff(Temp_List[i],ChangedAp,ChangedHp,false);
					DisBuff.Initialize();

					Effect.AddEffect( new Effect( 1.0f, effectTime , target, target,  "Skull") );
				}
			}
		}
    }
}
