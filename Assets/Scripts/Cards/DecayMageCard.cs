using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class DecayMageCard : Card
	{


        public DecayMageCard()
			: base(5, 3, "부식의 마법사", CardTypes.monster, new DecayMageField(), "DecayMage", "전투의 함성: 하수인 하나를 목표로 정해 그 공격력을 1로 만든다.") 
		{

		}

	}
}
