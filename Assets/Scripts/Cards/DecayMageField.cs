using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class DecayMageField : FieldObject
	{
		
		public DecayMageField(): base(5, 3, CardTypes.monster, "DecayMage", "부식의 마법사", 7, 9) 
		{ 
			TargetBattleCry += ApDecreaseDebuff;
			BattleCryTargetValidityCheck += (target, thisObject) =>
			{
				return target.CardType == CardTypes.monster;
			};
		}
		
		public void ApDecreaseDebuff(FieldObject Target, FieldObject Me)
		{
			Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Target, Target,  "Skull") );
			int ChangedAp = -(Target.CurrentAp - 1);
			Buff ApDeBuff = new StatBuff (Target, ChangedAp, 0);
			ApDeBuff.Initialize ();
		}
		
		
	}
}
