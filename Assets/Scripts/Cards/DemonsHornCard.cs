using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class DemonsHornCard : Card
    {
        public DemonsHornCard()
            : base(2, 2, "악마의 뿔피리", CardTypes.magic, new MagicBox(), "DemonsHorn", "적 전장 전체에 -2/-0을 부여하고 피해를 2 입힌다. 무덤에 가면서 이 카드를 천사의 뿔피리 카드로 바꾼다.")
		{
			NonTargetEvent += StatDebuffwithDamage;
		}

		public void StatDebuffwithDamage(Card Me)
		{
            Me.Recyclable = false;
			float effectTime = Effect.EFC.EffectLastTime();
			for (int i = 0; i < 5; i++)
			{
				FieldObject fo = Me.Controller.Opponent.Field[i];
				if (fo.CardType == CardTypes.monster)
				{
					int decAp = 2;
					if(fo.CurrentAp == 2)
						decAp = 1;
					else if(fo.CurrentAp == 1)
						decAp = 0;
					
					Buff DeBuff = new StatBuff(fo,-decAp,0);
					DeBuff.Initialize();
					GameAction.DealDamage(Me.Controller,fo,2);
				}
				Effect.AddEffect( new Effect( 1.0f, effectTime , fo, fo,  "Skull") );
			}

			Card newCard = new AngelsHornCard ();
			newCard.Controller = Me.Controller;
			newCard.Opponent = Me.Opponent;
			newCard.LinkedFieldObject.Controller = Me.Controller;
			newCard.LinkedFieldObject.Opponent = Me.Opponent;
			
			Me.Controller.Grave.Add(newCard);
		}
    }
}
