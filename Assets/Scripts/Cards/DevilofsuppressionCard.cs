using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class DevilofsuppressionCard : Card
	{

        public DevilofsuppressionCard()
            : base(8, 0, "억압의 악마", CardTypes.monster, new DevilofsuppressionField(), "Devilofsuppression", "각 플레이어의 마법은 모두 무효화된다.", false, false) 
		{ 

		}


	}
}
