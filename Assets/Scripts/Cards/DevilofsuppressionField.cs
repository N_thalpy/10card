using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class DevilofsuppressionField : FieldObject
	{
		
		public DevilofsuppressionField(): base(8, 0, CardTypes.monster, "Devilofsuppression", "억압의 악마", 11, 18, false, "", false) 
		{ 
			Spell += NonTargetMagicBlock;
			SpellTarget += TargetMagicBlock;
		}
		
		public void NonTargetMagicBlock(Card Spell, FieldObject Me)
		{
			Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , Me, Me,  "Vision") );
			if (Me.CardType != CardTypes.mystery) 
			{
				Spell.NonTargetEvent = delegate {};
				Spell.Spell = delegate {};
			}
			
		}
		
		public FieldObject TargetMagicBlock(Card Spell, FieldObject Me, FieldObject Target)
		{
			Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , Me, Me,  "Vision") );
			if (Me.CardType != CardTypes.mystery) 
			{
				Spell.TargetEvent = delegate {};
				Spell.SpellTarget = new Card.SpellTargetEventHandler(
					(Card Spell_, Card Me_, FieldObject Target_) =>
					{
					return Target_;
				});
			}
			
			return Target;
		}
		
		
	}
}
