using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class DevilofvoidCard : Card
	{
		
		public DevilofvoidCard()
			: base(0, 0, "공허의 악마", CardTypes.monster, new DevilofvoidField(), "Devilofvoid", "내 마나가 0일 때 상대가 마법을 사용하면 무효화하고, 그 마법의 비용의 3배만큼의 공/체로 손에서 소환한다.", false, false) 
		{ 
			Spell += SummonbyNonTarget;
			SpellTarget += SummonbyTarget;
		}
		
		public void SummonbyNonTarget(Card Spell, Card Me)
		{
			if (Me.Controller.Resource == 0 && Spell.Controller!=Me.Controller) 
			{
				Spell.NonTargetEvent = delegate {};
				Spell.Spell = delegate {};
				
				int increase = Spell.CurrentCost * 3;
				Me.Controller.Hand.Remove(Me);
				GameAction.SummonAtPosition(Me.Controller.Field[GameAction.EmptyFieldLocation(Me.Controller)],Me.LinkedFieldObject);
				StatBuff AtHpBuff = new StatBuff(Me.LinkedFieldObject,increase,increase);
				AtHpBuff.Initialize();
			}
		}
		
		public FieldObject SummonbyTarget(Card Spell, Card Me, FieldObject Target)
		{
			if (Me.Controller.Resource == 0 && Me.Controller!=Spell.Controller) 
			{
				Spell.TargetEvent = delegate {};
				Spell.SpellTarget = new Card.SpellTargetEventHandler(
					(Card Spell_, Card Me_, FieldObject Target_) =>
					{
					return Target_;
				});
				
				int increase = Spell.CurrentCost * 3;
				Me.Controller.Hand.Remove(Me);
				GameAction.SummonAtPosition(Me.Controller.Field[GameAction.EmptyFieldLocation(Me.Controller)],Me.LinkedFieldObject);
				StatBuff AtHpBuff = new StatBuff(Me.LinkedFieldObject,increase,increase);
				AtHpBuff.Initialize();
				
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Me.LinkedFieldObject, Me.LinkedFieldObject,  "Vision") );
			}
			
			return Target;
		}
	}
}
