﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class DevotedGuardianCard : Card
    {
        public DevotedGuardianCard()
            : base(3, 2, "헌신적인 수호자", CardTypes.monster, new DevotedGuardianField(), "DevotedGuardian","아군이 피해를 입으려 할 경우, 자신의 생명력을 희생하여 그 피해를 최소 1까지 경감시킨다.")
        {

        }


    }
}
