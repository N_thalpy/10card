﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class DevotedGuardianField : FieldObject
	{
		public DevotedGuardianField()
			: base(3, 2, CardTypes.monster, "DevotedGuardian", "헌신적인 수호자", 3, 8)
		{
			Damaged += DamageAbsorb;
		}
		
		public int DamageAbsorb(FieldObject DamageTaker, FieldObject DamageGiver, int DamageAmount, FieldObject Me)
		{
			if (Me.CardType != CardTypes.mystery) 
			{
				if (DamageAmount > 0) 
				{
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Me, Me,  "Holy") );
					if (DamageTaker.Controller == Me.Controller) 
					{
						int PreviousDamage = DamageAmount;
						if (Me.CurrentHp - PreviousDamage < 1) {
							DamageAmount = (PreviousDamage - Me.CurrentHp) + 1;
							Me.CurrentHp = 1;
						} 
						else 
						{
							DamageAmount = 1;
							Me.CurrentHp = Me.CurrentHp - PreviousDamage + 1;
						}
					}
				}
				
				Debug.Log ("헌신적인 수호자" + DamageAmount.ToString());
			}
			
			return DamageAmount;
			
		}
	}
}
