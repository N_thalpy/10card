using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class DoomdevilCard : Card
	{
		
		public DoomdevilCard()
			: base(20, 0, "종말의 악마", CardTypes.monster, new DoomdevilField(), "Doomdevil", "턴 시작에 자원이 20이상이면 손에서 비용 20으로 소환되고 자신 외 양 플레이어의 손과 전장 전체를 파괴한다.", false, false) 
		{
			TurnStart += ForcedSummon;
		}
		
		public void ForcedSummon(Card Me)
		{
			if (Me.Controller.Resource > 19) 
			{
				Me.Controller.Resource -= 20;
				
				float effectTime = Effect.EFC.EffectLastTime();
				for(int i = 0; i < Me.Controller.Field.Count; i++)
				{
					FieldObject target = Me.Controller.Field[i];
					if(target.CardType != CardTypes.empty)
					{
						Effect.AddEffect( new Effect( 1.0f, effectTime , target, target,  "Explosion") );
						GameAction.DestroyFieldObject(target);
					}
				}
				
				for(int i = 0; i < Me.Opponent.Field.Count; i++)
				{
					FieldObject target = Me.Opponent.Field[i];
					if(target.CardType != CardTypes.empty)
					{
						Effect.AddEffect( new Effect( 1.0f, effectTime , target, target,  "Explosion") );
						GameAction.DestroyFieldObject(Me.Opponent.Field[i]);
					}
				}

				GameAction.SummonAtPosition(Me.Controller.Field[2], Me.LinkedFieldObject);
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Me.LinkedFieldObject, Me.LinkedFieldObject,  "Vision") );

				
				while(Me.Controller.Hand.Count > 0)
				{
					if((Me.Controller.Hand[0] != Me) && (Me.Controller.Hand[0].Recyclable))
						Me.Controller.Grave.Add(Me.Controller.Hand[0]);
					
					Me.Controller.Hand.Remove(Me.Controller.Hand[0]);
				}
				
				while(Me.Opponent.Hand.Count > 0)
				{
					if((Me.Opponent.Hand[0] != Me) && (Me.Opponent.Hand[0].Recyclable))
						Me.Opponent.Grave.Add(Me.Opponent.Hand[0]);
					
					Me.Opponent.Hand.Remove(Me.Opponent.Hand[0]);
				}
				
				GameAction.MainSystem.CardSort();
			}
		}
		
		
	}
}
