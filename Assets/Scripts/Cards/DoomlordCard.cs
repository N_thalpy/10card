﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class DoomLordCard : Card
    {
        public DoomLordCard()
            : base(4, 0, "종말의 군주", CardTypes.monster, new DoomLordField(), "DoomLord", "내 하수인이 죽을 때마다 +2/+2를 받는다.",false)
        { }
    }
}
