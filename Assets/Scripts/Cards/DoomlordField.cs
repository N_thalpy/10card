﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class DoomLordField : FieldObject
    {
        public DoomLordField()
            : base(4, 0, CardTypes.monster, "DoomLord", "종말의 군주", 10, 10, false, "", false)
        {
            Death += (FieldObject Unit, FieldObject Me) =>
			{
                if (Me.CardType != CardTypes.mystery && (Unit.Controller == Me.Controller) && Unit != Me)
                {
                    Effect.AddEffect(new Effect(1.0f, Effect.EFC.EffectLastTime(), Me, Me, "Holy"));
                    new StatBuff(Me, 1, 1).Initialize();
                }
			};
        }
    }
}
