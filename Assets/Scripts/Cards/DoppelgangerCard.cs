using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class DoppelgangerCard : Card
	{

        public DoppelgangerCard()
            : base(3, 0, "도플갱어", CardTypes.monster, new DoppelgangerField(), "Doppelganger", "잠복 : 적 하수인이 소환될 때 자신이 그것으로 변신하고 그 하수인을 파괴한다.", false, true) 
		{ 

		}


	}
}
