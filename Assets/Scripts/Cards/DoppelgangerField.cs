using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class DoppelgangerField : FieldObject
	{

		public DoppelgangerField(): base(3, 0, CardTypes.monster, "Doppelganger", "도플갱어", 0, 1, false, "", false) 
		{ 
			Summon += ChangeEnemyAndKill;
		}

		public void ChangeEnemyAndKill(FieldObject Unit, FieldObject Me)
		{
			if (Me.CardType == CardTypes.mystery) 
			{
				if (Unit.CardType == CardTypes.monster && (Unit.Controller == Me.Opponent))
				{
					Effect.AddEffect(new Effect(1.0f, Effect.EFC.EffectLastTime(), Me, Me, "Smoke"));
					Me.FieldObjectCopy(Unit);
					//Me.Recyclable = false;
					
					Effect.AddEffect(new Effect(1.0f, Effect.EFC.EffectLastTime(), Unit, Unit, "Explosion"));
					GameAction.DestroyFieldObject(Unit);
				}
			}
		}
	}
}
