using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class DruidofdragonCard : Card
	{

        public DruidofdragonCard()
            : base(6, 0, "용의 드루이드", CardTypes.monster, new DruidofdragonField(), "Druidofdragon", "전투의 함성: 나의 체력이 상대보다 높으면 압도하는 용으로 변신한다. 상대보다 낮으면 역습의 용으로 변신한다.", false, false) 
		{ 

		}


	}
}
