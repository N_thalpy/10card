using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class DruidofdragonField : FieldObject
	{

		public DruidofdragonField(): base(6, 0, CardTypes.monster, "Druidofdragon", "용의 드루이드", 9, 9, false, "", false) 
		{ 
			Summon += TwoFaceSummon;
		}

		public void TwoFaceSummon(FieldObject Unit, FieldObject Me)
		{
			if (Unit == Me) 
			{
				if (Me.CardType != CardTypes.mystery)
				{
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Me, Me,  "Smoke") );

					int SummonerHp = Unit.Controller.CurrentHp;
					int OtherHp = Unit.Opponent.CurrentHp;
					int location = Unit.GetPosition();
					
					if(SummonerHp > OtherHp)
					{
						for(int i = 0; i < Unit.Opponent.Field.Count; i++)
						{
							if(Unit.Opponent.Field[i].CardType == CardTypes.monster)
							{
								Unit.Opponent.Field[i].CurrentAp -= 7;
								if(Unit.Opponent.Field[i].CurrentAp < 0)
									Unit.Opponent.Field[i].CurrentAp = 0;
							}
						}
						
						Card UnittoChange = new OverwhelmingdragonCard();
						
						FieldObject empty = new EmptyField();
						empty.Controller = Unit.Controller;
						empty.Opponent = Unit.Opponent;
						
						GameAction.MainSystem.FieldToUI (Unit.Controller.Field[location]).GetComponent<UIChanger> ().PlayerField = empty;
						
						Unit.Controller.Field[location] = empty;
						
						GameAction.SummonAtPosition(Unit.Controller.Field[location],UnittoChange.LinkedFieldObject);
						
					}
					else if(SummonerHp < OtherHp)
					{
						for(int i = 0; i < Unit.Opponent.Field.Count; i++)
						{
							if(Unit.Opponent.Field[i].CardType == CardTypes.monster)
								GameAction.DealDamage(Unit,Unit.Opponent.Field[i],7);
						}
						
						Card UnittoChange = new CounterattackdragonCard();
						
						FieldObject empty = new EmptyField();
						empty.Controller = Unit.Controller;
						empty.Opponent = Unit.Opponent;
						
						GameAction.MainSystem.FieldToUI (Unit.Controller.Field[location]).GetComponent<UIChanger> ().PlayerField = empty;
						
						Unit.Controller.Field[location] = empty;
						
						GameAction.SummonAtPosition(Unit.Controller.Field[location],UnittoChange.LinkedFieldObject);
					}
				}

			}
		}


	}
}
