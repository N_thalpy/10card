using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class DustWhirlwindCard : Card
    {
        public DustWhirlwindCard()
            : base(2, 1, "먼지 돌풍", CardTypes.magic, new MagicBox(), "DustWhirlwind", "하수인 하나에게 피해를 6 입히고 -3/-0을 부여한다.")
        {
			TargetEvent += StatDebuffwithDamage;
			TargetValidityCheck += ThisValidityCheck;
        }

		public void StatDebuffwithDamage(FieldObject Target, Card Me)
		{
			Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Target, Target,  "Wind") );
			if (Target.CardType == CardTypes.monster)
			{
				int decAp = 3;

				if(Target.CurrentAp == 2)
					decAp = 2;
				else if(Target.CurrentAp == 1)
					decAp = 1;
				
				Buff DeBuff = new StatBuff(Target,-decAp,0);
				DeBuff.Initialize();
				GameAction.DealDamage(Me.Controller,Target,6);
			}
		}

		public bool ThisValidityCheck(FieldObject Target, Card Me)
		{
			if (Target.CardType == CardTypes.monster)
				return true;
			else
				return false;
		}
    }
}
