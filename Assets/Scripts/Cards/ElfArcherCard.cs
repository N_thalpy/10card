using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ElfArcherCard : Card
	{

        public ElfArcherCard()
			: base(2, 1, "엘프 궁수", CardTypes.monster, new ElfArcherField(), "ElfArcher","전투의 함성: 하수인 하나에게 피해를 4 준다.") 
		{

		}
	}
}
