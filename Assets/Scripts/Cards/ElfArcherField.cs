using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ElfArcherField : FieldObject
	{

		public ElfArcherField(): base(2, 1, CardTypes.monster, "ElfArcher", "엘프 궁수", 6, 2) 
		{ 
			TargetBattleCry += DamageToObject;
			BattleCryTargetValidityCheck += ThisTargetValidity;
		}

		public void DamageToObject(FieldObject Target, FieldObject Me)
		{
			GameAction.DealDamage(Me,Target,4);
		}

		
		
		public bool ThisTargetValidity(FieldObject Target, FieldObject Me)
		{
			if (Target.CardType == CardTypes.monster)
				return true;
			else
				return false;
		}

	}
}
