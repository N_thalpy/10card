using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class ElixirofMaelstromCard : Card
    {
        public ElixirofMaelstromCard()
            : base(10, 0, "대혼돈의 영약", CardTypes.magic, new MagicBox(), "ElixirofMaelstrom", "전장의 모든 카드를 파괴한다. 그 비용의 총합만큼 내 체력을 늘린다.", false)
        {
			NonTargetEvent += AllDestroyHpRecovery;
        }

		public void AllDestroyHpRecovery(Card Me)
		{
			int HpUp = 0;

			float effectTime = Effect.EFC.EffectLastTime();
			for (int i = 0; i < Me.Controller.Field.Count; i++) 
			{
				FieldObject target = Me.Controller.Field[i];
				if(target.CardType != CardTypes.empty)
				{
					HpUp += target.CurrentCost;
					GameAction.DestroyFieldObject(target);
					Effect.AddEffect( new Effect( 1.0f, effectTime , target, target,  "Explosion") );
				}
			}
			for (int i = 0; i < Me.Opponent.Field.Count; i++) 
			{
				FieldObject target = Me.Controller.Field[i];
				if(target.CardType != CardTypes.empty)
				{
					HpUp += target.CurrentCost;
					GameAction.DestroyFieldObject(Me.Opponent.Field[i]);
					Effect.AddEffect( new Effect( 1.0f, effectTime , target, target,  "Explosion") );
				}
			}

			Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Me.Controller, Me.Controller,  "Skull") );
			GameAction.Heal (Me.Controller, HpUp);
		}
    }
}
