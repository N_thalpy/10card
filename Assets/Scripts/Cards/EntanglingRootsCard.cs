﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class EntanglingRootsCard : Card
	{
		public EntanglingRootsCard()
			: base(2, 2, "휘감는 뿌리", CardTypes.magic, new MagicBox(), "EntanglingRoots", "하수인 하나를 목표로 한다. 그 하수인은 행동 불가 상태가 된다.")
		{
			TargetEvent = delegate(FieldObject Target, Card ThisCard)
			{
				if ( Target.CardType == CardTypes.empty ) return;
				Target.Activity = false;
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Target, Target,  "Frost") );
			};
			TargetValidityCheck += EntanglingRootsTargetValidityCheck;
			
		}
		
		public bool EntanglingRootsTargetValidityCheck(FieldObject Target, Card thisCard)
		{
			return Target.CardType == CardTypes.monster;
		}
	}
}
