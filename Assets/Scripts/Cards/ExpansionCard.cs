﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class ExpansionCard : Card
    {
        public ExpansionCard()
            : base(2, 4, "팽창", CardTypes.magic, new MagicBox(), "Expansion","전장의 아군 하수인 하나에게 +5/+5를 부여하고 비용을 4 늘린다. 이번 턴 끝에 그것을 파괴한다.")
        {
			TargetValidityCheck += ExpansionTargetValidityCheck;
            TargetEvent = new TargetingEvent((FieldObject Target, Card Me) =>
                {
                    Buff zeroOne = new Buff(Target, 
				    (thisBuff) =>
                    {
                        thisBuff.BuffTarget.CurrentAp += 5;
                        thisBuff.BuffTarget.CurrentMaxHp += 5;
                        thisBuff.BuffTarget.CurrentHp += 5;
                        thisBuff.BuffTarget.CurrentCost += 4;

						Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), thisBuff.BuffTarget, thisBuff.BuffTarget,  "Spark") );
                    },
                    (thisBuff) =>
                    {
                        thisBuff.BuffTarget.CurrentAp -= 5;
                        thisBuff.BuffTarget.CurrentMaxHp -= 5;
                        thisBuff.BuffTarget.CurrentHp = Math.Min(thisBuff.BuffTarget.CurrentHp, thisBuff.BuffTarget.CurrentMaxHp);
                        thisBuff.BuffTarget.CurrentCost -= 4;

						GameAction.DestroyFieldObject(thisBuff.BuffTarget);
						Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), thisBuff.BuffTarget, thisBuff.BuffTarget,  "Spark") );
                    }, true);

                    zeroOne.Initialize();
                });
 
        }
        public  bool ExpansionTargetValidityCheck(FieldObject Target, Card ThisCard)
        {
            return (Target.Controller == this.Controller && Target.CardType == CardTypes.monster);
        }
    }

}
