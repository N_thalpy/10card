using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ExpertSmithCard : Card
	{

        public ExpertSmithCard()
			: base(5, 3, "노련한 대장장이", CardTypes.monster, new ExpertSmithField(),"ExpertSmith", "전투의 함성: 하수인 하나에게 턴 끝까지 +3/+0을 부여한다.") 
		{ 
			TargetValidityCheck += ThisTargetValidity;
		}

		public bool ThisTargetValidity(FieldObject Target, Card Me)
		{
			if (Target.CardType == CardTypes.monster)
				return true;
			else
				return false;
		}
	}
}
