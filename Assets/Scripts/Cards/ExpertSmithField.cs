using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ExpertSmithField : FieldObject
	{

		public ExpertSmithField(): base(5, 3, CardTypes.monster, "ExpertSmith", "노련한 대장장이", 7, 10) 
		{ 
			TargetBattleCry += AtUpBuff;
			BattleCryTargetValidityCheck += ThisValidityCheck;
		}

		public void AtUpBuff(FieldObject Target, FieldObject Me)
		{
			Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Target, Target,  "Holy") );
			StatBuff AtBuff = new StatBuff (Target, 3, 0, true);
			AtBuff.Initialize ();
		}

		public bool ThisValidityCheck(FieldObject Target, FieldObject Me)
		{
			if (Target.CardType == CardTypes.monster)
				return true;
			else
				return false;
		}
	}
}
