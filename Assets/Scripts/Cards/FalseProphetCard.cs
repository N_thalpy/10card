using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class FalseProphetCard : Card
	{

        public FalseProphetCard()
            : base(4, 2, "돌팔이 점술가", CardTypes.monster, new FalseProphetField(),"FalseProphet", "전투의 함성: 상대 뒷면 카드 하나에 잠복 효과가 있으면 파괴하고, 없으면 +3/+3을 부여한다.") 
		{ 
			//잠복 Boolean 인자가 나올때까지 보류
		}


	}
}
