using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class FalseProphetField : FieldObject
	{

		public FalseProphetField(): base(4, 2, CardTypes.monster, "FalseProphet", "돌팔이 점술가", 6, 9, false) 
		{
            TargetBattleCry += (target, thisObject) =>
			{
				if (target.LinkedCard.Stakeout)
				{
					GameAction.DestroyFieldObject(target);
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), target, target,  "Explosion") );
				}
				else
				{
					new StatBuff(target, 3, 3).Initialize();
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), target, target,  "Holy") );
				}
			};
            BattleCryTargetValidityCheck += (target, thisObject) =>
            {
                return target.CardType == CardTypes.mystery && !VChecks.IsMine(thisObject, target);
            };
		}


	}
}
