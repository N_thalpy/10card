using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class FinalChanceCard : Card
	{
		public FinalChanceCard()
			: base(10, 10, "마지막 기회", CardTypes.magic, new MagicBox(), "FinalChance", "내 전장이 비어 있고, 내 손에 다른 카드가 없으면 상대의 모든 하수인을 파괴하고 나의 다른 모든 카드를 회수한다.")
		{
			NonTargetEvent += AllDeleteRetrieve;
		}
		
		public void AllDeleteRetrieve(Card Me)
		{
			bool IsInCase = true;
			
			for(int i = 0; i < Me.Controller.Hand.Count; i++)
			{
				if(Me.Controller.Hand[i] != Me)
					IsInCase = false;
			}
			
			for (int i = 0; i < Me.Controller.Field.Count; i++) 
			{
				if(Me.Controller.Field[i].CardType != CardTypes.empty)
					IsInCase = false;
			}
			
			if (IsInCase) 
			{
				float effectTime = Effect.EFC.EffectLastTime();
				for (int i = 0; i < Me.Opponent.Field.Count; i++)
				{
					FieldObject target = Me.Opponent.Field[i];
					if(target.CardType != CardTypes.empty)
					{
						GameAction.DestroyFieldObject(target);
						Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), target, target,  "Skull") );
					}
				}
				
				List<Card> RetrieveList = new List<Card> ();
				
				for(int i = 0; i < Me.Controller.Grave.Count; i++)
				{
					RetrieveList.Add(Me.Controller.Grave[i]);
				}
				
				while(RetrieveList.Count > 0)
				{
					Me.Controller.Hand.Add(RetrieveList[0]);
					Trigger.RetrieveTrigger(RetrieveList[0]);
					RetrieveList.RemoveAt(0);
					Me.Controller.Grave.RemoveAt(0);
				}
			}
		}
	}
}
