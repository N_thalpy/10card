using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class FireheroCard : Card
	{

        public FireheroCard()
			: base(5, 0, "불꽃 영웅", CardTypes.monster, new FireheroField(), "Firehero", "공격할 때마다 +3/+0을 얻는다. 이 하수인이 죽을 때 상대의 각 하수인에게 자신의 공격력만큼의 피해를 입힌다.",false) 
		{ 

		}


	}
}
