using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class FireheroField : FieldObject
	{
		
		public FireheroField(): base(5, 0, CardTypes.monster, "Firehero", "불꽃 영웅", 9, 14, false,"",false) 
		{ 
			Attack += AtUpBuff;
			Death += AtDamageToEnemyAll;
			Summon += Flame;
		}

		public void Flame ( FieldObject Unit, FieldObject Me )
		{
			if (Unit == Me) 
			{
				Effect.AddEffect (new Effect (1.0f, Effect.EFC.EffectLastTime (), Me, Me, "Holy"));
			}
		}
		
		public void AtUpBuff(FieldObject Attacker, FieldObject Defender, FieldObject Me)
		{
			if ((Attacker == Me) && GameAction.IsFieldObjectOnField(Defender))
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Me, Me,  "Holy") );
				StatBuff AtBuff = new StatBuff(Me, 3, 0);
				AtBuff.Initialize();
			}
		}
		
		public void AtDamageToEnemyAll(FieldObject Unit, FieldObject Me)
		{
			if (Unit == Me) 
			{
				int AllDamage = Me.CurrentAp;
				
				float effectTime = Effect.EFC.EffectLastTime();
				for(int i = 0; i < Me.Opponent.Field.Count; i++)
				{
					FieldObject target = Me.Opponent.Field[i];
					if(target.CardType == CardTypes.monster)
					{
						Effect.AddEffect( new Effect( 1.0f, effectTime , target, target,  "Smoke") );
						GameAction.DealDamage(Me, target, AllDamage);
					}
				}
			}
		}
		
	}
}
