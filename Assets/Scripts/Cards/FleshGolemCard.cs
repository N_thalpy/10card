using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class FleshGolemCard : Card
	{

        public FleshGolemCard()
			: base(3, 2, "살점 골렘", CardTypes.monster, new FleshGolemField(), "FleshGolem", "이 카드가 손에 있는 동안, 전장의 카드가 파괴될 때마다 +1/+1을 얻는다.") 
		{ 
			Death += ApHpBuff;
		}

		public void ApHpBuff(FieldObject Unit, Card Me)
		{
			StatBuff AHBuff = new StatBuff (Me.LinkedFieldObject, 1, 1);
			AHBuff.Initialize ();			
		}


	}
}
