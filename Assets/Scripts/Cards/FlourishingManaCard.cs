using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class FlourishingManaCard : Card
    {
        public FlourishingManaCard()
            : base(0, 2, "넘치는 마나", CardTypes.magic, new MagicBox(), "FlourishingMana", "양 플레이어는 자원을 4씩 얻는다.")
        {
			NonTargetEvent += GainResource;
        }

		public void GainResource(Card Me)
		{
			Me.Controller.Resource += 4;
			Me.Opponent.Resource += 4;

			if (Me.Controller.Resource > Me.Controller.MaxResource)
				Me.Controller.Resource = Me.Controller.MaxResource;

			if (Me.Opponent.Resource > Me.Opponent.MaxResource)
				Me.Opponent.Resource = Me.Opponent.MaxResource;
		}
    }
}
