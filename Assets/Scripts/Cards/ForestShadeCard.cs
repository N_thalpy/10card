using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class ForestShadeCard : Card
    {
        public ForestShadeCard()
            : base(2, 1, "숲의 음지", CardTypes.magic, new MagicBox(), "ForestShade", "양 플레이어는 각자 손에서 비용 2 이하의 하수인을 무작위로 2개 뒷면으로 소환한다.")
        {
			NonTargetEvent += ForcedSummonTwo;
        }

		public void ForcedSummonTwo(Card Me)
		{
			List<Card> FirstPlayerTwoCostMonsterList = new List<Card> ();
			List<Card> SecondPlayerTwoCostMonsterList = new List<Card> ();
			Player FirstPlayer;
			Player SecondPlayer;

			if (GameAction.MainSystem.MePlayer.getNetworkID() == 1) 
			{
				FirstPlayer = GameAction.MainSystem.MePlayer;
				SecondPlayer = GameAction.MainSystem.MePlayer.Opponent;
			}
			else
			{
				FirstPlayer = GameAction.MainSystem.MePlayer.Opponent;
				SecondPlayer = GameAction.MainSystem.MePlayer;
			}

			for (int i = 0; i < FirstPlayer.Hand.Count; i++)
			{
				if(FirstPlayer.Hand[i].CurrentCost < 3)
				{
					if(FirstPlayer.Hand[i].CardType == CardTypes.monster)
						FirstPlayerTwoCostMonsterList.Add (FirstPlayer.Hand[i]);
				}
					
			}

			for (int i = 0; i < SecondPlayer.Hand.Count; i++) 
			{
				if(SecondPlayer.Hand[i].CurrentCost < 3)
				{
					if(SecondPlayer.Hand[i].CardType == CardTypes.monster)
						SecondPlayerTwoCostMonsterList.Add (SecondPlayer.Hand[i]);
				}
			}

			float effectTime = Effect.EFC.EffectLastTime();
			for (int i = 0; i < 2; i++) 
			{
				if (FirstPlayerTwoCostMonsterList.Count > 0) 
				{
					int SummonIndex = GameAction.RandomInt(0,FirstPlayerTwoCostMonsterList.Count);
					int SummonLocation = GameAction.EmptyFieldLocation(FirstPlayer);
					if(SummonLocation != -1)
					{
						GameAction.SummonAtPosition(FirstPlayer.Field[SummonLocation],FirstPlayerTwoCostMonsterList[SummonIndex].LinkedFieldObject,true);
						FirstPlayer.Hand.Remove(FirstPlayerTwoCostMonsterList[SummonIndex]);
						FirstPlayerTwoCostMonsterList.Remove(FirstPlayerTwoCostMonsterList[SummonIndex]);
						Effect.AddEffect( new Effect( 1.0f, effectTime , FirstPlayer.Field[SummonLocation], FirstPlayer.Field[SummonLocation],  "Smoke") );
					}
				}

				if (SecondPlayerTwoCostMonsterList.Count > 0)
				{
					int SummonIndex = GameAction.RandomInt(0,SecondPlayerTwoCostMonsterList.Count);
					int SummonLocation = GameAction.EmptyFieldLocation(SecondPlayer);
					if(SummonLocation != -1)
					{
						GameAction.SummonAtPosition(SecondPlayer.Field[SummonLocation],SecondPlayerTwoCostMonsterList[SummonIndex].LinkedFieldObject,true);
						SecondPlayer.Hand.Remove(SecondPlayerTwoCostMonsterList[SummonIndex]);
						SecondPlayerTwoCostMonsterList.Remove(SecondPlayerTwoCostMonsterList[SummonIndex]);
						Effect.AddEffect( new Effect( 1.0f, effectTime , SecondPlayer.Field[SummonLocation], SecondPlayer.Field[SummonLocation],  "Smoke") );
					}
				}
			}

			GameAction.MainSystem.CardSort ();

		}
    }
}
