using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class GhostKnightCard : Card
	{

        public GhostKnightCard()
            : base(3, 3, "유령 기사", CardTypes.monster, new GhostKnightField(),"GhostKnight", "*미구현*이 하수인은 공격받지 않습니다. 이 하수인은 상대 영웅을 직접 공격할 수 있습니다.") 
		{ 

		}


	}
}
