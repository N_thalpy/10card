using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class GodsPunishmentCard : Card
    {
        public GodsPunishmentCard()
            : base(3, 1, "신벌", CardTypes.magic, new MagicBox(), "GodsPunishment", "각자 전장의 무작위 하수인 하나를 파괴한다.")
        {
            NonTargetEvent += (me) =>
			{
				float effectTime = Effect.EFC.EffectLastTime();

				List<FieldObject> targetCandidates = new List<FieldObject>();
				FieldObject fo = null;
				for (int i = 0; i < 5; i++)
				{
					fo = me.Controller.Field[i];
					if (fo.CardType == CardTypes.monster)
						targetCandidates.Add(fo);
				}
				if (targetCandidates.Count > 0)
				{
					FieldObject target = targetCandidates[GameAction.RandomInt(0, targetCandidates.Count)];
					GameAction.DestroyFieldObject(target);
					Effect.AddEffect( new Effect( 1.0f, effectTime, target, target,  "Explosion") );
				}

				targetCandidates = new List<FieldObject>();
				for (int i = 0; i < 5; i++)
				{
					fo = me.Opponent.Field[i];
					if (fo.CardType == CardTypes.monster)
						targetCandidates.Add(fo);
				}
				if (targetCandidates.Count > 0)
				{
					FieldObject target = targetCandidates[GameAction.RandomInt(0, targetCandidates.Count)];
					GameAction.DestroyFieldObject(targetCandidates[GameAction.RandomInt(0, targetCandidates.Count)]);
					Effect.AddEffect( new Effect( 1.0f, effectTime, target, target,  "Explosion") );
				}
			};
        }
    }
}
