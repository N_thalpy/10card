﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class GozCard : Card
    {
        public GozCard()
            : base(8, 0, "명계의 사신", CardTypes.monster, new GozField(), "Goz","내가 9이상의 피해를 입었을 때 손에서 소환된다. 받은 피해를 공격력과 체력으로 가진 악마를 같이 소환한다.", false)
        {
            Damaged += (FieldObject DamageTaker, FieldObject DamageGiver, int DamageAmount, Card MeCard) =>
                {
                    if (DamageTaker == MeCard.Controller &&  DamageAmount >= 9)
                    {
                        int i;

                        for (i = 0; i < 5; i++)
                        {
                            if (MeCard.Controller.Field[i].CardType == CardTypes.empty)
                            {
                                MeCard.Controller.Hand.Remove(MeCard);
                                GameAction.MainSystem.CardSort();
                                GameAction.SummonAtPosition(MeCard.Controller.Field[i], LinkedFieldObject);
								Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , LinkedFieldObject, LinkedFieldObject,  "Skull") );
                                break;
                            }
                        }

                        for (int j = i; j < 5; j++)
                        {
                            if (MeCard.Controller.Field[j].CardType == CardTypes.empty)
                            {
                                Goz_LesserDemonField demonField = new Goz_LesserDemonField(DamageAmount);
                                demonField.Controller = MeCard.Controller;
                                demonField.Opponent = MeCard.Opponent;
                                GameAction.SummonAtPosition(MeCard.Controller.Field[j], demonField);
								Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , demonField, demonField,  "Skull") );
                                break;
                            }
                        }


                    }
                    return DamageAmount;
                };
        }
    }
}
