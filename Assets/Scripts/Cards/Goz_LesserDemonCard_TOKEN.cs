﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class Goz_LesserDemonCard : Card
    {
        public Goz_LesserDemonCard()
            : base(0, 0, "악마", CardTypes.monster, new Goz_LesserDemonField(0), "Goz_LesserDemon")
        {
        }
    }
}
