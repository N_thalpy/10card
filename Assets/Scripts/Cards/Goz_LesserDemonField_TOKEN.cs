﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class Goz_LesserDemonField : FieldObject
    {
        public Goz_LesserDemonField(int Damage)
            : base(0, 0, CardTypes.monster, "Goz_LesserDemon", "악마", Damage, Damage, true)
        {

        }
    }
}
