using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class GreatwizardCard : Card
	{

        public GreatwizardCard()
            : base(8, 0, "대마법사", CardTypes.monster, new GreatwizardField(), "Greatwizard", "자신이 마법을 사용할 시, 무작위 적 필드 유닛에게 8의 피해를 입힌다. ", false, false) 
		{ 

		}


	}
}
