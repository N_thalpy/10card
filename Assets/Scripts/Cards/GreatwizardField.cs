using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class GreatwizardField : FieldObject
	{
		
		public GreatwizardField(): base(8, 0, CardTypes.monster, "Greatwizard", "대마법사", 9, 18, false, "", false) 
		{ 
			Spell += GreatWisdom;
			SpellTarget += GreatWisdomTarget;
		}
		
		public void GreatWisdom(Card Spell, FieldObject Me )
		{
			if(Spell.Controller == Me.Controller && Me.CardType != CardTypes.mystery  )
			{
				List<FieldObject> TargetList = new List<FieldObject>();

				for(int i = 0; i < Me.Opponent.Field.Count; i++)
				{
					if(Me.Opponent.Field[i].CardType == CardTypes.monster && Me.Opponent.Field[i].CardType != CardTypes.empty) TargetList.Add( Me.Opponent.Field[i]);
				}
				if ( TargetList.Count > 0 )
				{
					FieldObject Target = TargetList[GameAction.RandomInt(0, TargetList.Count)];
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , Me, Target, "Explosion" ) );
					GameAction.DealDamage(Me, Target, 8);
				}
			}			
		}
		public FieldObject GreatWisdomTarget(Card Spell, FieldObject Me, FieldObject Target)
		{
			if(Spell.Controller == Me.Controller && Me.CardType != CardTypes.mystery  )
			{
				List<FieldObject> TargetList = new List<FieldObject>();
				
				for(int i = 0; i < Me.Opponent.Field.Count; i++)
				{
					if(Me.Opponent.Field[i].CardType == CardTypes.monster && Me.Opponent.Field[i].CardType != CardTypes.empty ) TargetList.Add( Me.Opponent.Field[i]);
				}
				if ( TargetList.Count > 0 )
				{
					FieldObject FireBallTarget = TargetList[GameAction.RandomInt(0, TargetList.Count)];
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , Me, FireBallTarget, "Explosion" ) );
					GameAction.DealDamage(Me, FireBallTarget, 8);
				}

			}
			return Target;
		}
		
	}
}
