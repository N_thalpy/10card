using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class GreedyGoblinCard : Card
	{

        public GreedyGoblinCard()
            : base(2, 1, "탐욕스런 고블린", CardTypes.monster, new GreedyGoblinField(),"GreedyGoblin", "전투의 함성: 내 자원을 모두 없앤다. 죽을 때 그만큼의 자원을 돌려받는다.") 
		{ 

		}


	}
}
