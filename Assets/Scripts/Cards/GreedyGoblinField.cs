using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class GreedyGoblinField : FieldObject
	{
        int greed = 0;

		public GreedyGoblinField(): base(2, 1, CardTypes.monster, "GreedyGoblin", "탐욕스런 고블린", 5, 7, false) 
		{
            NonTargetBattleCry += (me) =>
                {
                    greed = me.Controller.Resource;
                    me.Controller.Resource = 0;
                };
            Death += (unit, me) =>
                {
                    if (unit == me)
                        me.Controller.Resource += greed;
                };
		}


	}
}
