﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class GrowthCard : Card
    {
        public GrowthCard()
            : base(3, 2, "성장", CardTypes.magic, new MagicBox(), "Growth", "아군 전체에게 +2/+2를 부여한다.")
        {
            NonTargetEvent += (Card Me) =>
			{
				float effectTime = Effect.EFC.EffectLastTime();
				for (int i = 0; i < 5; i++)
				{
					FieldObject target = Me.Controller.Field[i];
					if (target.CardType == CardTypes.monster)
					{
						Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), target, target,  "Skull") );
						new StatBuff(target, 2, 2).Initialize();
					}
				}
			};
        }
    }
}
