using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class GuardianKnightCard : Card
	{

        public GuardianKnightCard()
            : base(3, 1, "근위 기사", CardTypes.monster, new GuardianKnightField(),"GuardianKnight", "자신의 전장이 비었고 자원이 1 이상일 때 상대에게 공격받으면 손에서 비용 1로 소환된다.") 
		{
            
            Attack += (FieldObject Attacker, FieldObject Defender, Card MeCard) =>
            {
                for (int i = 0; i < 5; i++)
                {
                    if (MeCard.Controller.Field[i].CardType != CardTypes.empty) return;
                }

                if (Defender == MeCard.Controller && MeCard.Controller.Resource >= 1)
                {
					int Location = GameAction.EmptyFieldLocation(MeCard.Controller);
					if(Location != -1)
					{
						MeCard.Controller.Resource -= 1;
						GameAction.MainSystem.ReplaceCardInHandWithEmpty(MeCard);
						GameAction.SummonAtPosition(MeCard.Controller.Field[Location], LinkedFieldObject);
						
						GameAction.MainSystem.CardSort();
					}
                }
            };
		}


	}
}
