﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class HallowedSpiritCard : Card
    {
        public HallowedSpiritCard()
            : base(3, 6, "성령", CardTypes.monster, new HallowedSpiritField(), "HallowedSpirit", "매 턴 시작마다 내 체력을 4씩 회복시킨다.")
        {

        }
    }
}
