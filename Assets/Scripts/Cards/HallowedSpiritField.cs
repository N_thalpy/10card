﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class HallowedSpiritField : FieldObject
    {
        public HallowedSpiritField()
            : base(3, 6, CardTypes.monster, "HallowedSpirit", "성령", 6, 6)
        {
            TurnStart += (thisObject) =>
			{
				if(thisObject.CardType != CardTypes.mystery)
				{
					GameAction.Heal(thisObject.Controller, 4);
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), thisObject, thisObject,  "Heal") );
				}
			};
        }
    }
}
