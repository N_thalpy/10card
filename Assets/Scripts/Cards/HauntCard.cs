﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class HauntCard : Card
    {
        public HauntCard()
            : base(4, 3, "빙의", CardTypes.magic, new MagicBox(), "Haunt", "잠복: 이것 또는 내가 공격받으면, 공격 수행 이후 다음 내 턴 끝까지 공격자를 우리편으로 만든다.",true)
        {
            LinkedFieldObject.Attack += Haunt;
        }

        Player meplayer = null;
        Buff ChangeMindBuff = null;

        void Haunt(FieldObject Attacker, FieldObject Defender, FieldObject Me)
        {
            if (!GameAction.IsFieldObjectOnField(Attacker)) return;

            if (Me.CardType == CardTypes.mystery && (Defender == Me || Defender == Me.Controller))
            {
                if (Defender != Me) GameAction.Reverse(Me);
                ChangeMind(Attacker, Me);
                meplayer = Me.Controller;
                Attacker.TurnEnd += ExpireAtMyEOT;
            }            
        }

        Player myController = null;
        Player origController = null;
        void ChangeMind(FieldObject Target, FieldObject thisObject)
        {
            myController = thisObject.Controller;
            origController = Target.Controller;
            ChangeMindBuff = new Buff(Target, ChangeMindinit, ChangeMindEnd);
            ChangeMindBuff.Initialize();
        }

        void ChangeMindinit(Buff thisBuff)
        {
            GameAction.GiveControl(thisBuff.BuffTarget, myController);
            thisBuff.BuffTarget.Activity = true;
        }

        void ChangeMindEnd(Buff thisBuff)
        {
            if (!GameAction.IsFieldObjectOnField(thisBuff.BuffTarget)) return;
            GameAction.GiveControl(thisBuff.BuffTarget, origController);
            thisBuff.BuffTarget.Activity = true;
        }

        void ExpireAtMyEOT(FieldObject thisObject)
        {
            if (meplayer.Turn)
            {
                ChangeMindBuff.Terminate();
                thisObject.TurnEnd -= ExpireAtMyEOT;
            }
        }
    }
}
