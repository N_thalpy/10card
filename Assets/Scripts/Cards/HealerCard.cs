using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class HealerCard : Card
	{

        public HealerCard()
            : base(1, 1, "치료사", CardTypes.monster, new HealerField(), "Healer", "전투의 함성: 앞면 하수인 또는 플레이어 하나를 4만큼 회복시킨다.") 
		{ 

		}


	}
}
