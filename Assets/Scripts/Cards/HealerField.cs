using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class HealerField : FieldObject
	{
		
		public HealerField(): base(1, 1, CardTypes.monster, "Healer", "치료사", 3, 5) 
		{
			TargetBattleCry += (target, me) =>
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime() , target, target,  "Smoke") );
				GameAction.Heal(target, 4);
			};
			//유효성 검사기는 기본을 씀.
		}
	}
}
