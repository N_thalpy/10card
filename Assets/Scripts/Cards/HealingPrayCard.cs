using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class HealingPrayCard : Card
    {
        public HealingPrayCard()
            : base(2, 1, "회복의 기도", CardTypes.magic, new MagicBox(), "HealingPray", "하수인 하나와 나의 체력을 6 회복시킨다.")
        {
            TargetEvent += (target, me) =>
			{
				float effectTime = Effect.EFC.EffectLastTime();
				GameAction.Heal(target, 6);
				Effect.AddEffect( new Effect( 1.0f, effectTime , target, target,  "Smoke") );
				GameAction.Heal(me.Controller, 6);
				Effect.AddEffect( new Effect( 1.0f, effectTime , me.Controller, me.Controller,  "Smoke") );
			};

            TargetValidityCheck += (target, me) =>
			{
				return VChecks.IsMonsterNotMyetery(target);
			};

        }
    }
}
