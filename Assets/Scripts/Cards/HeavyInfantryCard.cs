using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class HeavyInfantryCard : Card
	{

        public HeavyInfantryCard()
            : base(3, 1, "중무장 보병", CardTypes.monster, new HeavyInfantryField(),"HeavyInfantry", "피해를 입게 될 때, 그 피해량을 0으로 만들고 이 능력을 없앤다.") 
		{ 

		}


	}
}
