using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class HeavyInfantryField : FieldObject
	{
		
		public HeavyInfantryField(): base(3, 1, CardTypes.monster, "HeavyInfantry", "중무장 보병", 6, 8, false) 
		{
			Damaged += AbsorbDamage;
		}
		
		int AbsorbDamage(FieldObject DamageTaker, FieldObject DamageGiver, int DamageAmount, FieldObject Me)
		{
			if (Me == DamageTaker)
			{
				Effect.AddEffect( new Effect( 0.5f, Effect.EFC.EffectLastTime(), DamageTaker, DamageTaker,  "Shield") );
				Me.Damaged -= AbsorbDamage;
				return 0;
			}
			else return DamageAmount;
		}
	}
}
