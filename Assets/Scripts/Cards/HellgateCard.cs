﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class HellgateCard : Card
    {
        public HellgateCard()
            : base(7, 6, "지옥의 문", CardTypes.monster, new HellgateField(), "Hellgate", "내 턴 시작마다, 모든 하수인에게 6의 피해를 입힌다.")
        {

        }
    }
}
