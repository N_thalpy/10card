﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class HellgateField : FieldObject
    {
        public HellgateField()
            : base(7, 6, CardTypes.monster, "Hellgate", "지옥의 문", 0, 27)
        {
            TurnStart += (FieldObject Me) =>
			{
				if(Me.CardType != CardTypes.mystery)
				{
					if (Me.Controller.Turn)
					{
						float effectTime = Effect.EFC.EffectLastTime();
						for (int i = 0; i < 5; i++)
						{
							if (Me.Controller.Field[i].CardType == CardTypes.monster || Me.Controller.Field[i].CardType == CardTypes.mystery)
							{
								Effect.AddEffect( new Effect( 1.0f, effectTime, Me.Controller.Field[i], Me.Controller.Field[i],  "Explosion") );
								GameAction.DealDamage(Me, Me.Controller.Field[i], 6);
							}
							if (Me.Opponent.Field[i].CardType == CardTypes.monster || Me.Opponent.Field[i].CardType == CardTypes.mystery)
							{
								Effect.AddEffect( new Effect( 1.0f, effectTime, Me.Opponent.Field[i], Me.Opponent.Field[i],  "Explosion") );
								GameAction.DealDamage(Me, Me.Opponent.Field[i], 6);
							}
						}
					}
				}
				
			};
        }
    }
}
