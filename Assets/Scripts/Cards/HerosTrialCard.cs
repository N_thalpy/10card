using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class HerosTrialCard : Card
    {
        public HerosTrialCard()
            : base(2, 2, "용사의 시련", CardTypes.magic, new MagicBox(), "HerosTrial", "하수인 하나의 공격력과 체력을 각각 3~6 중 무작위 수치만큼 감소시킨다. 이후 두 배로 만든다.")
        {
            int deltaAp = 0;
            int deltaHp = 0;
            int deltaMaxHp = 0;

           	TargetEvent += (target, thisCard) =>
			{
				int rint1 = GameAction.RandomInt(3, 7), rint2 = GameAction.RandomInt(3, 7);
				new StatBuff(target, -Math.Min(target.CurrentAp, rint1), -rint2).Initialize();
				if (GameAction.IsFieldObjectOnField(target))
					new Buff(target,
					 (thisBuff) =>
					 {
						deltaAp = thisBuff.BuffTarget.CurrentAp;
						deltaHp = thisBuff.BuffTarget.CurrentHp;
						deltaMaxHp = thisBuff.BuffTarget.CurrentMaxHp;
						thisBuff.BuffTarget.CurrentAp += deltaAp;
						thisBuff.BuffTarget.CurrentMaxHp += deltaMaxHp;
						thisBuff.BuffTarget.CurrentHp += deltaHp;
						if (thisBuff.BuffTarget.CurrentHp <= 0)
							GameAction.DestroyFieldObject(thisBuff.BuffTarget);
						Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), target, target,  "Wind") );
					},
					(thisBuff) =>
					{
						thisBuff.BuffTarget.CurrentAp -= deltaAp;
						thisBuff.BuffTarget.CurrentMaxHp -= deltaMaxHp;
						thisBuff.BuffTarget.CurrentHp = Math.Min(thisBuff.BuffTarget.CurrentHp, thisBuff.BuffTarget.CurrentMaxHp);
						Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), target, target,  "Holy") );
					}).Initialize();
			};
            TargetValidityCheck += (target, thisCard) =>
			{
				return VChecks.IsMonsterNotMyetery(target);
			};
        }
    }
}
