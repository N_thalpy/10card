using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class HousefairyCard : Card
	{

        public HousefairyCard()
			: base(1, 0, "집요정", CardTypes.monster, new HousefairyField(), "Housefairy", "전투의 함성: 내 전장에 다른 카드가 없으면 소환 비용을 돌려받는다.") 
		{ 

		}

		
	}
}
