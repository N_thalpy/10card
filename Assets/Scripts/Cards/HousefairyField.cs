using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class HousefairyField : FieldObject
	{
		
		
		public HousefairyField()
			: base(1, 0, CardTypes.monster, "Housefairy", "집요정", 3, 5)
		{
			NonTargetBattleCry += (me) =>
			{
				for (int i = 0; i < 5; i++)
					if (me.Controller.Field[i].CardType != CardTypes.empty && me.Controller.Field[i] != me) return;
				
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), me, me,  "Holy") );
				me.Controller.Resource += me.LinkedCard.CurrentCost;
			};
			
		}
		
		
		
	}
}
