﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class InfernalMageCard : Card
    {
        public InfernalMageCard()
            : base(6, 3, "지옥의 마도사", CardTypes.monster, new InfernalMageField(), "InfernalMage", "매 턴 시작마다 무작위로 카드 한 장을 버린다. 이후 손에 카드가 없으면 +8/+8을 받는다.")
        {
        }
    }
}
