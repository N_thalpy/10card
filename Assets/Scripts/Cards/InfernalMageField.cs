﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class InfernalMageField : FieldObject
    {
        public InfernalMageField()
            : base(6, 3, CardTypes.monster, "InfernalMage", "지옥의 마도사", 11, 11)
        {
            TurnStart += (me) =>
			{
				if(me.CardType != CardTypes.mystery)
				{
					if (me.Controller.Hand.Count > 0)
					{
						Card randCard = me.Controller.Hand[GameAction.RandomInt(0, me.Controller.Hand.Count)];
						
						me.Controller.Hand.Remove(randCard);
						if (randCard.Recyclable)
						{
							Card newCard = (Card)Activator.CreateInstance(randCard.GetType());
							newCard.Controller = randCard.Controller;
							newCard.Opponent = randCard.Opponent;
							newCard.LinkedFieldObject.Controller = newCard.Controller;
							newCard.LinkedFieldObject.Opponent = newCard.Opponent;
							
							me.Controller.Grave.Add(newCard);
						}
					}
					if (me.Controller.Hand.Count == 0)
					{
						new StatBuff(me, 8, 8).Initialize();
						Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), me, me,  "Holy") );
					}
				}
			};
        }
    }
}
