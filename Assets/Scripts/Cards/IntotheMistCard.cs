﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class IntotheMistCard : Card
	{
		public IntotheMistCard()
			: base(3, 5, "안개 속으로", CardTypes.magic, new MagicBox(), "IntotheMist", "앞면 또는 뒷면 카드 하나를 주인의 손으로 돌린다.")
		{
			TargetValidityCheck += IntotheMistTargetValidityCheck;
			TargetEvent = delegate(FieldObject Target, Card ThisCard)
			{
				Effect.AddEffect( new Effect( 2.5f, Effect.EFC.EffectLastTime(), Target, Target,  "BlackSmoke") );
				GameAction.ReturnToHand(Target);
			};
		}
		public bool IntotheMistTargetValidityCheck(FieldObject Target, Card thisCard)
		{
			return (Target.CardType != CardTypes.player && Target.CardType != CardTypes.empty);
		}
	}
}
