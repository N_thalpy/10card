using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class KingsDownfallCard : Card
    {
        public KingsDownfallCard()
            : base(6, 2, "왕의 몰락", CardTypes.magic, new MagicBox(), "KingsDownfall", "적 전장의 각 하수인에 -x/-x를 부여한다. x는 적 하수인의 비용 중 가장 높은 값이다.")
        {
            NonTargetEvent += (thisCard) =>
			{
				int expensive = 0;
				FieldObject fo;

				for (int i = 0; i < 5; i++)
				{
					fo = thisCard.Opponent.Field[i];
					if (fo.CardType == CardTypes.monster)
					{
						if (expensive < fo.CurrentCost)
							expensive = fo.CurrentCost;
					}
				}
				
				if (expensive > 0)
				{
					float effectTime = Effect.EFC.EffectLastTime();
					for (int i = 0; i < 5; i++)
					{
						fo = thisCard.Opponent.Field[i];
						if (fo.CardType == CardTypes.monster)
						{
							new StatBuff(fo, -expensive, -expensive).Initialize();
							Effect.AddEffect(new Effect(1.0f, effectTime, fo, fo, "Skull"));
						}
					}
				}
			};
        }
    }
}
