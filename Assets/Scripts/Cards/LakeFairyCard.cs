using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class LakeFairyCard : Card
	{

        public LakeFairyCard()
            : base(4, 2, "호수의 요정", CardTypes.monster, new LakeFairyField(),"LakeFairy", "전투의 함성: 하수인 하나에게 +5/+0을 부여한다.") 
		{ 

		}


	}
}
