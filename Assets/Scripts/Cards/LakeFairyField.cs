using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class LakeFairyField : FieldObject
	{
		
		public LakeFairyField(): base(4, 2, CardTypes.monster, "LakeFairy", "호수의 요정", 6, 8, false) 
		{
			TargetBattleCry += GivePlusBuff;
			BattleCryTargetValidityCheck += LFTargetValidityCheck;
		}
		
		void GivePlusBuff(FieldObject target, FieldObject thisObject)
		{
			if (!GameAction.IsFieldObjectOnField(target)) return;
			StatBuff statBuff = new StatBuff(target, 5, 0, false);
			statBuff.Initialize();
			Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), target, target,  "Holy") );
		}
		
		public bool LFTargetValidityCheck(FieldObject Target, FieldObject thisObject)
		{
			
			return VChecks.IsMonsterNotMyetery(Target) && VChecks.IsMine(thisObject, Target);
		}
		
		
	}
}
