using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class LastFeatCard : Card
    {
        public LastFeatCard()
            : base(2, 0, "마지막 위업", CardTypes.magic, new MagicBox(), "LastFeat", "최저 비용 아군 하수인과 최고 비용 적 하수인을 파괴한 뒤, 비용 차이만큼 아군 하수인 전체의 공/체를 올린다.", false)
        {
            NonTargetEvent += (thisCard) =>
			{
				FieldObject cheapestMon = null;
				FieldObject expensiveMon = null;
				FieldObject fo;
				
				for (int i = 0; i < 5; i++)
				{
					fo = thisCard.Controller.Field[i];
					if (fo.CardType == CardTypes.monster)
					{
						if (cheapestMon == null || cheapestMon.CurrentCost > fo.CurrentCost)
							cheapestMon = fo;
					}
				}
				
				for (int i = 0; i < 5; i++)
				{
					fo = thisCard.Opponent.Field[i];
					if (fo.CardType == CardTypes.monster)
					{
						if (expensiveMon == null || expensiveMon.CurrentCost < fo.CurrentCost)
							expensiveMon = fo;
					}
					
				}

				float effectTime = Effect.EFC.EffectLastTime();
				Effect.AddEffect( new Effect( 1.0f, effectTime , cheapestMon, cheapestMon,  "Explosion") );
				Effect.AddEffect( new Effect( 1.0f, effectTime , expensiveMon, expensiveMon,  "Explosion") );

				if (GameAction.IsFieldObjectOnField(cheapestMon)) GameAction.DestroyFieldObject(cheapestMon);
				if (GameAction.IsFieldObjectOnField(expensiveMon)) GameAction.DestroyFieldObject(expensiveMon);
				
				if (expensiveMon == null || cheapestMon == null ) return;
				int dif = Math.Abs(expensiveMon.CurrentCost - cheapestMon.CurrentCost);
				
				for (int i = 0; i < 5; i++)
				{
					fo = thisCard.Controller.Field[i];
					if (fo.CardType == CardTypes.monster)
					{
						new StatBuff(fo, dif, dif).Initialize();
					}
				}
				
			};
        }
    }
}
