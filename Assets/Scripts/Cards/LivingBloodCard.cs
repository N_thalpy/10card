using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class LivingBloodCard : Card
	{
		
		public LivingBloodCard()
			: base(3, 2, "살아있는 피", CardTypes.monster, new LivingBloodField(),"LivingBlood", "내가 자원이 3 이상일 때 상대 턴에 3 이상의 피해를 입으면 손에서 비용 3으로 소환되어 그 피해만큼을 공/체로 가진다.") 
		{
			Damaged += (FieldObject DamageTaker, FieldObject DamageGiver, int DamageAmount, Card MeCard) =>
			{
				if (DamageTaker == MeCard.Controller && DamageAmount >= 3 && MeCard.Controller.Resource >= 3 && MeCard.Opponent.Turn)
				{
					int i;
					
					for (i = 0; i < 5; i++)
					{
						FieldObject target = MeCard.Controller.Field[i];
						if (target.CardType == CardTypes.empty)
						{
							Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), target, target,  "Blood") );
							
							MeCard.Controller.Hand.Remove(MeCard);
							GameAction.MainSystem.CardSort();
							GameAction.SummonAtPosition(target, LinkedFieldObject);
							MeCard.Controller.Resource -= 3;
							LinkedFieldObject.CurrentMaxHp = LinkedFieldObject.CurrentHp = LinkedFieldObject.CurrentAp = DamageAmount;
							
							break;
						}
					}
				}
				return DamageAmount;
			};
		}
		
		
	}
}
