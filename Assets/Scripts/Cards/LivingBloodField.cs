using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class LivingBloodField : FieldObject
	{

		public LivingBloodField(): base(3, 2, CardTypes.monster, "LivingBlood", "살아있는 피", 0, 1, false) 
		{ 

		}


	}
}
