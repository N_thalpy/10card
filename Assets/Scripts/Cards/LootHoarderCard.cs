using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class LootHoarderCard : Card
	{

        public LootHoarderCard()
            : base(1, 3, "전리품 수집가", CardTypes.monster, new LootHoarderField(),"LootHoarder", "전장의 하수인이 죽을 때마다 내 자원이 1 증가한다.") 
		{ 

		}


	}
}
