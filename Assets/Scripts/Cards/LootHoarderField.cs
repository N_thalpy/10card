using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class LootHoarderField : FieldObject
	{

		public LootHoarderField(): base(1, 3, CardTypes.monster, "LootHoarder", "전리품 수집가", 1, 5) 
		{
            Death += Collect;
        }

        void Collect(FieldObject unit, FieldObject me)
        {
            if ((unit.LinkedCard == null || unit.LinkedCard.CardType != CardTypes.magic) && me.CardType != CardTypes.mystery)
			{
				if(unit != me)
				{
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), me, me,  "Shield") );
					me.Controller.Resource += 1;
				}

        	}
		}


	}
}
