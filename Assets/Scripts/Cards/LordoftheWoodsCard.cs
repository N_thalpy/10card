﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class LordoftheWoodsCard : Card
    {
        public LordoftheWoodsCard()
            : base(6, 2, "숲의 지배자", CardTypes.monster, new LordoftheWoodsField(), "LordoftheWoods", "전투의 함성: 양 옆에 3/1 나무정령을 소환한다. \n (비지 않은 칸에는 소환되지 않는다.)")
        {
        }
    }
}
