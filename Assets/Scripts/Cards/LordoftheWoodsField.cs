﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class LordoftheWoodsField : FieldObject
    {
        public LordoftheWoodsField()
            : base(6, 2, CardTypes.monster, "LordoftheWoods", "숲의 지배자", 4, 9)
        {
            NonTargetBattleCry += (FieldObject Me) =>
			{
				LordoftheWoods_WoodElementalField we;
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Me, Me, "Holy") );

				if (Me.GetPosition() - 1 >= 0 && Me.Controller.Field[Me.GetPosition() - 1].CardType == CardTypes.empty)
				{
					we = new LordoftheWoods_WoodElementalField();
					we.Controller = Me.Controller;
					we.Opponent = Me.Opponent;
					GameAction.SummonAtPosition(Me.Controller.Field[Me.GetPosition() - 1], we);
				}
				if (Me.GetPosition() + 1 < 5 && Me.Controller.Field[Me.GetPosition() + 1].CardType == CardTypes.empty)
				{
					we = new LordoftheWoods_WoodElementalField();
					we.Controller = Me.Controller;
					we.Opponent = Me.Opponent;
					GameAction.SummonAtPosition(Me.Controller.Field[Me.GetPosition() + 1], we);
				}
			};
        }
    }
}
