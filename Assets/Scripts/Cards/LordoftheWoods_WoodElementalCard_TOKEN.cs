﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class LordoftheWoods_WoodElementalCard : Card
    {
        public LordoftheWoods_WoodElementalCard()
            : base(0, 0, "나무 정령", CardTypes.monster, new LordoftheWoods_WoodElementalField(), "LordoftheWoods_WoodElemental")
        {
        }
    }
}
