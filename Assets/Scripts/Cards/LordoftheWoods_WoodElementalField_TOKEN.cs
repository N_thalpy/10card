﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class LordoftheWoods_WoodElementalField : FieldObject
    {
        public LordoftheWoods_WoodElementalField()
            : base(0, 0, CardTypes.monster, "LordoftheWoods_WoodElemental", "나무 정령", 3, 1, true)
        {
        }
    }
}
