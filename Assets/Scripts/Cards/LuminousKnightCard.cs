using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class LuminousKnightCard : Card
	{

        public LuminousKnightCard()
            : base(2, 2, "광휘의 기사", CardTypes.monster, new LuminousKnightField(), "*미구현*전장에 나올 때 뒷면 카드 하나를 목표로 정합니다. 그 리버스 효과를 무효화하고 앞면으로 만듭니다.") 
		{ 

		}


	}
}
