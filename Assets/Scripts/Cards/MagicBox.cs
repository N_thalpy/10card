﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class MagicBox : FieldObject
	{
		public MagicBox() : base(0, 1000, CardTypes.monster, "MagicBox", "MagicBox" , 0, 1) 
        {
            Reverse += DestroyOnReverse;
        }

        void DestroyOnReverse(FieldObject unit, FieldObject thisObject, FieldObject attacker)
        {
            if (unit == thisObject && GameAction.IsFieldObjectOnField(thisObject))
                GameAction.DestroyFieldObject(thisObject);
        }
	}
}
