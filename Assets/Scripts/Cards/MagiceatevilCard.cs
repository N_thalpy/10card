using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class MagiceatevilCard : Card
	{

        public MagiceatevilCard()
            : base(6, 0, "마법을 먹는 마수", CardTypes.monster, new MagiceatevilField(), "Magiceatevil", "플레이어가 마법을 사용할 때마다 체력을 전부 회복하고 공격력이 2 증가한다.", false, false) 
		{ 

		}


	}
}
