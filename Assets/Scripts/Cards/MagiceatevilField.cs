using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class MagiceatevilField : FieldObject
	{
		
		public MagiceatevilField(): base(6, 0, CardTypes.monster, "Magiceatevil", "마법을 먹는 마수", 8, 12, false, "", false) 
		{ 
			Spell += SpellEatNonTarget;
			SpellTarget += SpellEatTarget;
			
		}
		
		public void SpellEatNonTarget(Card Spell, FieldObject Me)
		{
			if (Me.CardType != CardTypes.mystery) 
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Me, Me,  "Holy") );
				Me.CurrentHp = Me.CurrentMaxHp;
				
				StatBuff AtBuff = new StatBuff (Me, 2, 0);
				AtBuff.Initialize ();
			}
		}
		
		public FieldObject SpellEatTarget(Card Spell, FieldObject Me, FieldObject Target)
		{
			if (Me.CardType != CardTypes.mystery) 
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Me, Me,  "Holy") );
				Me.CurrentHp = Me.CurrentMaxHp;
				
				StatBuff AtBuff = new StatBuff (Me, 2, 0);
				AtBuff.Initialize ();
			}
			
			return Target;
		}
	}
}
