using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class MagicianCard : Card
	{

        public MagicianCard()
            : base(3, 0, "마술사", CardTypes.monster, new MagicianField(), "Magician", "내 턴 끝마다 내 전장의 모든 카드를 뒷면으로 바꿔 섞는다. 잠복 : 내 턴 시작에 상대의 앞면 하수인 카드 하나를 파괴한다.", false, true) 
		{ 

		}


	}
}
