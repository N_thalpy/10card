using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class MagicianField : FieldObject
	{
		
		public MagicianField(): base(3, 0, CardTypes.monster, "Magician", "마술사", 4, 7, false, "", false) 
		{
			TurnStart += KillMystery;
			TurnEnd += AlltoMystery;
		}
		
		public void AlltoMystery(FieldObject Me)
		{
			if (Me.Controller.Turn) 
			{
				if(Me.CardType == CardTypes.monster)
				{
					Player FirstPlayer = Me.Controller;
					Player SecondPlayer = Me.Opponent;
					List<FieldObject> FirstPlayerFieldList = new List<FieldObject> ();
					List<FieldObject> SecondPlayerFieldList = new List<FieldObject> ();
					int RndNum = 0;
					
					while(FirstPlayer.Field.Count > 0)
					{
						RndNum = GameAction.RandomInt(0,FirstPlayer.Field.Count);
						
						if((FirstPlayer.Field[RndNum].CardType != CardTypes.mystery) && (FirstPlayer.Field[RndNum].CardType != CardTypes.empty))
							FirstPlayer.Field[RndNum].CardType = CardTypes.mystery;
						
						FirstPlayerFieldList.Add (FirstPlayer.Field[RndNum]);
						FirstPlayer.Field.Remove(FirstPlayer.Field[RndNum]);
					}
					
					for(int i = 0; i < FirstPlayerFieldList.Count; i++)
					{
						FirstPlayer.Field.Add(FirstPlayerFieldList[i]);
						GameAction.MainSystem.FieldToUI (FirstPlayer.Field[i]).GetComponent<UIChanger> ().PlayerField = FirstPlayerFieldList[i];
					}
					
					float effectTime = Effect.EFC.EffectLastTime();
					for (int i = 0; i < GameAction.MainSystem.MyField.Length; i++) 
					{
						GameAction.MainSystem.MyField[i].GetComponent<UIChanger>().PlayerField = GameAction.MainSystem.MePlayer.Field[i];
						Effect.AddEffect( new Effect( 1.0f, effectTime , GameAction.MainSystem.MePlayer.Field[i], GameAction.MainSystem.MePlayer.Field[i],  "Smoke") );
					}
					
					for (int i = 0; i < GameAction.MainSystem.OpponentField.Length; i++) 
					{
						GameAction.MainSystem.OpponentField[i].GetComponent<UIChanger>().PlayerField = GameAction.MainSystem.OpponentPlayer.Field[i];
					}
				}
			}
			
		}
		
		public void KillMystery(FieldObject Me)
		{
			if (Me.Controller.Turn) 
			{
				if (Me.CardType == CardTypes.mystery) 
				{
					
					List<FieldObject> OpponentPlayerMonsterList = new List<FieldObject> ();
					int RndNum = 0;
					
					for(int i = 0; i < Me.Opponent.Field.Count; i++)
					{
						if(Me.Opponent.Field[i].CardType == CardTypes.monster)
							OpponentPlayerMonsterList.Add (Me.Opponent.Field[i]);
					}
					
					if (OpponentPlayerMonsterList.Count == 0) return;
					
					if (GameAction.IsFieldObjectOnField(Me)) GameAction.Reverse(Me);
					
					RndNum = GameAction.RandomInt(0,OpponentPlayerMonsterList.Count);
					
					GameAction.DestroyFieldObject(OpponentPlayerMonsterList[RndNum]);
				}
			}
		}
	}
}
