using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ManaDevourerCard : Card
	{

        public ManaDevourerCard()
            : base(3, 1, "마나 아귀", CardTypes.monster, new ManaDevourerField(),"ManaDevourer", "매 턴 시작마다 양 플레이어의 자원을 1씩 줄인다. 줄어든 자원만큼 체력을 회복한다.") 
		{ 

		}


	}
}
