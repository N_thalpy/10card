using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ManaDevourerField : FieldObject
	{

		public ManaDevourerField(): base(3, 1, CardTypes.monster, "ManaDevourer", "마나 아귀", 5, 8) 
		{
            TurnStart += (me) =>
			{
				if(me.CardType != CardTypes.mystery)
				{
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), me, me, "Vision") );
					if (me.Controller.Resource > 0)
					{
						me.Controller.Resource -= 1;
						GameAction.Heal(me, 1);
					}
					
					if (me.Opponent.Resource > 0)
					{
						me.Opponent.Resource -= 1;
						GameAction.Heal(me, 1);
					}
				}
			};
		}


	}
}
