using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ManaDrakeCard : Card
	{

        public ManaDrakeCard()
            : base(9, 0, "마나 비룡", CardTypes.monster, new ManaDrakeField(), "ManaDrake", "플레이어가 마법을 쓸 때마다 비용이 1씩 감소한다. 전투의 함성: 하수인 하나에게 소환 비용만큼의 피해를 입힌다.")
        {
            SpellTarget += (spell, me, target) =>
                {
                    me.CurrentCost -= 1;
					if(me.CurrentCost < 0)
						me.CurrentCost = 0;
                    return target;
                };
            Spell += (spell, me) =>
                {
                    me.CurrentCost -= 1;
					if(me.CurrentCost < 0)
						me.CurrentCost = 0;
                };
        }


	}
}
