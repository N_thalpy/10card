using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ManaDrakeField : FieldObject
	{
		
		public ManaDrakeField(): base(9, 0, CardTypes.monster, "ManaDrake", "마나 비룡", 7, 10) 
		{
			BattleCryTargetValidityCheck += (target, thisObject) =>
			{
				return VChecks.IsMonsterNotMyetery(target);
			};
			
			TargetBattleCry += (target, thisObject) =>
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), thisObject, thisObject, "Vision") );
				GameAction.DealDamage(thisObject, target, thisObject.LinkedCard.CurrentCost);
			};
		}
		
		
	}
}
