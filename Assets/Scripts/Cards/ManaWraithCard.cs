﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class ManaWraithCard : Card
    {
        public ManaWraithCard()
            : base(1, 1, "마나 망령", CardTypes.monster, new ManaWraithField(), "ManaWraith", "공격할 때마다 내 자원을 2 더한다. 영웅을 공격하는 경우 추가로 2 더한다.")
        {
        }
    }
}
