﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class ManaWraithField : FieldObject
    {
        public ManaWraithField()
            : base(1, 1, CardTypes.monster, "ManaWraith", "마나 망령", 3, 4)
        {
            Attack += (FieldObject Attacker, FieldObject Defender, FieldObject Me) =>
			{
				if (Attacker == Me)
				{
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Me, Me, "Vision") );
					if((Defender.CardType == CardTypes.monster) || (Defender.CardType == CardTypes.mystery))
						Me.Controller.Resource += 2;
					else if(Defender == Me.Opponent)
						Me.Controller.Resource += 4;
				}
			};
        }
    }
}
