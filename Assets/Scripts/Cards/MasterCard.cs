using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class MasterCard : Card
	{

        public MasterCard()
            : base(1, 1, "달인", CardTypes.monster, new MasterField(), "Master", "이 하수인이 자신보다 비용이 높은 대상에게 피해를 주는 경우, 피해량을 3 늘린다.", true, false) 
		{ 

		}


	}
}
