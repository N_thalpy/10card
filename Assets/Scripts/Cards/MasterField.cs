using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class MasterField : FieldObject
	{

		public MasterField(): base(1, 1, CardTypes.monster, "Master", "달인", 5, 7, false, "", true) 
		{
            Damaging += (giver, taker, amount, me) =>
			{
				if (giver == me && taker.CurrentCost > me.CurrentCost)
				{
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), taker, taker, "Explosion") );
					return 3 + amount;
				}
				else
					return amount;
			};
		}


	}
}
