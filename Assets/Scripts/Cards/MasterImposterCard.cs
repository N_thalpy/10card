﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class MasterImposterCard : Card
    {
        public MasterImposterCard()
            : base(6, 3, "달인 흉내꾼", CardTypes.monster, new MasterImposterField(), "MasterImposter", "전투의 함성: 앞면 또는 뒷면 카드 하나의 복제가 된다.")
        {
        }
    }
}
