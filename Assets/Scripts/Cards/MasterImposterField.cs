﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class MasterImposterField : FieldObject
	{
		public MasterImposterField()
			: base(6, 3, CardTypes.monster, "MasterImposter", "달인 흉내꾼", 2, 2)
		{
			TargetBattleCry += (target, thisObject) =>
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), thisObject, thisObject,  "Smoke") );
				thisObject.FieldObjectCopy(target);
			};
			
			BattleCryTargetValidityCheck += (target, thisObject) =>
			{
				if(!VChecks.isPlayer(target) && (target.CardType != CardTypes.empty))
					return true;
				else
					return false;
			};
			
		}
	}
}
