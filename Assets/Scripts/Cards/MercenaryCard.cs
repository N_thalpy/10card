using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class MercenaryCard : Card
	{

        public MercenaryCard()
            : base(1, 3, "용병", CardTypes.monster, new MercenaryField(), "Mercenary", "매 턴 시작마다 내 자원을 1 줄인다.") 
		{ 

		}


	}
}
