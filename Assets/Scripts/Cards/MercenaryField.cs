using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class MercenaryField : FieldObject
	{
		
		public MercenaryField(): base(1, 3, CardTypes.monster, "Mercenary", "용병", 5, 7, false) 
		{
			TurnStart += (me) =>
			{
				if (me.CardType != CardTypes.mystery && (me.Controller.Resource > 0))
				{
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), me, me,  "Vision") );
					me.Controller.Resource -= 1;
				}
			};
			
		}
		
		
	}
}
