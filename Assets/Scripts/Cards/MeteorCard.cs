﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class MeteorCard : Card
    {
        public MeteorCard()
            : base(6, 4, "운석", CardTypes.magic, new MagicBox(), "Meteor", "상대 전장에서 무작위 2칸을 고릅니다. 그 칸에 카드가 있다면 파괴한다.")
        {
            NonTargetEvent += (thisCard) =>
			{
				int rand1 = GameAction.RandomInt(0, 5);
				int rand2 = GameAction.RandomInt(0, 4);
				if (rand2 >= rand1) rand2++;

				float effectTime = Effect.EFC.EffectLastTime();
				if (thisCard.Opponent.Field[rand1].CardType != CardTypes.empty)
				{
					Effect.AddEffect( new Effect( 1.0f, effectTime, thisCard.Opponent.Field[rand1], thisCard.Opponent.Field[rand1],  "Explosion") );
					GameAction.DestroyFieldObject(thisCard.Opponent.Field[rand1]);
				}

				if (thisCard.Opponent.Field[rand2].CardType != CardTypes.empty)
				{
					Effect.AddEffect( new Effect( 1.0f, effectTime, thisCard.Opponent.Field[rand2], thisCard.Opponent.Field[rand2],  "Explosion") );
					GameAction.DestroyFieldObject(thisCard.Opponent.Field[rand2]);
				}
			};
        }
    }
}
