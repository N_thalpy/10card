using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class MirrorEffigyCard : Card
    {
        public MirrorEffigyCard()
            : base(1, 2, "거울 인형", CardTypes.magic, new MagicBox(), "MirrorEffigy", "*미구현* 잠복: 상대 하수인이 공격하면 이 카드가 대신 공격받는다. 이후 이 카드가 받은 피해만큼 그 하수인에게 피해를 준 뒤, 이 카드를 파괴한다.")
        {
            
        }
    }
}
