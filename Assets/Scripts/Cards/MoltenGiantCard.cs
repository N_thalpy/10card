﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class MoltenGiantCard : Card
    {
        public MoltenGiantCard()
            : base(20, 1, "용암거인", CardTypes.monster, new MoltenGiantField(), "MoltenGiant", "이 하수인이 손에 있을 때 내 영웅이 피해를 입으면 이 하수인의 비용이 그만큼 내려간다.")
        {
            Damaged += (FieldObject DamageTaker, FieldObject DamageGiver, int Damage, Card MeHand) =>
                {
                    if (DamageTaker == MeHand.Controller)
					{
						if(MeHand.CurrentCost - Damage >= 0)
						{
							MeHand.CurrentCost -= Damage;
							MeHand.LinkedFieldObject.CurrentCost -= Damage;
						}
					}
                    return Damage;
                };
        }
    }
}
