﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class MoltenGiantField : FieldObject
    {
        public MoltenGiantField()
            : base(20, 1, CardTypes.monster, "MoltenGiant", "용암거인", 16, 16)
        {
        }
    }
}
