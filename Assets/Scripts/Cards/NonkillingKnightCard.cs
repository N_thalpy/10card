using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class NonkillingKnightCard : Card
	{

        public NonkillingKnightCard()
            : base(3, 2, "불살의 기사", CardTypes.monster, new NonkillingKnightField(), "NonkillingKnight", "이 하수인이 입히는 피해는 체력을 최소 1까지만 낮춘다.") 
		{ 

		}


	}
}
