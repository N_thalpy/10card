using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class NonkillingKnightField : FieldObject
	{

		public NonkillingKnightField(): base(3, 2, CardTypes.monster, "NonkillingKnight", "불살의 기사", 7, 10, false) 
		{
            Damaging += (giver, taker, amount, me) =>
                {
                    if (me.CardType != CardTypes.mystery && giver == me && amount >= taker.CurrentHp)
                        return taker.CurrentHp - 1;
                    else return amount;
                };
		}


	}
}
