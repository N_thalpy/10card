using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class NotablepoliticianCard : Card
	{

        public NotablepoliticianCard()
            : base(4, 0, "저명한 정치인", CardTypes.monster, new NotablepoliticianField(), "Notablepolitician", "전투의 함성: 공격력 9 이상인 하수인 하나를 파괴한다. 이 하수인이 죽을 때 무작위 상대 하수인 하나를 파괴한다.", false, false) 
		{ 

		}


	}
}
