using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class NotablepoliticianField : FieldObject
	{

		public NotablepoliticianField(): base(4, 0, CardTypes.monster, "Notablepolitician", "저명한 정치인", 6, 8, false, "", false) 
		{ 
			TargetBattleCry += GivingYouBigGun;
			Death += DieTogether;
			BattleCryTargetValidityCheck += ThisValidityCheck;
		}

		public void GivingYouBigGun(FieldObject Target, FieldObject Me)
		{
			Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Target, Target,  "Explosion") );
			GameAction.DestroyFieldObject (Target);
		}

		public void DieTogether(FieldObject Unit, FieldObject Me)
		{
			if (Unit == Me) 
			{
				List<FieldObject> TempKillList = new List<FieldObject>();
				int RndNum = 0;

				for(int i = 0; i < Me.Opponent.Field.Count; i++)
				{
					if(Me.Opponent.Field[i].CardType != CardTypes.empty)
						TempKillList.Add(Me.Opponent.Field[i]);
				}

				if(TempKillList.Count > 0)
				{
					RndNum = GameAction.RandomInt(0,TempKillList.Count);
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), TempKillList[RndNum], TempKillList[RndNum], "Explosion") );
					GameAction.DestroyFieldObject(TempKillList[RndNum]);
				}
			}
		}

		public bool ThisValidityCheck(FieldObject Target, FieldObject Me)
		{
			if (Target.CardType == CardTypes.monster && Target.CurrentAp > 8)
				return true;
			else
				return false;
		}

	}
}
