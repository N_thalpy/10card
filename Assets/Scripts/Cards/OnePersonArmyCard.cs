using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class OnePersonArmyCard : Card
    {
        public OnePersonArmyCard()
            : base(5, 2, "일당백", CardTypes.magic, new MagicBox(), "OnePersonArmy", "행동 가능한 아군 하수인 하나를 정한다. 그것은 상대 전장의 각 하수인을 차례대로 공격하고 행동 불능이 된다.")
        {
            TargetValidityCheck += (target, me) =>
                {
                    return VChecks.IsMine(me, target) && VChecks.IsMonsterNotMyetery(target) && target.Activity;
                };
            TargetEvent += (target, me) =>
                {

                    for (int i = 0; i < 5; i++)
                    {
                        if (GameAction.IsFieldObjectOnField(target))
                            target.Activity = true;
                        else
                            return;
                        if (me.Opponent.Field[i].CardType == CardTypes.monster)
                            GameAction.Attack(target, me.Opponent.Field[i]);
                    }
                    if (GameAction.IsFieldObjectOnField(target))
                        target.Activity = false;
                };
        }

        
    }
}
