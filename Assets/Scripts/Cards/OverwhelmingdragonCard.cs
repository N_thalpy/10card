using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class OverwhelmingdragonCard : Card
	{

        public OverwhelmingdragonCard()
            : base(6, 0, "압도하는 용", CardTypes.monster, new OverwhelmingdragonField(), "Overwhelmingdragon", "변신하면서 상대의 모든 하수인의 공격력을 7만큼 감소시킨다. 매 턴 시작 시 공격력 5 이하의 상대 하수인을 행동 불능으로 만든다.", false, false) 
		{ 

		}


	}
}
