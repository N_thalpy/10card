using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class OverwhelmingdragonField : FieldObject
	{

		public OverwhelmingdragonField(): base(6, 0, CardTypes.monster, "Overwhelmingdragon", "압도하는 용", 9, 15, false, "", false) 
		{
			TurnStart += AttackSeal;
		}

		public void AttackSeal(FieldObject Me)
		{
			if (Me.CardType != CardTypes.mystery) 
			{
				for (int i = 0; i < Me.Opponent.Field.Count; i++) 
				{
					if(Me.Opponent.Field[i].CardType == CardTypes.monster)
					{
						if(Me.Opponent.Field[i].CurrentAp < 6)
						{
							Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Me.Opponent.Field[i], Me.Opponent.Field[i],  "Frost") );
							Me.Opponent.Field[i].Activity = false;
						}
					}
				}
			}
		}
	}
}
