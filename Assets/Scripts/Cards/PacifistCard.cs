﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class PacifistCard : Card
    {
        public PacifistCard()
            : base(7, 5, "평화주의자", CardTypes.monster, new PacifistField(), "Pacifist", "전투의 함성: 마주본 하수인의 공격력을 0으로 만든다.")
        {
        }
    }
}
