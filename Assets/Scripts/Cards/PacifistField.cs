﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class PacifistField : FieldObject
	{
		public PacifistField()
			: base(7, 5, CardTypes.monster, "Pacifist", "평화주의자", 0, 7)
		{
			NonTargetBattleCry += (FieldObject Me) =>
			{
				FieldObject facing = Me.Opponent.Field[Me.GetPosition()];
				if (GameAction.IsFieldObjectOnField(facing) && GameAction.IsFieldObjectMonster(facing))
				{
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), facing, facing, "Frost") );
					new StatBuff(facing, -facing.CurrentAp, 0, false).Initialize();
				}
			};
		}
	}
}
