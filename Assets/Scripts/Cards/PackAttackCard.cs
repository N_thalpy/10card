﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class PackAttackCard : Card
    {
        public PackAttackCard()
            : base(3, 2, "무리 공격", CardTypes.magic, new MagicBox(), "PackAttack", "적 하수인 하나를 목표로 정한다. 각 아군 하수인은 자신의 공격력만큼 그 하수인에게 피해를 준다.")
        {
			TargetValidityCheck += PackAttackTargetValidityCheck;
            TargetEvent += ( FieldObject Target, Card Me) =>
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Target, Target,  "Blood") );
				for (int i = 0; i < 5; i++)
				{
					if (Me.Controller.Field[i].CardType == CardTypes.monster)
					{
						GameAction.DealDamage(Me.Controller.Field[i], Target, Me.Controller.Field[i].CurrentAp);
					}
				}
			};
        }
        public bool PackAttackTargetValidityCheck(FieldObject Target, Card thisCard)
        {
            return (VChecks.IsMonsterNotMyetery(Target));
        }
    }
}
