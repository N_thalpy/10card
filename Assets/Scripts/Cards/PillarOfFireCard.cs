using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class PillarOfFireCard : Card
    {
        public PillarOfFireCard()
            : base(7, 2, "불기둥", CardTypes.magic, new MagicBox(), "PillarOfFire", "상대의 모든 하수인에게 피해를 10 준다.")
        {
            NonTargetEvent += DealMassDamage;
        }
        void DealMassDamage(Card thisCard)
        {
			float effectTime = Effect.EFC.EffectLastTime();
            for (int i = 0; i < 5; i++)
            {
				FieldObject target = thisCard.Opponent.Field[i];
                if (target.CardType == CardTypes.monster)
				{
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), target, target,  "Explosion") );
					GameAction.DealDamage(thisCard.Controller, target, 10);
        	    }
			}
        }
    }
}
