using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class PinkDolphinCard : Card
	{

        public PinkDolphinCard()
            : base(2, 1, "보랏빛 돌고래", CardTypes.monster, new PinkDolphinField(),"PinkDolphin", "이 하수인을 공격한 하수인은 공격력이 1이 된다.") 
		{ 

		}


	}
}
