using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class PinkDolphinField : FieldObject
	{

		public PinkDolphinField(): base(2, 1, CardTypes.monster, "PinkDolphin", "보랏빛 돌고래", 0, 7, false) 
		{
            Attack += (attacker, defender, me) =>
			{
				if (defender == me)
					AtkToOne(attacker);
			};
		}

        void AtkToOne(FieldObject attacker)
        {
            if (GameAction.IsFieldObjectOnField(attacker))
            {
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), attacker, attacker,  "Frost") );
                StatBuff atkDebuff = new StatBuff(attacker, 1 - attacker.CurrentAp, 0, false);
                atkDebuff.Initialize();
            }
        }
	}
}
