﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class PitLordCard : Card
    {
        public PitLordCard()
            : base(4, 2, "지옥의 군주", CardTypes.monster, new PitLordField(), "PitLord", "전투의 함성: 내 영웅에게 피해를 8 준다.")
        {
        }
    }
}
