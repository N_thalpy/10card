﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class PitLordField : FieldObject
	{
		public PitLordField()
			: base(4, 2, CardTypes.monster, "PitLord", "지옥의 군주", 7, 10)
		{
			NonTargetBattleCry += (FieldObject Me) =>
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Me.Controller, Me.Controller,  "Explosion") );
				GameAction.DealDamage(Me, Me.Controller, 8);
			};
		}
	}
}
