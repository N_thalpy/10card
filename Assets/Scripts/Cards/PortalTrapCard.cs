﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class PortalTrapCard : Card
    {
        public PortalTrapCard():
             base(5, 3, "차원문 함정", CardTypes.magic, new MagicBox(), "PortalTrap", "잠복: 이것 또는 내가 공격받으면 공격자 외 무작위 상대 하수인을 턴 끝까지 아군으로 만들고 공격자에게 공격받게 한다.",true,true)
        {
            //LinkedFieldObject.Reverse += PortalTrap;
            LinkedFieldObject.Attack += PortalTrap;
        }

        void PortalTrap(FieldObject attacker, FieldObject defender, FieldObject thisObject)
        {
            if (!GameAction.IsFieldObjectOnField(attacker) || thisObject.CardType != CardTypes.mystery) return;
            if (defender == thisObject.Controller || defender == thisObject)
            {
                

                List<FieldObject> targetCandidates = new List<FieldObject>();
                FieldObject fo;

                for (int i = 0; i < 5; i++)
                {
                    fo = thisObject.Opponent.Field[i];
                    if (fo.CardType == CardTypes.monster && fo != attacker)
                    {
                        targetCandidates.Add(fo);
                    }
                }
                if (targetCandidates.Count == 0) return;
                else
                {
                    if (defender != thisObject)
                        GameAction.Reverse(thisObject);

                    fo = targetCandidates[GameAction.RandomInt(0, targetCandidates.Count)];
                    ChangeMind(fo, thisObject);
                    GameAction.Attack(attacker, fo);
                }
            }
        }

        Player myController = null;
        Player origController = null;
        void ChangeMind(FieldObject Target, FieldObject thisObject)
        {
            myController = thisObject.Controller;
            origController = Target.Controller;
            Buff ChangeMindBuff = new Buff(Target, ChangeMindinit, ChangeMindEnd, true);
            ChangeMindBuff.Initialize();
        }

        void ChangeMindinit(Buff thisBuff)
        {
			Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), thisBuff.BuffTarget, thisBuff.BuffTarget,  "Vision") );
            GameAction.GiveControl(thisBuff.BuffTarget, myController);
            thisBuff.BuffTarget.Activity = true;
        }

        void ChangeMindEnd(Buff thisBuff)
        {
            if (!GameAction.IsFieldObjectOnField(thisBuff.BuffTarget)) return;
            GameAction.GiveControl(thisBuff.BuffTarget, origController);
            thisBuff.BuffTarget.Activity = true;
        }
    }
}
