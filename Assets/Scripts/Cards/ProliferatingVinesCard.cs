﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class ProliferatingVinesCard : Card
    {
        public ProliferatingVinesCard()
            : base(2, 3, "증식하는 넝쿨", CardTypes.monster, new ProliferatingVinesField(), "ProliferatingVines", "매 턴 시작마다 자신의 오른쪽 칸이 비었으면 1/1 넝쿨 하나를 소환한다.")
        {
        }
    }
}
