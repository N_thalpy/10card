﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class ProliferatingVinesField : FieldObject
    {
        public ProliferatingVinesField()
            : base(2, 3, CardTypes.monster, "ProliferatingVines", "증식하는 넝쿨", 3, 6)
        {
            TurnStart += (FieldObject Me) =>
			{
				if (Me.GetPosition() + 1 < 5 && Controller.Field[Me.GetPosition() + 1].CardType == CardTypes.empty)
				{
					ProliferatingVines_VineField vine = new ProliferatingVines_VineField();
					vine.Controller = Me.Controller;
					vine.Opponent = Me.Opponent;

					GameAction.SummonAtPosition(Me.Controller.Field[Me.GetPosition() + 1], vine);
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), vine, vine,  "Wind") );
					//Trigger.SummonTrigger(vine);
				}
			};
        }
    }
}
