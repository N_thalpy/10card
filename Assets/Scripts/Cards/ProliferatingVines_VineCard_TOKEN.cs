﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class ProliferatingVines_VineCard : Card 
    {
        public ProliferatingVines_VineCard()
            : base(0, 0, "넝쿨", CardTypes.monster, new ProliferatingVines_VineField(), "ProliferatingVines_Vine")
        {

        }
    }
}
