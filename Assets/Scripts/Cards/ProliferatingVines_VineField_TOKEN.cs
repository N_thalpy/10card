﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class ProliferatingVines_VineField : FieldObject
    {
        public ProliferatingVines_VineField()
            : base(0, 0, CardTypes.monster, "ProliferatingVines_Vine", "넝쿨", 1, 1, true)
        {

        }
    }
}
