﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class RegeneratingVineWallCard : Card
	{

        public RegeneratingVineWallCard()
            : base(2, 3, "재생되는 덩굴벽", CardTypes.monster, new RegeneratingVineWallField(), "RegeneratingVineWall", "매 턴 시작마다 체력을 최대치로 회복한다.") 
		{ 

		}


	}
}
