﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class RegeneratingVineWallField : FieldObject
	{
        public RegeneratingVineWallField() : base(2, 3, CardTypes.monster, "RegeneratingVineWall", "재생되는 덩굴벽", 0, 11) 
        {
            TurnStart += (me) =>
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), me, me,  "Shield") );
				GameAction.Heal(me, me.CurrentMaxHp);
			};
        }
	}
}
