using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class ResonanceCard : Card
    {
        public ResonanceCard()
            : base(3, 1, "공명", CardTypes.magic, new MagicBox(), "Resonance", "하수인 하나를 목표로 정한다. 그 하수인 자체를 포함해 그것와 공격력 또는 체력이 같은 모든 하수인을 파괴한다.")
        {
            TargetEvent += (target, thisCard) =>
                {
					if ( target.CardType != CardTypes.empty )
					{
	                    int resAtk = target.CurrentAp;
                    	int resHp = target.CurrentHp;
                    	FieldObject fo;
                    	for (int i = 0; i < 5; i++)
                    	{
	                        fo = thisCard.Controller.Field[i];
                        	if (fo.CurrentHp == resHp || fo.CurrentAp == resAtk)
	                            GameAction.DestroyFieldObject(fo);
                    	}
                    	for (int i = 0; i < 5; i++)
                    	{
	                        fo = thisCard.Opponent.Field[i];
                        	if (fo.CurrentHp == resHp || fo.CurrentAp == resAtk)
	                            GameAction.DestroyFieldObject(fo);
                    	}
					}
                };
            TargetValidityCheck = (target, thisCard) =>
                {
                    return VChecks.IsMonsterNotMyetery(target);
                };
        }
    }
}
