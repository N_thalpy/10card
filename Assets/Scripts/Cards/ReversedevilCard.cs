using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ReversedevilCard : Card
	{

        public ReversedevilCard()
            : base(10, 0, "반전의 악마", CardTypes.monster, new ReversedevilField(), "Reversedevil", "전투의 함성: 양 플레이어 모두 손과 무덤을 바꾼다.", false, false) 
		{ 

		}


	}
}
