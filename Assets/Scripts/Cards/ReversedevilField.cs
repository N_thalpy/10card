using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ReversedevilField : FieldObject
	{
		
		public ReversedevilField(): base(10, 0, CardTypes.monster, "Reversedevil", "반전의 악마", 12, 17, false, "", false) 
		{ 
			Summon += AllDeactive;
		}
		
		public void AllDeactive(FieldObject Unit, FieldObject Me)
		{
			if (Unit == Me) 
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Me, Me,  "Vision") );
				
				List<Card> TempMeHandList = new List<Card> ();
				List<Card> TempMeGraveList = new List<Card> ();
				List<Card> TempOpHandList = new List<Card> ();
				List<Card> TempOpGraveList = new List<Card> ();
				
				
				for(int i = 0; i < Me.Controller.Hand.Count; i++)
				{
					if(Me.Controller.Hand[i].Recyclable)
						TempMeHandList.Add(Me.Controller.Hand[i]);
				}
				
				
				for(int i = 0; i < Me.Controller.Grave.Count; i++)
					TempMeGraveList.Add (Me.Controller.Grave[i]);
				
				while(Me.Controller.Hand.Count > 0)
					Me.Controller.Hand.Remove(Me.Controller.Hand[0]);
				
				while(Me.Controller.Grave.Count > 0)
					Me.Controller.Grave.Remove(Me.Controller.Grave[0]);
				
				for(int i = 0; i < TempMeHandList.Count; i++)
					Me.Controller.Grave.Add (TempMeHandList[i]);
				
				for(int i = 0; i < TempMeGraveList.Count; i++)
					Me.Controller.Hand.Add (TempMeGraveList[i]);
				
				
				
				for(int i = 0; i < Me.Opponent.Hand.Count; i++)
				{
					if(Me.Opponent.Hand[i].Recyclable)
						TempOpHandList.Add(Me.Opponent.Hand[i]);
				}
				
				
				for(int i = 0; i < Me.Opponent.Grave.Count; i++)
					TempOpGraveList.Add (Me.Opponent.Grave[i]);
				
				while(Me.Opponent.Hand.Count > 0)
					Me.Opponent.Hand.Remove(Me.Opponent.Hand[0]);
				
				while(Me.Opponent.Grave.Count > 0)
					Me.Opponent.Grave.Remove(Me.Opponent.Grave[0]);
				
				for(int i = 0; i < TempOpHandList.Count; i++)
					Me.Opponent.Grave.Add (TempOpHandList[i]);
				
				for(int i = 0; i < TempOpGraveList.Count; i++)
					Me.Opponent.Hand.Add (TempOpGraveList[i]);
				
				GameAction.MainSystem.CardSort();
			}
		}
		
		
	}
}
