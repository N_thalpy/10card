using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class RitualDaggerCard : Card
    {
        public RitualDaggerCard()
            : base(3, 10, "의식용 단검", CardTypes.magic, new MagicBox(), "RitualDagger", "하수인 하나에게 +3/+0을 부여한다. 그 하수인이 죽을 때 이 카드와 그 하수인을 회수한다.")
        {
            TargetEvent += (target, thisCard) =>
			{
				if ( target.CardType != CardTypes.empty ) return;
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), target, target,  "Shield") );
				new StatBuff(target, 3, 0).Initialize();

				target.Death += (unit, me) =>
				{
					if (unit == me)
					{
						for (int i = 0; i < unit.LinkedCard.Controller.Grave.Count; i++)
						{
							if (unit.LinkedCard.Controller.Grave[i].Name == this.Name)
							{
								unit.LinkedCard.Controller.Hand.Add(unit.LinkedCard.Controller.Grave[i]);
								unit.LinkedCard.Controller.Grave.Remove(unit.LinkedCard.Controller.Grave[i]);
								break;
							}
						}
						for (int i = 0; i < unit.LinkedCard.Controller.Grave.Count; i++)
						{
							if (unit.LinkedCard.Controller.Grave[i].Name == unit.LinkedCard.Name)
							{
								unit.LinkedCard.Controller.Hand.Add(unit.LinkedCard.Controller.Grave[i]);
								unit.LinkedCard.Controller.Grave.Remove(unit.LinkedCard.Controller.Grave[i]);
								break;
							}
						}
					}
				};
			};

            TargetValidityCheck = (target, thisCard) =>
            {
                return VChecks.IsMonsterNotMyetery(target);
            };
        }
    }
}
