using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class SilverknightCard : Card
	{

        public SilverknightCard()
            : base(5, 0, "은의 기사", CardTypes.monster, new SilverknightField(), "Silverknight", "*미구현*자신보다 비용이 낮은 마법 및 몬스터에게 받는 피해가 0이 됩니다", false, false) 
		{ 

		}


	}
}
