using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class SilverknightField : FieldObject
	{

		public SilverknightField(): base(5, 0, CardTypes.monster, "Silverknight", "은의 기사", 9, 13, false, "", false) 
		{ 
			Damaged += DamageModifybyCost;
		}

		public int DamageModifybyCost(FieldObject DamageTaker, FieldObject DamageGiver, int DamageAmount, FieldObject Me)
		{
			//구조상 마법카드의 코스트 참조 불가로 보류

			return DamageAmount;
		}
	}
}
