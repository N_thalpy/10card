using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class SirensSongCard : Card
    {
        public SirensSongCard()
            : base(3, 3, "세이렌의 노래", CardTypes.magic, new MagicBox(), "SirensSong", "상대 손의 최고 비용 하수인을 공격력 1로 상대에게 소환한다. 그 하수인 비용의 절반(버림)만큼의 자원을 얻는다.")
        {
            NonTargetEvent += (thisCard) =>
			{
				FieldObject empty;
				if (GameAction.EmptyFieldLocation(thisCard.Opponent) == -1) return;
				else empty = thisCard.Opponent.Field[GameAction.EmptyFieldLocation(thisCard.Opponent)];
				
				Card memon = null;
				Card hc = null;
				for (int i = 0; i < thisCard.Opponent.Hand.Count; i++)
				{
					hc = thisCard.Opponent.Hand[i];
					if (hc.CardType == CardTypes.monster)
					{
						if (memon == null || memon.CurrentCost < hc.CurrentCost)
							memon = hc;
					}
				}
				if (memon == null) 
					return;
				GameAction.MainSystem.ReplaceCardInHandWithEmpty(memon);
				
				GameAction.SummonAtPosition(empty, memon.LinkedFieldObject);
				
				new StatBuff(memon.LinkedFieldObject, 1 - memon.LinkedFieldObject.CurrentAp, 0).Initialize();
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), memon.LinkedFieldObject, memon.LinkedFieldObject, "Vision") );
				thisCard.Controller.Resource += memon.CurrentCost / 2;
			};
        }
    }
}
