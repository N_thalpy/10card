﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class SmokecasterCard : Card
    {
        public SmokecasterCard()
            : base(3, 6, "연막술사", CardTypes.monster, new SmokecasterField(), "SmokeCaster", "목표를 정하는 주문은 가능한 전체 목표 중 무작위를 정하게 된다.")
        {
        }
    }
}
