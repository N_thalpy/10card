﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class SmokecasterField : FieldObject
    {
        public SmokecasterField()
            : base(3, 6, CardTypes.monster, "SmokeCaster", "연막술사", 5, 7)
        {
            SpellTarget += (spell, me, target) =>
			{
				List<FieldObject> targetCandidates = new List<FieldObject>();
				FieldObject fo;
				for (int i = 0; i < 5; i++)
				{
					fo = me.Controller.Field[i];
					if (fo.CardType != CardTypes.empty && spell.TargetValidityCheck(fo, spell))
						targetCandidates.Add(fo);
				}
				if (spell.TargetValidityCheck(me.Controller,spell))
					targetCandidates.Add(me.Controller);
				
				for (int i = 0; i < 5; i++)
				{
					fo = me.Opponent.Field[i];
					if (fo.CardType != CardTypes.empty && spell.TargetValidityCheck(fo, spell))
						targetCandidates.Add(fo);
				}
				
				if (spell.TargetValidityCheck(me.Opponent, spell))
					targetCandidates.Add(me.Opponent);

				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), me, me, "Vision") );
				if (targetCandidates.Count == 0) 
					return target;
				else 
					return targetCandidates[GameAction.RandomInt(0, targetCandidates.Count)];
			};
        }
    }
}
