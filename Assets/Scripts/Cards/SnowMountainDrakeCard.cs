using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class SnowMountainDrakeCard : Card
	{

        public SnowMountainDrakeCard()
            : base(4, 2, "설산 비룡", CardTypes.monster, new SnowMountainDrakeField(),"SnowMountainDrake", "전투의 함성: 하수인 하나를 행동 불가로 만든다.") 
		{ 

		}


	}
}
