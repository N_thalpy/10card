using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class SnowMountainDrakeField : FieldObject
	{

		public SnowMountainDrakeField(): base(4, 2, CardTypes.monster, "SnowMountainDrake", "설산 비룡", 6, 8, false) 
		{
            TargetBattleCry += (target, thisObj) =>
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), target, target,  "Frost") );
				target.Activity = false;
			};
            BattleCryTargetValidityCheck += (target, thisObj) =>
			{
				return VChecks.IsMonsterNotMyetery(target);
			};
		}


	}
}
