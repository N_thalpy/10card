﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class SoulfireCard : Card
    {
        public SoulfireCard()
            : base(4, 0, "영혼의 불길", CardTypes.magic, new MagicBox(), "Soulfire", "아군 하수인 모두를 파괴한다. 하수인 또는 플레이어 하나에게 그 공격력의 총합만큼의 피해를 입힌다.",false)
        {
			TargetValidityCheck += SoulfireTargetValidityCheck;
            TargetEvent += (FieldObject Target, Card thisCard) =>
			{
				int sum = 0;
				float effectTime = Effect.EFC.EffectLastTime();
				for (int i = 0; i < 5; i++)
				{
					FieldObject fo = thisCard.Controller.Field[i];
					Effect.AddEffect( new Effect( 1.0f, effectTime , fo, fo, "Smoke") );
					if (fo.CardType == CardTypes.mystery || fo.CardType == CardTypes.monster)
					{
						sum += thisCard.Controller.Field[i].CurrentAp;
						GameAction.DestroyFieldObject(fo);
					}
				}

				Effect.AddEffect(new Effect(1.0f, effectTime, Target, Target, "Smoke"));
				GameAction.DealDamage(thisCard.Controller, Target, sum);
			};
       
        }
        public bool SoulfireTargetValidityCheck(FieldObject Target, Card thisCard)
        {
            return Target.CardType != CardTypes.empty;
        }
    }
}
