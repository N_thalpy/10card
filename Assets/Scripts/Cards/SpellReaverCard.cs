using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class SpellReaverCard : Card
	{

        public SpellReaverCard()
            : base(4, 2, "주문 강탈자", CardTypes.monster, new SpellReaverField(),"SpellReaver", "전투의 함성: 상대 전장의 무작위 토큰 하나를 우리 편으로 만든다.") 
		{ 

		}


	}
}
