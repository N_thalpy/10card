using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class SpellReaverField : FieldObject
	{

		public SpellReaverField(): base(4, 2, CardTypes.monster, "SpellReaver", "주문 강탈자", 6, 8, false) 
		{
            NonTargetBattleCry += (me) =>
			{
				List<FieldObject> targetCandidates = new List<FieldObject>();
				FieldObject fo;
				for (int i = 0; i < 5; i++)
				{
					fo = me.Opponent.Field[i];
					if (fo.IsToken && fo.CardType == CardTypes.monster)
						targetCandidates.Add(fo);
				}
				if (targetCandidates.Count == 0) 
					return;
				else 
					GameAction.GiveControl(targetCandidates[GameAction.RandomInt(0, targetCandidates.Count)], me.Controller);
			};
		}


	}
}
