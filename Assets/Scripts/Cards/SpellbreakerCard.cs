using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class SpellbreakerCard : Card
	{

        public SpellbreakerCard()
            : base(2, 1, "주문 파괴자", CardTypes.monster, new SpellbreakerField(),"Spellbreaker", "잠복: 상대가 마법을 사용하면, 상대는 그 두 배의 비용을 지불해야 한다. 그럴 수 없으면 그것을 무효화한다.") 
		{ 

		}


	}
}
