using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class SpellbreakerField : FieldObject
	{

		public SpellbreakerField(): base(2, 1, CardTypes.monster, "Spellbreaker", "주문 파괴자", 4, 6) 
		{
            Spell += BreakSpell;
            SpellTarget += (spell, me, target) => 
            {
                BreakSpell(spell, me); 
                return target; 
            };
        }

        void BreakSpell(Card spell, FieldObject thisObject)
        {
            if (spell.Controller != thisObject.Controller && thisObject.CardType == CardTypes.mystery)
            {
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), thisObject, thisObject,  "Vision") );
                GameAction.Reverse(thisObject);
                if (spell.Controller.Resource > spell.CurrentCost)
                    spell.Controller.Resource -= spell.CurrentCost;
                else
				{
                    if (spell.TargetEvent != null)
                        spell.TargetEvent = delegate { };
                    else
                        spell.NonTargetEvent = delegate { };
            	}
			}
        }
	}
}
