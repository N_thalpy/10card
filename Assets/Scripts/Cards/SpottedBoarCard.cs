using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class SpottedBoarCard : Card
	{

        public SpottedBoarCard()
            : base(4, 1, "점박이 멧돼지", CardTypes.monster, new SpottedBoarField(),"SpottedBoar", "이 하수인이 파괴될 때, 양 플레이어의 체력을 8 회복시킨다.") 
		{ 

		}


	}
}
