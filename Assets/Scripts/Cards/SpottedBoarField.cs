using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class SpottedBoarField : FieldObject
	{
		
		public SpottedBoarField(): base(4, 1, CardTypes.monster, "SpottedBoar", "점박이 멧돼지", 6, 8, false) 
		{
			Death += HealPlayers;
		}
		
		void HealPlayers(FieldObject Unit, FieldObject thisObject)
		{
			if (Unit == thisObject)
			{
				float effectTime = Effect.EFC.EffectLastTime();
				Effect.AddEffect( new Effect( 1.0f, effectTime , thisObject.Controller, thisObject.Controller,  "Heal") );
				Effect.AddEffect( new Effect( 1.0f, effectTime , thisObject.Opponent, thisObject.Opponent,  "Heal") );
				
				GameAction.Heal(thisObject.Controller, 8);
				GameAction.Heal(thisObject.Opponent, 8);
			}
		}
	}
}
