using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class SpyCard : Card
    {
        public SpyCard()
            : base(1, 2, "밀정", CardTypes.magic, new MagicBox(), "Spy", "*미구현*상대의 리버스 효과는 발동하지 않습니다.")
        {
            
        }
    }
}
