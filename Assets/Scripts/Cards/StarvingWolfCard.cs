using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class StarvingWolfCard : Card
	{

        public StarvingWolfCard()
            : base(4, 1, "굶주린 늑대", CardTypes.monster, new StarvingWolfField(),"StarvingWolf", "전장의 하수인이 피해를 입을 때마다, 이 하수인에게 +1/+0을 부여한다.") 
		{ 

		}


	}
}
