using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class StarvingWolfField : FieldObject
	{
		
		public StarvingWolfField(): base(4, 1, CardTypes.monster, "StarvingWolf", "굶주린 늑대", 3, 8, false) 
		{
			Damaged += Rage;
		}
		
		int Rage(FieldObject DamageTaker, FieldObject DamageGiver, int ActualAmount, FieldObject thisObject)
		{
			if(DamageTaker.CardType != CardTypes.player)
			{
				if(GameAction.IsFieldObjectMonster(thisObject))
				{
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), thisObject, thisObject,  "Holy") );
					new StatBuff(thisObject, 1, 0).Initialize();
				}

			}
			
			return ActualAmount;
		}
	}
}
