using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class SufferingPeopleCard : Card
	{

        public SufferingPeopleCard()
            : base(0, 1, "피폐한 백성", CardTypes.monster, new SufferingPeopleField(), "SufferingPeople", "내 턴 시작마다 자신에게 피해를 2 준다.") 
		{ 

		}


	}
}
