using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class SufferingPeopleField : FieldObject
	{
		
		public SufferingPeopleField(): base(0, 1, CardTypes.monster, "SufferingPeople", "피폐한 백성", 5, 3, false) 
		{
			TurnStart += Suffer;
		}
		
		void Suffer(FieldObject thisObject)
		{
			if (thisObject.Controller.Turn)
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), thisObject, thisObject,  "Frost") );
				GameAction.DealDamage(thisObject, thisObject, 2);
			}
		}
	}
}
