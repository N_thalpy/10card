using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class SummonbreakerCard : Card
	{

        public SummonbreakerCard()
            : base(4, 0, "소환파괴자", CardTypes.monster, new SummonbreakerField(), "Summonbreaker", "전투의 함성: 모든 토큰 하수인을 파괴한다. 파괴한 토큰 하나당 +3/+3을 얻는다.", false, false) 
		{ 

		}
	}
}
