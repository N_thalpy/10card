using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class SummonbreakerField : FieldObject
	{

		public SummonbreakerField(): base(4, 0, CardTypes.monster, "Summonbreaker", "소환파괴자", 6, 8, false, "", false) 
		{ 
			Summon += KillAllToken;
		}

		public void KillAllToken(FieldObject Unit, FieldObject Me)
		{
			if (Unit == Me) 
			{
				float effectTime = Effect.EFC.EffectLastTime();
				for(int i = 0; i < Me.Controller.Field.Count; i++)
				{
					FieldObject target = Me.Controller.Field[i];
					if((target.CardType != CardTypes.empty) && (target.IsToken))
					{
						Effect.AddEffect( new Effect( 1.0f, effectTime , target, target,  "Explosion") );
						GameAction.DestroyFieldObject(target);
						StatBuff AtHpBuff = new StatBuff(Me, 3, 3);
						AtHpBuff.Initialize();
					}
				}
				for(int i = 0; i < Me.Opponent.Field.Count; i++)
				{
					FieldObject target = Me.Opponent.Field[i];
					if((target.CardType != CardTypes.empty) && (target.IsToken))
					{
						Effect.AddEffect( new Effect( 1.0f, effectTime , target, target,  "Explosion") );
						GameAction.DestroyFieldObject(target);
						StatBuff AtHpBuff = new StatBuff(Me, 3, 3);
						AtHpBuff.Initialize();
					}
				}

				Effect.AddEffect( new Effect( 1.0f, effectTime , Me, Me,  "Holy") );
			}
		}
	}
}
