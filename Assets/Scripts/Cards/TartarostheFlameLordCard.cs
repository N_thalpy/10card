using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class TartarostheFlameLordCard : Card
	{

        public TartarostheFlameLordCard()
            : base(9, 4, "염제", CardTypes.monster, new TartarostheFlameLordField(),"TartarostheFlameLord", "전투의 함성: 전장의 카드 하나를 파괴한다.", false) 
		{ 

		}


	}
}
