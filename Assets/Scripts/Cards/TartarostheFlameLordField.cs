using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class TartarostheFlameLordField : FieldObject
	{

		public TartarostheFlameLordField(): base(9, 4, CardTypes.monster, "TartarostheFlameLord", "염제", 12, 16, false, "", false) 
		{
            TargetBattleCry += DestroyTarget;
		}

        void DestroyTarget(FieldObject target, FieldObject thisObject)
        {
			Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), target, target,  "Explosion") );
            GameAction.DestroyFieldObject(target);
        }

        bool TartarosTargetValidityCheck(FieldObject target, FieldObject thisObject)
        {
            return !VChecks.isPlayer(target);
        }
	}
}
