using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class TearingScarsCard : Card
    {
        public TearingScarsCard()
            : base(2, 2, "상처 찢기", CardTypes.magic, new MagicBox(), "TearingScars", "모든 하수인이 입은 피해만큼 피해를 받는다.")
        {
            NonTargetEvent += TearScar;
        }

        void TearScar(Card thisCard)
		{
			float effectTime = Effect.EFC.EffectLastTime();
            for (int i = 0; i < 5; i++)
            {
				int dealing = thisCard.Controller.Field[i].CurrentMaxHp - thisCard.Controller.Field[i].CurrentHp;
				Effect.AddEffect( new Effect( 1.0f, effectTime , thisCard.Opponent.Field[i], thisCard.Opponent.Field[i],  "Explosion") );
				GameAction.DealDamage(thisCard.Controller, thisCard.Controller.Field[i], dealing);
            }
            for (int i = 0; i < 5; i++)
            {
				int dealing = thisCard.Opponent.Field[i].CurrentMaxHp - thisCard.Opponent.Field[i].CurrentHp;
				Effect.AddEffect( new Effect( 1.0f, effectTime , thisCard.Opponent.Field[i], thisCard.Opponent.Field[i],  "Explosion") );
				GameAction.DealDamage(thisCard.Controller, thisCard.Opponent.Field[i], dealing);
            }
        }
    }
}
