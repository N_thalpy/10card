﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class TerrorintheDarkCard : Card
    {
        public TerrorintheDarkCard()
            : base(4, 3, "어둠 속의 공포", CardTypes.monster, new TerrorintheDarkField(), "TerrorintheDark", "상대 턴 시작마다 행동 불가 상태가 된다.\n앞면이 될 때 행동 불가 상태가 풀린다." )
        {
        }
    }
}
