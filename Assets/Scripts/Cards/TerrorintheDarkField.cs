﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class TerrorintheDarkField : FieldObject
	{
        public TerrorintheDarkField()
            : base(4, 3, CardTypes.monster, "TerrorintheDark", "어둠 속의 공포", 13, 13)
        {
            TurnStart += (FieldObject Me) =>
			{
				if (Me.Opponent.Turn)
					Me.Activity = false;
			};
			Reverse += (FieldObject Unit, FieldObject Me, FieldObject Attacker) =>
			{
				if (Unit == Me)
					Me.Activity = true;
			};
        }
	}
}
