using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class TheBlackPlagueCard : Card
    {
        public TheBlackPlagueCard()
            : base(2, 1, "흑사병", CardTypes.magic, new MagicBox(), "TheBlackPlague", "*수정중*하수인 하나에게 \"매 턴 시작마다 자신에게 피해를 5 입힌다. 이 하수인이 죽을 때 이 효과를 전장의 다른 무작위 하수인에게 부여한다.\"를 부여한다.")
        {
            TargetValidityCheck += TBPCValidityCheck;
            TargetEvent += GivePlagueBuff;
        }

        bool TBPCValidityCheck(FieldObject target, Card thisCard)
        {
            return VChecks.IsMonsterNotMyetery(target);
        }

        void GivePlagueBuff(FieldObject Target, Card thisCard)
        {
            new Buff(Target, PlagueInit, PlagueTerminate, false).Initialize();
        }

        void PlagueInit(Buff thisBuff)
        {
            thisBuff.BuffTarget.TurnStart += PlagueDamage;
            thisBuff.BuffTarget.Death += SpreadPlague;
        }

        void PlagueTerminate(Buff thisBuff)
        {
            thisBuff.BuffTarget.TurnStart -= PlagueDamage;
            thisBuff.BuffTarget.Death -= SpreadPlague;
        }

        void PlagueDamage(FieldObject thisObject)
        {
            GameAction.DealDamage(thisObject, thisObject, 5);
        }

        void SpreadPlague(FieldObject unit, FieldObject thisObject)
        {
            if (unit == thisObject)
            {
                List<FieldObject> TargetCandidates = new List<FieldObject>();

                for (int i = 0; i < 5; i++)
                {
                    if (thisObject.Controller.Field[i].CardType == CardTypes.monster)
                        TargetCandidates.Add(thisObject.Controller.Field[i]);
                }

                for (int i = 0; i < 5; i++)
                {
                    if (thisObject.Opponent.Field[i].CardType == CardTypes.monster)
                        TargetCandidates.Add(thisObject.Opponent.Field[i]);
                }

                if (TargetCandidates.Count == 0)
                    return;
                else
                    GivePlagueBuff(TargetCandidates[GameAction.RandomInt(0, TargetCandidates.Count)], null);
            }
        }
    }
}
