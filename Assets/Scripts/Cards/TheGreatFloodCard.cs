using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class TheGreatFloodCard : Card
    {
        public TheGreatFloodCard()
            : base(8, 2, "대범람", CardTypes.magic, new MagicBox(), "TheGreatFlood", "각 플레이어는 자신의 전장에 있는 모든 카드를 손으로 돌리고, 그 비용 총합만큼의 자원을 얻는다.")
        {
            NonTargetEvent += ReturnAndGet;
        }

        void ReturnAndGet(Card thisCard)
        {
            FieldObject fo;
            int costsum = 0;
			float effectTime = Effect.EFC.EffectLastTime();

            for (int i = 0; i < 5; i++)
            {
                fo = thisCard.Controller.Field[i];
                if (fo.CardType != CardTypes.empty)
                {
					Effect.AddEffect( new Effect( 1.0f, effectTime , fo, fo, "Water") );
					GameAction.ReturnToHand(fo);
                    costsum += fo.CurrentCost;
                }
            }
            thisCard.Controller.Resource += costsum;
            costsum = 0;

            for (int i = 0; i < 5; i++)
            {
                fo = thisCard.Opponent.Field[i];
                if (fo.CardType != CardTypes.empty)
				{
					Effect.AddEffect( new Effect( 1.0f, effectTime , fo, fo, "Water") );
                    GameAction.ReturnToHand(fo);
                    costsum += fo.CurrentCost;
                }
            }
            thisCard.Opponent.Resource += costsum;
        }
    }
}
