using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class ThreateningCard : Card
    {
        public ThreateningCard()
            : base(4, 2, "협박", CardTypes.magic, new MagicBox(), "Threatening", "잠복: 상대가 하수인 카드를 낼 때 상대는 두 배의 자원을 써야 한다. 불가능한 경우 그 하수인은 손으로 돌아간다.")
        {
            LinkedFieldObject.Summon += Threaten;
        }

        void Threaten(FieldObject unit, FieldObject thisObject)
        {
            if (unit.CardType == CardTypes.monster && unit.Controller != thisObject.Controller)
            {
                GameAction.Reverse(thisObject);
                if (unit.Controller.Resource > unit.CurrentCost)
                    unit.Controller.Resource -= unit.CurrentCost;
                else
                    GameAction.ReturnToHand(unit);
            }
        }
    }
}
