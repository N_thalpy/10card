using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ThrowTwoHandedWeaponCard : Card
	{

        public ThrowTwoHandedWeaponCard()
            : base(5, 2, "양손 무기 투척", CardTypes.magic, new MagicBox(), "ThrowTwoHandedWeapon", "아군 하수인 하나가 상대의 최고 체력 하수인과 최저 체력 하수인에게 자신의 공격력만큼의 피해를 입힌다.") 
		{
            TargetEvent = Throw;
            TargetValidityCheck += TTHWValidityCheck;
		}

        void Throw(FieldObject target, Card thisCard)
        {
            if (!GameAction.IsFieldObjectOnField(target)) return;
            FieldObject greatestHPMon = null;
            FieldObject leastHPMon = null;
            FieldObject fo;
            for (int i = 0; i < 5; i++)
            {
                fo = thisCard.Opponent.Field[i];
                if (fo.CardType == CardTypes.monster)
                {
                    if (greatestHPMon == null || greatestHPMon.CurrentHp < fo.CurrentHp)
                        greatestHPMon = fo;
                    if (leastHPMon == null || leastHPMon.CurrentHp > fo.CurrentHp)
                        leastHPMon = fo;
                }

            }

			float effectTime = Effect.EFC.EffectLastTime();;
            if (greatestHPMon != null)
            {
				Effect.AddEffect( new Effect( 1.0f, effectTime , greatestHPMon, greatestHPMon,  "Explosion") );
				Effect.AddEffect( new Effect( 1.0f, effectTime , leastHPMon, leastHPMon,  "Explosion") );

                GameAction.DealDamage(target, greatestHPMon, target.CurrentAp);
                GameAction.DealDamage(target, leastHPMon, target.CurrentAp);
            }
        }

        bool TTHWValidityCheck(FieldObject Target, Card thisCard)
        {
            return (VChecks.IsMine(thisCard, Target) && VChecks.IsMonsterNotMyetery(Target));
        }
	}
}
