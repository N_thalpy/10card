﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class TreasuryCard : Card
    {
        public TreasuryCard()
            : base(2, 2, "제물", CardTypes.magic, new MagicBox(), "Treasury", "아군 하수인 하나를 파괴하고 그 비용만큼의 자원을 얻는다.")
        {
			TargetValidityCheck += TreasuryTargetValidityCheck;
            TargetEvent = new TargetingEvent((FieldObject Target, Card Me) =>
			{
				if ( Target.CardType == CardTypes.empty ) return;
				if (!GameAction.IsFieldObjectOnField(Target))return;
				
				int resc = Target.CurrentCost;
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Target, Target,  "Blood") );
				GameAction.DestroyFieldObject(Target);
				Me.Controller.Resource += resc;
			});
        }
        public bool TreasuryTargetValidityCheck(FieldObject Target, Card thisCard)
        {
            return (Target.CardType == CardTypes.monster && Target.Controller == thisCard.Controller);
        }
    }
}
