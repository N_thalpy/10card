using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class TristantheParagonCard : Card
	{

        public TristantheParagonCard()
            : base(7, 2, "용장 트리스탄", CardTypes.monster, new TristantheParagonField(), "TristantheParagon", "전투의 함성: 전장의 총 빈 칸 수의 3배만큼의 피해를 적 전장 전체에 입힌다.") 
		{ 
            
		}

	}
}
