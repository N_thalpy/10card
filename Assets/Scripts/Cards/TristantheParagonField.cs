using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class TristantheParagonField : FieldObject
	{
		
		public TristantheParagonField(): base(7, 2, CardTypes.monster, "TristantheParagon", "용장 트리스탄", 7, 10, false) 
		{
			NonTargetBattleCry += DealMassDamage;
		}
		
		
		void DealMassDamage(FieldObject thisObject)
		{
			int blanks = 0;
			float effectTime = Effect.EFC.EffectLastTime();
			for (int i = 0; i < 5; i++)
			{
				if (thisObject.Controller.Field[i].CardType == CardTypes.empty) blanks++;
				if (thisObject.Opponent.Field[i].CardType == CardTypes.empty) blanks++;
			}
			for (int i = 0; i < 5; i++)
			{
				FieldObject target = thisObject.Opponent.Field[i];
				if (target.CardType == CardTypes.monster)
				{
					Effect.AddEffect( new Effect( 1.0f, effectTime , target, target,  "Smoke") );
					GameAction.DealDamage(thisObject, target, 3 * blanks);
				}
			}
		}
		
	}
}
