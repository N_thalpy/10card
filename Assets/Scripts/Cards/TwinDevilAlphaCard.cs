using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class TwinDevilAlphaCard : Card
	{

        public TwinDevilAlphaCard()
            : base(4, 2, "쌍둥이 악마 알파", CardTypes.monster, new TwinDevilAlphaField(),"TwinDevilAlpha", "전투의 함성: 6/4 쌍둥이 악마 베타를 소환한다.") 
		{ 

		}


	}
}
