using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class TwinDevilAlphaField : FieldObject
	{
		
		public TwinDevilAlphaField(): base(4, 2, CardTypes.monster, "TwinDevilAlpha", "쌍둥이 악마 알파", 4, 6) 
		{
			NonTargetBattleCry += SummonBeta;
		}
		
		void SummonBeta(FieldObject thisObject)
		{
			
			for (int i = 0; i < 5; i++)
			{
				if (thisObject.Controller.Field[i].CardType == CardTypes.empty)
				{
					TwinDevilAlpha_BetaField beta = new TwinDevilAlpha_BetaField();
					beta.Controller = thisObject.Controller;
					beta.Opponent = thisObject.Opponent;
					GameAction.SummonAtPosition(thisObject.Controller.Field[i], beta, false);
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), beta, beta,  "Blood") );
					break;
				}
			}
		}
		
		
	}
}
