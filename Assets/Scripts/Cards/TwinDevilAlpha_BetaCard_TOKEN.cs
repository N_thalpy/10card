using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class TwinDevilAlpha_BetaCard : Card
	{

        public TwinDevilAlpha_BetaCard()
            : base(4, 2, "쌍둥이 악마 베타", CardTypes.monster, new TwinDevilAlpha_BetaField(), "TwinDevilAlpha_Beta") 
		{ 

		}


	}
}
