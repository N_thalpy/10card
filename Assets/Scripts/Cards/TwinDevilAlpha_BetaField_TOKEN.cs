using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class TwinDevilAlpha_BetaField : FieldObject
	{

		public TwinDevilAlpha_BetaField(): base(4, 2, CardTypes.monster, "TwinDevilAlpha_Beta", "쌍둥이 악마 베타", 6, 4, true) 
		{ 

		}


	}
}
