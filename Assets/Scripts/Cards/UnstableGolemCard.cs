using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class UnstableGolemCard : Card
	{

        public UnstableGolemCard()
            : base(2, 0, "불안정한 골렘", CardTypes.monster, new UnstableGolemField(),"UnstableGolem", "내 턴 시작마다 1/2의 확률로 스스로를 파괴한다.") 
		{ 

		}


	}
}
