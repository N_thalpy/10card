using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class UnstableGolemField : FieldObject
	{
		
		public UnstableGolemField(): base(2, 0, CardTypes.monster, "UnstableGolem", "불안정한 골렘", 5, 8) 
		{
			TurnStart += SuicideAtFiftyPercent;
		}
		
		void SuicideAtFiftyPercent(FieldObject thisObject)
		{
            if (thisObject.Controller.Turn && GameAction.IsFieldObjectOnField(thisObject) && thisObject.CardType == CardTypes.monster && GameAction.RandomInt(0, 2) == 1)
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), thisObject, thisObject,  "Explosion") );
				GameAction.DestroyFieldObject(thisObject);
			}
		}
		
	}
}
