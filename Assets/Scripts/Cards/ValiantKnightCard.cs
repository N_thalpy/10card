using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ValiantKnightCard : Card
	{

        public ValiantKnightCard()
            : base(5, 1, "용맹한 기사", CardTypes.monster, new ValiantKnightField(), "ValiantKnight", "전투의 함성: 3/5 종자를 전장에 소환한다.") 
		{ 

		}

	}
}
