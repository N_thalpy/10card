using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ValiantKnightField : FieldObject
	{
		
		public ValiantKnightField(): base(5, 1, CardTypes.monster, "ValiantKnight", "용맹한 기사", 6, 8, false) 
		{
			NonTargetBattleCry += SummonSquire;
		}
		
		void SummonSquire(FieldObject thisObject)
		{
			for (int i = 0; i < 5; i++)
			{
				FieldObject target = thisObject.Controller.Field[i];
				if (target.CardType == CardTypes.empty)
				{
					ValiantKnight_SquireField squire = new ValiantKnight_SquireField();
					squire.Controller = thisObject.Controller;
					squire.Opponent = thisObject.Opponent;
					GameAction.SummonAtPosition(target, squire, false);
					Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), target, target,  "Holy") );
					break;
				}
			}
		}
		
	}
}
