using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ValiantKnight_SquireCard : Card
	{

        public ValiantKnight_SquireCard()
            : base(0, 1, "종자", CardTypes.monster, new ValiantKnight_SquireField(), "ValiantKnight_Squire") 
		{ 

		}


	}
}
