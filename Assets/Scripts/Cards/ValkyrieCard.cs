using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ValkyrieCard : Card
	{

        public ValkyrieCard()
            : base(5, 3, "발키리", CardTypes.monster, new ValkyrieField(), "Valkyrie", "전투의 함성: 전장의 각 앞면 하수인에게 \"이 하수인이 죽을 때 바로 무료로 회수한다.\"를 부여한다.") 
		{ 

		}


	}
}
