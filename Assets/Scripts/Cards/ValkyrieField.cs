using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class ValkyrieField : FieldObject
	{
		
		public ValkyrieField(): base(5, 3, CardTypes.monster, "Valkyrie", "발키리", 6, 8, false) 
		{
			NonTargetBattleCry += GiveResurructionBuff;
		}
		
		void GiveResurructionBuff(FieldObject thisObject)
		{
			Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), thisObject, thisObject, "Holy") );
			if (thisObject.CardType != CardTypes.mystery) 
			{
				FieldObject fo;
				for (int i = 0; i < 5; i++)
				{
					fo = thisObject.Controller.Field[i];
					if (fo.CardType == CardTypes.monster)
						fo.Death += RetrieveAtFree;
				}
				
				for (int i = 0; i < 5; i++)
				{
					fo = thisObject.Opponent.Field[i];
					if (fo.CardType == CardTypes.monster)
						fo.Death += RetrieveAtFree;
				}  
			}
			
		}
		
		void RetrieveAtFree(FieldObject unit, FieldObject thisObject)
		{
			if (thisObject == unit && thisObject.IsToken == false )
			{
				Player owner = thisObject.LinkedCard.Controller;
				for (int i = 0; i < owner.Grave.Count; i++)
				{
					if (owner.Grave[i].Name == thisObject.LinkedCard.Name)
					{
						owner.Hand.Add(owner.Grave[i]);
						owner.Grave.Remove(owner.Grave[i]);
						GameAction.MainSystem.CardSort();
					}
				}
			}
		}
	}
}
