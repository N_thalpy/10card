using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class VeteranChampionCard : Card
	{

        public VeteranChampionCard()
            : base(6, 2, "역전의 용사", CardTypes.monster, new VeteranChampionField(),"VeteranChampion", "전투의 함성: 나의 체력이 상대보다 낮을 경우, +x/+x를 받는다. x는 나와 상대의 체력 차이의 1/3(버림)이다.") 
		{ 

		}


	}
}
