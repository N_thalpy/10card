using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class VeteranChampionField : FieldObject
	{
		
		public VeteranChampionField(): base(6, 2, CardTypes.monster, "VeteranChampion", "역전의 용사", 7, 11) 
		{
			NonTargetBattleCry += GivePlusBuff;
		}
		
		void GivePlusBuff(FieldObject thisObject)
		{
			Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), thisObject, thisObject,  "Holy") );
			int difdiv3 = (thisObject.Opponent.Controller.CurrentHp - thisObject.Controller.CurrentHp) / 3;
			if (difdiv3 > 0)
			{
				new StatBuff(thisObject, difdiv3, difdiv3, false).Initialize();
			}
			
		}
	}
}
