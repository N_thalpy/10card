﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class VoidPredatorCard : Card
    {
        public VoidPredatorCard()
            : base(13, 13, "공허의 포식자", CardTypes.monster, new VoidPredatorField(), "VoidPredator", "전투의 함성: 전장의 아무 카드 하나를 파괴하고 그 공/체만큼 자신의 공/체를 늘린다. (마법이면 0/0)")
        {
        }
    }
}
