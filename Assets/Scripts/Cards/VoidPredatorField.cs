﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class VoidPredatorField : FieldObject
    {
        public VoidPredatorField()
            : base(13, 13, CardTypes.monster, "VoidPredator", "공허의 포식자", 3, 3)
        {
			BattleCryTargetValidityCheck += VoidPredatorBattleCryTargetValidityCheck;
            TargetBattleCry += Eat;
        }

        void Eat(FieldObject target, FieldObject thisObject)
        {
            if (GameAction.IsFieldObjectOnField(target) && GameAction.IsFieldObjectOnField(thisObject))
            {
				float effectTime = Effect.EFC.EffectLastTime();
				Effect.AddEffect( new Effect( 1.0f, effectTime, thisObject, thisObject,  "Blood") );
				Effect.AddEffect( new Effect( 1.0f, effectTime, target, target,  "Explosion") );

                if (GameAction.IsFieldObjectMonster(target))
                {
                    int atk = target.CurrentAp;
                    int def = target.CurrentHp;
                    GameAction.DestroyFieldObject(target);
                    new StatBuff(thisObject, atk, def, false).Initialize();
                }
                else
                    GameAction.DestroyFieldObject(target);
            }
        }

        public bool VoidPredatorBattleCryTargetValidityCheck(FieldObject Target, FieldObject thisObject)
        {
            return VChecks.IsMonsterOrMystery(Target);
        }
    }
}
