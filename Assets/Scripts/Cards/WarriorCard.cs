using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class WarriorCard : Card
	{

        public WarriorCard()
			: base(1, 0, "용사", CardTypes.monster, new WarriorField(), "Warrior", "내 턴 시작마다 +4/+4를 받는다.",false) 
		{ 

		}

	}
}
