using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class WarriorField : FieldObject
	{
		
		public WarriorField(): base(1, 0, CardTypes.monster, "Warrior", "용사", 0, 7, false,"",false) 
		{ 
			TurnStart += AtHpBuff;
		}
		
		public void AtHpBuff(FieldObject Me)
		{
			if (Me.Controller.Turn) 
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), Me, Me,  "Holy") );
				StatBuff AHBuff = new StatBuff (Me, 4, 4);
				AHBuff.Initialize ();
			}
			
		}
	}
}
