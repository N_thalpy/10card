using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class WeaponBreakCard : Card
	{
		public WeaponBreakCard()
			: base(3, 3, "무기 파괴", CardTypes.magic, new MagicBox(), "WeaponBreak", "하수인 하나의 공격력을 1로 만든다.")
		{
			TargetValidityCheck += WeaponBreakTargetValidityCheck;
			TargetEvent += AtkToOne;
		}
		
		void AtkToOne(FieldObject target, Card thisCard)
		{
			if (GameAction.IsFieldObjectOnField(target))
			{
				Effect.AddEffect( new Effect( 1.0f, Effect.EFC.EffectLastTime(), target, target,  "Explosion") );
				StatBuff atkDebuff = new StatBuff(target, 1 - target.CurrentAp, 0, false);
				atkDebuff.Initialize();
			}
		}
		
		public bool WeaponBreakTargetValidityCheck(FieldObject Target, Card thisCard)
		{
			return VChecks.IsMonsterNotMyetery(Target);
		}
	}
}
