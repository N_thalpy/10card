using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class WebTrapCard : Card
    {
        public WebTrapCard()
            : base(0, 2, "그물 함정", CardTypes.magic, new MagicBox(), "WebTrap", "잠복: 이것 또는 내가 공격받으면 공격자의 공격력을 턴 끝까지 0으로 만든다.",true,true)
        {
             LinkedFieldObject.Attack += AtkToZero;
        }

        public void AtkToZero(FieldObject Attacker, FieldObject Defender, FieldObject Me)
        {
            if (Me.CardType == CardTypes.mystery && (Defender == Me || Defender == Me.Controller) && GameAction.IsFieldObjectOnField(Attacker))
            {
                if (Defender != Me) GameAction.Reverse(Me);
                Effect.AddEffect(new Effect(1.0f, Effect.EFC.EffectLastTime(), Attacker, Attacker, "Explosion"));
                StatBuff atkDebuff = new StatBuff(Attacker, -Attacker.CurrentAp, 0, true); 
                atkDebuff.Initialize();
            }
        }
    }
}
