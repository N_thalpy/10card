using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class WizardsDuelCard : Card
	{
		public WizardsDuelCard()
			: base(1, 1, "마법사의 결투", CardTypes.magic, new MagicBox(), "WizardsDuel", "하수인 하나를 정한다. 내가 상대보다 손에 마법 카드가 많으면 그 차이의 3배만큼의 피해를 그 하수인에게 입힌다.")
		{
			TargetEvent += DoDamage;
			TargetValidityCheck += WizardsDuelTargetValidityCheck;
		}
		
		public void DoDamage(FieldObject target, Card thisCard)
		{
			int myHandMagicCount = 0;
			int opHandMagicCount = 0;
			
			for (int i = 0; i < thisCard.Controller.Hand.Count; i++)
			{
				if (thisCard.Controller.Hand[i].CardType == CardTypes.magic)
					myHandMagicCount += 1;
			}
			
			for (int i = 0; i < thisCard.Opponent.Hand.Count; i++)
			{
				if (thisCard.Opponent.Hand[i].CardType == CardTypes.magic)
					opHandMagicCount += 1;
			}
			if (myHandMagicCount - opHandMagicCount > 0) 
			{
				float DuelTime = Effect.EFC.EffectLastTime();
				Effect DuelEffect = new Effect( 1.0f, DuelTime , target,target, "Vision" );
				Effect.AddEffect( DuelEffect );
				GameAction.DealDamage (thisCard.Controller, target, 3 * (myHandMagicCount - opHandMagicCount));
			}
		}
		public bool WizardsDuelTargetValidityCheck(FieldObject Target, Card thisCard)
		{
			return VChecks.IsMonsterNotMyetery(Target);
		}
	}
}
