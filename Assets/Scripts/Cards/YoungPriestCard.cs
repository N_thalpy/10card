using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class YoungPriestCard : Card
	{

        public YoungPriestCard()
            : base(2, 2, "젊은 사제", CardTypes.monster, new YoungPriestField(), "YoungPriest", "전투의 함성: 아군 하수인 하나에게 +1/+1을 부여한다.") 
		{ 

		}


	}
}
