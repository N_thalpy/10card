using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class YoungPriestField : FieldObject
	{

		public YoungPriestField(): base(2, 2, CardTypes.monster, "YoungPriest", "젊은 사제", 3, 5) 
		{
            TargetBattleCry += GivePlusBuff;
			BattleCryTargetValidityCheck += YoungPriestBattleCryTargetValidityCheck;
		}

        void GivePlusBuff(FieldObject target, FieldObject thisObject)
        {
			if (!GameAction.IsFieldObjectOnField(target)) return;
			Effect HappyEffect = new Effect( 1.0f, Effect.EFC.EffectLastTime() , target, target,"Holy" );
			Effect.AddEffect( HappyEffect );
            StatBuff statBuff = new StatBuff(target, 1, 1, false);
            statBuff.Initialize();
            

        }
        
        public bool YoungPriestBattleCryTargetValidityCheck(FieldObject Target, FieldObject thisObject)
        {

            return VChecks.IsMonsterNotMyetery(Target) && VChecks.IsMine(thisObject, Target);
        }
	}
}
