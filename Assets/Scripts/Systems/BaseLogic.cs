﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Base{

	public enum CardTypes { magic , monster, mystery, player, empty};

	public delegate void TargetingEvent( FieldObject AvatarTarget , FieldObject Unit );
	public delegate void NonTargetingEvent( FieldObject Unit );

	public static class Data
	{
		public static List<Card> NormalDictionary;
        public static List<Card> GoldDictionary;

		public static void DataInit()
        {
            NormalDictionary = new List<Card>();
            GoldDictionary = new List<Card>();

            Assembly asm = typeof(Card).Assembly;
            Type[] types = asm.GetTypes();
            Type type = null;
            for (int i = 0; i < types.Length; i++)
            {
                type = types[i];
                if (type == typeof(EmptyCard) ||
                    type == typeof(GhostKnightCard) ||
                    type == typeof(LuminousKnightCard) ||
                    type == typeof(MirrorEffigyCard) ||
                    type == typeof(SpyCard) ||
                    type == typeof(CounterattackdragonCard) ||
                    type == typeof(OverwhelmingdragonCard) ||
                    type == typeof(MasterCard) ||
                    type == typeof(DemonsHornCard) ||
                    type == typeof(SilverknightCard) ||
                    type == typeof(TheBlackPlagueCard))
                    continue;

                if (type.IsClass == true && type.BaseType == typeof(Card))
                {
                    Card c = (Card)type.GetConstructor(new Type[0]).Invoke(new System.Object[0]);
                    if (c.LinkedFieldObject.IsToken == true)
                        continue;

                    if (c.Recyclable == true)
                        NormalDictionary.Add(c);
                    else
                        GoldDictionary.Add(c);
                }
            }

            Comparison<Card> comp = (lhs, rhs) =>
            {
                /*
                if (lhs.PrintedCost == rhs.PrintedCost)
                    return lhs.Name.CompareTo(rhs.Name);
                else
                    return lhs.PrintedCost.CompareTo(rhs.PrintedCost);
                 */
                return lhs.Name.CompareTo(rhs.Name);
            };
            NormalDictionary.Sort(comp);
            GoldDictionary.Sort(comp);
		}
	}


	public static class Trigger
	{
		

		public static void PersistentTrigger( FieldObject Unit, FieldObject Me)
		{

		}

		public static void SummonTrigger( FieldObject Unit )
		{
			FieldObject Me;
			for (int i = 0; i <5; i ++) 
			{
				Me = Unit.Controller.Field[i];
                Me.Summon (Unit,Me);
				Me = Unit.Opponent.Field[i];
				Me.Summon (Unit,Me);
			}
			Card MeHand;
			for (int i = 0; i < Unit.Controller.Hand.Count; i ++) 
			{
				MeHand = Unit.Controller.Hand[i];
                MeHand.Summon(Unit, MeHand);
			}
			for (int i = 0; i < Unit.Opponent.Hand.Count; i ++) 
			{
				MeHand = Unit.Opponent.Hand[i];
                MeHand.Summon(Unit, MeHand);
			}

            LogSystem.Log(LogData.ActionType.Summon, Unit);
		}

		public static FieldObject ActiveTrigger( FieldObject Unit, FieldObject Position )
		{
			FieldObject Me;
			for (int i = 0; i <5; i ++) 
			{
				Me = Unit.Controller.Field[i];
				Unit = Me.Active (Unit,Me,Position);
				Me = Unit.Opponent.Field[i];
				Unit = Me.Active (Unit,Me,Position);
			}
			Card MeHand;
			for (int i = 0; i <Unit.Controller.Hand.Count; i ++) 
			{
				MeHand = Unit.Controller.Hand[i];
				Unit = MeHand.Active (Unit,MeHand,Position);
			}
			for (int i = 0; i <Unit.Opponent.Hand.Count; i ++) 
			{
				MeHand = Unit.Opponent.Hand[i];
				Unit = MeHand.Active (Unit,MeHand,Position);
			}


			return Unit;
		}

		public static void LeaveTrigger( FieldObject Unit)
		{
			FieldObject Me;
			for (int i = 0; i <5; i ++) 
			{
				Me = Unit.Controller.Field[i];
				Me.Leave (Unit,Me);
				Me = Unit.Opponent.Field[i];
				Me.Leave (Unit,Me);
			}
			Card MeHand;
			for (int i = 0; i <Unit.Controller.Hand.Count; i ++) 
			{
				MeHand = Unit.Controller.Hand [i];
				MeHand.Leave (Unit, MeHand);
			}
			for (int i = 0; i <Unit.Opponent.Hand.Count; i ++) 
			{
				MeHand = Unit.Opponent.Hand[i];
				MeHand.Leave(Unit,MeHand);
			}
		}

		public static void DeathTrigger( FieldObject Unit)
		{
			FieldObject Me;
            Unit.Death(Unit, Unit);
			for (int i = 0; i <5; i ++) 
			{
				Me = Unit.Controller.Field[i];
				Me.Death (Unit,Me);
				Me = Unit.Opponent.Field[i];
				Me.Death (Unit,Me);
			}
			Card MeHand;
			for (int i = 0; i <Unit.Controller.Hand.Count; i ++) 
			{
				MeHand = Unit.Controller.Hand [i];
				MeHand.Death (Unit, MeHand);
			}
			for (int i = 0; i <Unit.Opponent.Hand.Count; i ++) 
			{
				MeHand = Unit.Opponent.Hand[i];
				MeHand.Death(Unit,MeHand);
			}
		}

		public static void ReverseTrigger( FieldObject Unit, FieldObject attacker = null)
        {
            FieldObject Me;
            for (int i = 0; i < 5; i++)
            {
                Me = Unit.Controller.Field[i];
                Me.Reverse(Unit, Me, attacker);
                Me = Unit.Opponent.Field[i];
                Me.Reverse(Unit, Me, attacker);
            }
            Card MeHand;
            for (int i = 0; i < Unit.Controller.Hand.Count; i++)
            {
                MeHand = Unit.Controller.Hand[i];
                MeHand.Reverse(Unit, MeHand, attacker);
            }
            for (int i = 0; i < Unit.Opponent.Hand.Count; i++)
            {
                MeHand = Unit.Opponent.Hand[i];
                MeHand.Reverse(Unit, MeHand, attacker);
            }

            LogSystem.Log(LogData.ActionType.Reverse, Unit);
        }

		public static void MoveTrigger( FieldObject UnitToMove, FieldObject DestinationPosition)
		{
			FieldObject Me;
			for (int i = 0; i <5; i ++) 
			{
				Me = UnitToMove.Controller.Field[i];
				Me.Move (UnitToMove,Me,DestinationPosition);
				Me = UnitToMove.Opponent.Field[i];
				Me.Move (UnitToMove,Me,DestinationPosition);
			}
			Card MeHand;
			for (int i = 0; i <UnitToMove.Controller.Hand.Count; i ++) 
			{
				MeHand = UnitToMove.Controller.Hand [i];
				MeHand.Move (UnitToMove, MeHand,DestinationPosition);
			}
			for (int i = 0; i <UnitToMove.Opponent.Hand.Count; i ++) 
			{
				MeHand = UnitToMove.Opponent.Hand[i];
				MeHand.Move(UnitToMove,MeHand,DestinationPosition);
			}
		}


		public static FieldObject SpellTargetTrigger(Card Spell, FieldObject Target)
		{
			FieldObject Me;
			for (int i = 0; i <5; i ++) 
			{
				Me = Spell.Controller.Field[i];
				Target = Me.SpellTarget (Spell,Me,Target);
				Me = Spell.Opponent.Field[i];
				Target = Me.SpellTarget (Spell,Me,Target);
			}
            Card MeHand;
            
            for (int i = 0; i < Spell.Controller.Hand.Count; i++)
            {
                MeHand = Spell.Controller.Hand[i];
                Target = MeHand.SpellTarget(Spell, MeHand, Target);
            }
			for (int i = 0; i < Spell.Opponent.Hand.Count; i++)
			{
				MeHand = Spell.Opponent.Hand[i];
				Target = MeHand.SpellTarget(Spell, MeHand, Target);
			}

            LogSystem.Log(LogData.ActionType.Spell, Spell);
			return Target;
		}

		public static void SpellTrigger(Card Spell)
		{
			FieldObject Me;
			for (int i = 0; i <5; i ++) 
			{
				Me = Spell.Controller.Field[i];
				Me.Spell (Spell,Me);
				Me = Spell.Opponent.Field[i];
				Me.Spell (Spell,Me);
			}
            Card MeHand;
            for (int i = 0; i < Spell.Controller.Hand.Count; i++)
            {
                MeHand = Spell.Controller.Hand[i];
                MeHand.Spell(Spell, MeHand);
            }
			for (int i = 0; i < Spell.Opponent.Hand.Count; i++)
			{
				MeHand = Spell.Opponent.Hand[i];
				MeHand.Spell(Spell, MeHand);
			}

            LogSystem.Log(LogData.ActionType.Spell, Spell);
		}
        /*
        public static void RandomCaller( int startNum, int endNum, todo )
        {
            GameAction.MainSystem.StartCoroutine("RandomCaller", 1,2,todo);
        }
        */


		public static void DamageMarker ( FieldObject DamagedObject, int Damage )
		{
			Effect WordEffect = new Effect( 0, Effect.EFC.EffectLastTime() , DamagedObject,DamagedObject,"MessageBox" );
			WordEffect.DamageAmount = Damage;
			Effect.AddEffect( WordEffect );
		}

        
		public static void TurnStartTrigger()
		{
            FieldObject Me;

            for (int i = 0; i < 5; i++)
            {
                Me = GameAction.MainSystem.MePlayer.Field[i];
                Me.TurnStart(Me);
                Me = GameAction.MainSystem.OpponentPlayer.Field[i];
                Me.TurnStart(Me);
            }

            Card MeHand;

            for (int i = 0; i < GameAction.MainSystem.MePlayer.Hand.Count; i++)
            {
                MeHand = GameAction.MainSystem.MePlayer.Hand[i];
                MeHand.TurnStart(MeHand);
             }
            for (int i = 0; i < GameAction.MainSystem.OpponentPlayer.Hand.Count; i++)
			{
                MeHand = GameAction.MainSystem.OpponentPlayer.Hand[i];
				MeHand.TurnStart(MeHand);
			}
		}

		public static void TurnEndTrigger()
		{
            FieldObject Me;

            for (int i = 0; i < 5; i++)
            {
                Me = GameAction.MainSystem.MePlayer.Field[i];
                Me.TurnEnd(Me);
                Me = GameAction.MainSystem.OpponentPlayer.Field[i];
                Me.TurnEnd(Me);
            }

            Card MeHand;

            for (int i = 0; i < GameAction.MainSystem.MePlayer.Hand.Count; i++)
            {
                MeHand = GameAction.MainSystem.MePlayer.Hand[i];
                MeHand.TurnEnd(MeHand);
            }
            for (int i = 0; i < GameAction.MainSystem.OpponentPlayer.Hand.Count; i++)
			{
                MeHand = GameAction.MainSystem.OpponentPlayer.Hand[i];
				MeHand.TurnEnd(MeHand);
			}
		}

        public static void MyReverseTrigger(FieldObject Unit)
        {
            GameAction.MainSystem.Clicked = false;
            GameAction.MainSystem.StartCoroutine("ReversePhase", Unit);
        }

        public static void RetrieveTrigger(Card RetrievedCard)
        {
			FieldObject Me;

			for (int i =0; i < 5; i ++) 
			{
				Me = GameAction.MainSystem.MePlayer.Field[i];
				Me.Retrieve(RetrievedCard, Me);
				Me = GameAction.MainSystem.OpponentPlayer.Field[i];
				Me.Retrieve(RetrievedCard, Me);
			}

			Card MeHand;

			for (int i = 0; i < GameAction.MainSystem.MePlayer.Hand.Count; i++) 
			{
				MeHand = GameAction.MainSystem.MePlayer.Hand[i];
				MeHand.Retrieve(RetrievedCard, MeHand );
			}
			for (int i = 0; i < GameAction.MainSystem.OpponentPlayer.Hand.Count; i++) 
			{
				MeHand = GameAction.MainSystem.OpponentPlayer.Hand[i];
				if ( MeHand == null )
					Debug.Log(i);
				MeHand.Retrieve( RetrievedCard, MeHand );
			}
        }
        public static void EnemyRetrieveTrigger(Card User)
        {
            GameAction.MainSystem.Clicked = false;
			GameAction.MainSystem.EnemyRetrieve (User);
        }

		public static void TargetingBattleCry ( Card User )
		{
            GameAction.MainSystem.Clicked = false;
            GameAction.MainSystem.StartCoroutine("TargetingBattleCryPhase", User);
		}

        public static void NonTargetingSpell (Card User)
        {
            GameAction.MainSystem.Clicked = false;
            GameAction.MainSystem.StartCoroutine("NonTargetingSpellPhase", User);
        }

		public static void RetrieveCoroutineStart(Card User)
		{
			GameAction.MainSystem.Clicked = false;
			GameAction.MainSystem.StartCoroutine ("MyRetrievePhase", User);
		}

		public static void TargetingSpell ( Card User )
		{
            GameAction.MainSystem.Clicked = false;
            GameAction.MainSystem.StartCoroutine("TargetingSpellPhase", User);
		}

		public static void BackToMain ()
		{
            GameAction.MainSystem.Clicked = false;
            GameAction.MainSystem.SelectedCard = null;
            GameAction.MainSystem.SelectedObject = null;
            GameAction.MainSystem.StartCoroutine("MainPhase");
		}

		public static void BackToBattle ()
		{
			GameAction.MainSystem.Clicked = false;
			GameAction.MainSystem.SelectedCard = null;
			GameAction.MainSystem.SelectedObject = null;
			GameAction.MainSystem.StartCoroutine("BattlePhase");
		}


		public static void BackToInfo ()
		{
            GameAction.MainSystem.Clicked = false;
            GameAction.MainSystem.SelectedCard = null;
            GameAction.MainSystem.SelectedObject = null;
            GameAction.MainSystem.StartCoroutine("InfoPhase");
		}

		public static void Summoning ( Card User )
		{
            GameAction.MainSystem.Clicked = false;
            GameAction.MainSystem.StartCoroutine("Summon", User);
		}

		//public void Summoning ( 
	}

    public static class GameAction
    {

        public static GameSystem MainSystem;

        /// <summary>
        /// min을 범위에 포함하나 max를 범위에 포함하지 않습니다.
        /// </summary>
        public static int RandomInt(int min, int max)
        {
            int q;
            if (MainSystem.isConnected)
            {
                q = MainSystem.RandomQueue.Dequeue();
                MainSystem.RandomQueue.Enqueue(q);
                return (q % (max - min)) + min;
            }
            else
                return UnityEngine.Random.Range(min, max);
        }

        public static void MoveObject(FieldObject UnitToMove, FieldObject DestinationPosition)
        {
            //Debug.Assert(UnitToMove.Controller == DestinationPosition.Controller);
            if (DestinationPosition.CardType != CardTypes.empty)
            {
                Debug.Log("ERROR: Tried to move to a nonempty position");
                return;
            }

            int tmp = UnitToMove.GetPosition();
            UnitToMove.Controller.Field[DestinationPosition.GetPosition()] = UnitToMove;
            UnitToMove.Controller.Field[tmp] = DestinationPosition;

            Debug.Log("MOVED!");
            Trigger.MoveTrigger(UnitToMove, DestinationPosition);
        }

        public static void Reverse(FieldObject fieldObject, FieldObject attacker = null)
        {
            if (fieldObject.CardType != CardTypes.mystery)
                Debug.LogError("ERROR: Tried to reverse a non-mystery card!");

//            if (fieldObject.LinkedCard.CardType == CardTypes.monster)
                fieldObject.CardType = CardTypes.monster;
            if (fieldObject.Controller.Turn)
				fieldObject.Activity = false;
            

            Trigger.ReverseTrigger(fieldObject, attacker);
        }

        public static void Attack(FieldObject Attacker, FieldObject Defender)
        {
            if (Attacker.Activity == false)
            {
                return;
            }
            FieldObject Me;
            Card MeHand;
            bool reversed = false;

            for (int i = 0; i < 5; i++)
            {
                Me = Attacker.Controller.Field[i];
                Me.Attack(Attacker, Defender, Me);
                Me = Attacker.Opponent.Field[i];
                Me.Attack(Attacker, Defender, Me);
            }
            for (int i = 0; i < Attacker.Controller.Hand.Count; i++)
            {
                MeHand = Attacker.Controller.Hand[i];
                MeHand.Attack(Attacker, Defender, MeHand);
            }
            for (int i = 0; i < Attacker.Opponent.Hand.Count; i++)
            {
                MeHand = Attacker.Opponent.Hand[i];
                MeHand.Attack(Attacker, Defender, MeHand);
            }
            if (Defender.CardType == Base.CardTypes.mystery)
            {
                /*
                Defender.CardType = CardTypes.monster;
                for (int i = 0; i < 5; i++)
                {
                    Me = Defender.Controller.Field[i];
                    Me.Reverse(Defender, Me);
                    Me = Defender.Opponent.Field[i];
                    Me.Reverse(Defender, Me);
                }

                for (int i = 0; i < Defender.Controller.Hand.Count; i++)
                {
                    MeHand = Defender.Controller.Hand[i];
                    MeHand.Reverse(Defender, MeHand);
                }
                for (int i = 0; i < Defender.Opponent.Hand.Count; i++)
                {
                    MeHand = Defender.Opponent.Hand[i];
                    MeHand.Reverse(Defender, MeHand);
                }*/
                reversed = true;
                GameAction.Reverse(Defender, Attacker);
            }

            for (int i = 0; i < 5; i++)
            {
                Me = Defender.Controller.Field[i];
                Me.Defend(Defender, Attacker, Me);
                Me = Defender.Opponent.Field[i];
                Me.Defend(Defender, Attacker, Me);
            }
            for (int i = 0; i < Defender.Controller.Hand.Count; i++)
            {
                MeHand = Defender.Controller.Hand[i];
                MeHand.Defend(Defender, Attacker, MeHand);
            }
            for (int i = 0; i < Defender.Opponent.Hand.Count; i++)
            {
                MeHand = Defender.Opponent.Hand[i];
                MeHand.Defend(Defender, Attacker, MeHand);
            }
            if (GameAction.IsFieldObjectOnField(Defender)|| Defender.CardType == CardTypes.player )
            {
                DealDamage(Attacker, Defender, Attacker.CurrentAp);
                if (!reversed && (Defender.CardType != CardTypes.player))
                    DealDamage(Defender, Attacker, Defender.CurrentAp);
            }
			Attacker.Activity = false;
        }

        public static void DealDamage(FieldObject DamageGiver, FieldObject DamageTaker, int DamageAmount)
        {
            if (DamageTaker.CardType != CardTypes.player && !GameAction.IsFieldObjectOnField(DamageTaker)) return;
            int ActualDamage = DamageAmount;

			FieldObject Me;
			Card MeHand;

			for (int i = 0; i < 5; i++)
			{
				Me = DamageGiver.Controller.Field[i];
				if (DamageGiver != Me)
				{
					ActualDamage = Me.Damaging(DamageGiver, DamageTaker, ActualDamage, Me);
				}
				
				Me = DamageGiver.Opponent.Field[i];
				if (DamageGiver != Me)
				{
					ActualDamage = Me.Damaging(DamageGiver, DamageTaker, ActualDamage, Me);
				}
				
			}

            for (int i = 0; i < DamageGiver.Controller.Hand.Count; i++)
            {
                MeHand = DamageGiver.Controller.Hand[i];
				ActualDamage = MeHand.Damaging(DamageGiver, DamageTaker, ActualDamage, MeHand);
            }
            for (int i = 0; i < DamageGiver.Opponent.Hand.Count; i++)
            {
                MeHand = DamageGiver.Opponent.Hand[i];
				ActualDamage = MeHand.Damaging(DamageGiver, DamageTaker, ActualDamage, MeHand);
            }

            for (int i = 0; i < 5; i++)
            {
                Me = DamageTaker.Controller.Field[i];
                if (DamageTaker != Me)
                {
					ActualDamage = Me.Damaged(DamageTaker, DamageGiver, ActualDamage, Me);

                }

                Me = DamageTaker.Opponent.Field[i];
                if (DamageTaker != Me)
                {
					ActualDamage = Me.Damaged(DamageTaker, DamageGiver, ActualDamage, Me);
                }

            }
            for (int i = 0; i < DamageTaker.Controller.Hand.Count; i++)
            {
                MeHand = DamageTaker.Controller.Hand[i];
                ActualDamage = MeHand.Damaged(DamageTaker, DamageGiver, ActualDamage, MeHand);
            }
            for (int i = 0; i < DamageTaker.Opponent.Hand.Count; i++)
            {
                MeHand = DamageTaker.Opponent.Hand[i];
				ActualDamage = MeHand.Damaged(DamageTaker, DamageGiver, ActualDamage, MeHand);
            }

			ActualDamage = DamageGiver.Damaging(DamageGiver, DamageTaker, ActualDamage, DamageGiver);
			ActualDamage = DamageTaker.Damaged(DamageTaker, DamageGiver, ActualDamage, DamageTaker);
			
			if (ActualDamage <= 0) return;

			Debug.Log (DamageTaker + " get Damage");
            DamageTaker.CurrentHp -= ActualDamage;
            Trigger.DamageMarker(DamageTaker, ActualDamage);

            if (DamageTaker.CurrentHp <= 0)
                if (DamageTaker.CardType != CardTypes.player)
                    GameAction.DestroyFieldObject(DamageTaker);
                else
                    GameAction.MainSystem.GameEnd(DamageTaker.Opponent);
        }


        public static bool IsCardInGrave(Card CheckingCard)
        {
            if (CheckingCard.Controller == null)
                Debug.LogError("No controller for " + CheckingCard.Name);
            for (int i = 0; i < CheckingCard.Controller.Grave.Count; i++)
            {
                if (CheckingCard == CheckingCard.Controller.Grave[i])
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsFieldObjectOnField(FieldObject fieldObject)
        {
            if (fieldObject == null || fieldObject.Controller == null)
                return false;
            if (fieldObject.CardType == CardTypes.player) return true;
            bool res = false;
            for (int i = 0; i < 5; i++)
            {
                if (fieldObject.Controller.Field[i] == fieldObject)
                    res = true;
            }
            return res;
        }

        public static void SummonAtPosition(FieldObject Position, FieldObject ObjectToSummon, bool isMystery = false)
        {
            if (Position.CardType != CardTypes.empty)
                Debug.LogError("ERROR! Tried to summon in a nonempty position.");

			GameObject SummoningPos = MainSystem.FieldToUI (Position);
			SummoningPos.GetComponent<UIChanger> ().PlayerField = ObjectToSummon;
			Position.Controller.Field[Position.GetPosition()] = ObjectToSummon;


            ObjectToSummon.Controller = Position.Controller;
            ObjectToSummon.Opponent = Position.Opponent;

            ObjectToSummon.Activity = !ObjectToSummon.Controller.Turn;

            if (isMystery)
            {
                Debug.Log("Mystery Summon");
                ObjectToSummon.CardType = CardTypes.mystery;
            }

            Trigger.SummonTrigger(ObjectToSummon);
        }

        public static void ReturnToHand(FieldObject ObjectToReturn)
        {

            FieldObject empty = new EmptyField();
            ObjectToReturn.Controller.Field[ObjectToReturn.GetPosition()] = empty;
            empty.Controller = ObjectToReturn.Controller;
            empty.Opponent = ObjectToReturn.Opponent;
            if (!ObjectToReturn.IsToken)
            {
                Card newCard = (Card)Activator.CreateInstance(ObjectToReturn.LinkedCard.GetType());
                newCard.Controller = ObjectToReturn.LinkedCard.Controller;
                newCard.Opponent = ObjectToReturn.LinkedCard.Opponent;
                newCard.LinkedFieldObject.Controller = newCard.Controller;
                newCard.LinkedFieldObject.Opponent = newCard.Opponent;
                //카드가 손에 갈 때 같은 종류의 새 카드로 바꿔넣음.
                
                ObjectToReturn.LinkedCard.Controller.Hand.Add(newCard);
            }
            Trigger.LeaveTrigger(ObjectToReturn);

			MainSystem.UIChange ();
            MainSystem.CardSort();
        }

        public static void DestroyFieldObject(FieldObject ObjectToDestroy)
        {
            Debug.Log("Destroy " + ObjectToDestroy.Name);
            FieldObject empty = new EmptyField();
			MainSystem.FieldToUI (ObjectToDestroy.Controller.Field[ObjectToDestroy.GetPosition()]).GetComponent<UIChanger> ().PlayerField = empty;
            ObjectToDestroy.Controller.Field[ObjectToDestroy.GetPosition()] = empty;
            empty.Controller = ObjectToDestroy.Controller;
            empty.Opponent = ObjectToDestroy.Opponent;
            if (!ObjectToDestroy.IsToken && ObjectToDestroy.LinkedCard.Recyclable)
            {
                Card newCard = (Card)Activator.CreateInstance(ObjectToDestroy.LinkedCard.GetType());
                newCard.Controller = ObjectToDestroy.LinkedCard.Controller;
                newCard.Opponent = ObjectToDestroy.LinkedCard.Opponent;
                newCard.LinkedFieldObject.Controller = newCard.Controller;
                newCard.LinkedFieldObject.Opponent = newCard.Opponent;
                //카드가 무덤에 갈 때 같은 종류의 새 카드로 바꿔넣음.

				newCard.UsingThisTurn = true;

                ObjectToDestroy.LinkedCard.Controller.Grave.Add(newCard);
            }
            Trigger.DeathTrigger(ObjectToDestroy);
            Trigger.LeaveTrigger(ObjectToDestroy);
            GameAction.MainSystem.CardSort();

        }

        public static void GiveControl(FieldObject fieldObject, Player player)
        {
            if (fieldObject.Controller == player) return;
            for (int i = 0; i < 5; i++)
            {
                if (player.Field[i].CardType == CardTypes.empty)
                {
                    FieldObject empty = player.Field[i];
                    fieldObject.Controller.Field[fieldObject.GetPosition()] = empty;
                    player.Field[i] = fieldObject;

                    fieldObject.Controller = player;
                    fieldObject.Opponent = player.Opponent;
                    empty.Controller = player.Opponent;
                    empty.Opponent = player;

                    break;
                }
            }
			for (int i = 0; i < MainSystem.MyField.Length; i++) 
			{
				MainSystem.MyField[i].GetComponent<UIChanger>().PlayerField = MainSystem.MePlayer.Field[i];
			}
			for (int i = 0; i < MainSystem.MyField.Length; i++) 
			{
				MainSystem.OpponentField[i].GetComponent<UIChanger>().PlayerField = MainSystem.OpponentPlayer.Field[i];
			}

        }

        public static bool IsFieldObjectMonster(FieldObject fieldObject)
        {
            return (fieldObject.CardType == CardTypes.monster || (fieldObject.CardType == CardTypes.mystery && fieldObject.LinkedCard.CardType == CardTypes.monster));
        }

		public static int EmptyFieldLocation (Player CallPlayer)
		{
			for (int i = 0; i < CallPlayer.Field.Count; i++) 
			{
				if(CallPlayer.Field[i].CardType == CardTypes.empty)
					return i;
			}

			return -1;
		}

		public static void Heal(FieldObject Target, int HealAmount)
		{
			Target.CurrentHp += HealAmount;
			if (Target.CurrentHp > Target.CurrentMaxHp)
				Target.CurrentHp = Target.CurrentMaxHp;
		}
    }

	public abstract class Card 
	{
		public string ImagePath;
        public string Name;
		public Player Controller;
		public Player Opponent;
		
		public CardTypes CardType;
		public bool Grave = false;

		public int PrintedCost;
		public int PrintedGraveCost; 
		public int CurrentCost;
		public int CurrentGraveCost;
		public string Text;
		public bool Recyclable;
		public bool Stakeout;

		public FieldObject LinkedFieldObject;

		// event triggered by automatically
		public delegate void AttackEventHandler( FieldObject Attacker, FieldObject Defender,Card Me );
		public delegate void DefendEventHandler( FieldObject Defender, FieldObject Attacker,Card Me );
		public delegate void MoveEventHandler( FieldObject Unit,Card Me, FieldObject PositionBefore );
		public delegate void SummonEventHandler ( FieldObject Unit,Card Me );
		public delegate void LeaveEventHandler ( FieldObject Unit,Card Me );
		public delegate void DeathEventHandler ( FieldObject Unit,Card Me );
		public delegate void ReverseEventHandler ( FieldObject Unit,Card Me, FieldObject attacker = null );
		public delegate FieldObject SpellTargetEventHandler ( Card Spell , Card Me, FieldObject Target );
		public delegate void SpellEventHandler ( Card Spell , Card Me );
		public delegate FieldObject ActiveTargetEventHandler ( FieldObject User, Card Me, FieldObject Position );
		public delegate int DamagingEventHandler ( FieldObject DamageGiver, FieldObject DamageTaker, int DamageAmount,Card Me );
		public delegate int DamagedEventHandler ( FieldObject DamageTaker, FieldObject DamageGiver, int DamageAmount,Card Me );
		public delegate void TurnStartEventHandler ( Card Me );
		public delegate void TurnEndEventHandler ( Card Me);
        public delegate void CardWentToGraveEventHandler(Card BuriedCard, Card Me);
        public delegate void RetrieveTrigger(Card RetrievedCard, Card Me);


        public AttackEventHandler Attack = delegate { };
        public DefendEventHandler Defend = delegate { };
        public MoveEventHandler Move = delegate { };
        public SummonEventHandler Summon = delegate { };
        public LeaveEventHandler Leave = delegate { };
        public DeathEventHandler Death = delegate { };
        public ReverseEventHandler Reverse = delegate { };
        public SpellTargetEventHandler SpellTarget = new SpellTargetEventHandler(
            (Card Spell, Card Me, FieldObject Target) =>
            {
                return Target;
            });
        public SpellEventHandler Spell = delegate { };
        public ActiveTargetEventHandler Active = new ActiveTargetEventHandler(
            (FieldObject User, Card Me, FieldObject Target) =>
            {
                return Target;
            });
        public DamagingEventHandler Damaging = new DamagingEventHandler(
            (FieldObject DamageGiver, FieldObject DamageTaker, int DamageAmount, Card Me) =>
            {
                return DamageAmount;
            });
        public DamagedEventHandler Damaged = new DamagedEventHandler(
            (FieldObject DamageTaker, FieldObject DamageGiver, int DamageAmount, Card Me) =>
            {
                return DamageAmount;
            });
        public TurnStartEventHandler TurnStart = delegate { };
        public TurnEndEventHandler TurnEnd = delegate { };
        public CardWentToGraveEventHandler CardWentToGrave = delegate { };
        public RetrieveTrigger Retrieve = delegate { };

		// event caused by player ( click, targeting etc )
		public delegate void TargetingEvent( FieldObject Target , Card thisCard );
		public delegate void NonTargetingEvent( Card thisCard);

		public TargetingEvent TargetEvent;
		public NonTargetingEvent NonTargetEvent;

		public delegate bool TargetValidityCheckTrigger( FieldObject Target, Card thisCard );

		public TargetValidityCheckTrigger TargetValidityCheck;

		
		private bool BasicTargetValidityCheck (FieldObject Target, Card thisCard) 
		{
			if (this.CardType != CardTypes.magic)
			Debug.LogError("ERROR: Validity Check Called in Non-Magic Card");
			
			return (VChecks.IsMonsterNotMyetery(Target) || VChecks.isPlayer(Target));
		}

        public bool UsingThisTurn = false;

		protected Card(int Cost, int GraveCost, string Name, CardTypes CardType, FieldObject LinkedFieldObject, string ImagePath, string text = "",bool recyclable = true, bool stakeout = false)
        {
            this.ImagePath = ImagePath;

            this.PrintedCost = CurrentCost = Cost;
            this.PrintedGraveCost = CurrentGraveCost = GraveCost;
            this.Name = Name;
            this.CardType = CardType;
            this.Text = text;
            this.LinkedFieldObject = LinkedFieldObject;
            this.LinkedFieldObject.LinkedCard = this;
			this.Recyclable = recyclable;
			this.Stakeout = stakeout;
			this.TargetValidityCheck += BasicTargetValidityCheck;

			if (this.CardType == CardTypes.monster)
            {
                if (this.LinkedFieldObject.Text == "")
                    this.LinkedFieldObject.Text = this.Text;

                if (LinkedFieldObject.PrintedCost != this.PrintedCost)
                    Debug.LogError("ERROR: This card's cost is different from that of its linked field object. - " + Name);
                if (LinkedFieldObject.PrintedGraveCost != this.PrintedGraveCost)
                    Debug.LogError("ERROR: This card's gravecost is different from that of its linked field object. - " + Name);
                if (LinkedFieldObject.Name != this.Name)
                    Debug.LogError("ERROR: This card's name is different from that of its linked field object. - " + Name + " " + LinkedFieldObject.Name);
                if (LinkedFieldObject.ImagePath != this.ImagePath)
                    Debug.LogError("ERROR: This card's image path is different from that of its linked field object. - " + Name);
                if (LinkedFieldObject.CardType != this.CardType)
                    Debug.LogError("ERROR: This card's type is different from that of its linked field object, - " + Name);
                
            }
            else if (this.CardType == CardTypes.magic)
            {
                if (LinkedFieldObject.Name != "MagicBox")
                    Debug.LogError("ERROR: Magic card isn't linked to magicbox! - " + Name);
                LinkedFieldObject.PrintedCost = LinkedFieldObject.CurrentCost = Cost;
                LinkedFieldObject.PrintedGraveCost = LinkedFieldObject.CurrentGraveCost = GraveCost;
                LinkedFieldObject.Name = Name;
                LinkedFieldObject.ImagePath = ImagePath;
                LinkedFieldObject.Text = Text;
            }
            if (this.LinkedFieldObject == null)
                Debug.LogError("ERROR: This card's linked field object is null! - " + Name);
            //this.ImagePath = name;
        }

		public  Card CardCopy ( Card CopyingCard )
		{
			this.Active = CopyingCard.Active;
			this.Attack = CopyingCard.Attack;
			this.CardType = CopyingCard.CardType;
			this.CardWentToGrave = CopyingCard.CardWentToGrave;
			//this.Controller = CopyingCard.Controller;
			this.CurrentCost = CopyingCard.CurrentCost;
			this.CurrentGraveCost = CopyingCard.CurrentGraveCost;
			this.Damaged = CopyingCard.Damaged;
			this.Damaging = CopyingCard.Damaging;
			this.Death = CopyingCard.Death;
			this.Defend = CopyingCard.Defend;
			this.Grave = CopyingCard.Grave;
			this.ImagePath = CopyingCard.ImagePath;
			this.Leave = CopyingCard.Leave;
			this.Move = CopyingCard.Move;
			this.Name = CopyingCard.Name;
			this.NonTargetEvent = CopyingCard.NonTargetEvent;
			//this.Opponent = CopyingCard.Opponent;
			this.PrintedCost = CopyingCard.PrintedCost;
			this.PrintedGraveCost = CopyingCard.PrintedGraveCost;
			this.Retrieve = CopyingCard.Retrieve;
			this.Reverse = CopyingCard.Reverse;
			this.Spell = CopyingCard.Spell;
			this.SpellTarget = CopyingCard.SpellTarget;
			this.Summon = CopyingCard.Summon;
			this.TargetEvent = CopyingCard.TargetEvent;
			this.Text = CopyingCard.Text;
			this.TurnEnd = CopyingCard.TurnEnd;
			this.TurnStart = CopyingCard.TurnStart;
			this.Recyclable = CopyingCard.Recyclable;
			FieldObject CopyObjectTemp = (FieldObject)Activator.CreateInstance (CopyingCard.LinkedFieldObject.GetType ());
			CopyObjectTemp.FieldObjectCopy( CopyingCard.LinkedFieldObject );
			
			return this;
		}



	}



	public abstract class FieldObject
	{
        public string ImagePath;
        public string Name;
		public int PrintedHp;
		public int CurrentHp;
		public int CurrentMaxHp;
		
		
		public int PrintedAp;
		public int CurrentAp;

		public int PrintedCost;
		public int PrintedGraveCost;
		public int CurrentCost;
		public int CurrentGraveCost;
		public CardTypes CardType;
		public bool Activity = false;
		public bool Recyclable;

        public List<int> Counters = new List<int>();

		public Player Controller;
		public Player Opponent;

        public Card LinkedCard;

        public readonly bool IsToken;
		public string Text;

		// event triggered by automatically
		public delegate void AttackEventHandler( FieldObject Attacker, FieldObject Defender,FieldObject Me );
		public delegate void DefendEventHandler( FieldObject Defender, FieldObject Attacker,FieldObject Me );
		public delegate void MoveEventHandler( FieldObject UnitMoved, FieldObject Me, FieldObject PositionBefore );
		public delegate void SummonEventHandler ( FieldObject Unit,FieldObject Me );
		public delegate void LeaveEventHandler ( FieldObject Unit,FieldObject Me );
		public delegate void DeathEventHandler ( FieldObject Unit,FieldObject Me );
		public delegate void ReverseEventHandler ( FieldObject Unit, FieldObject Me, FieldObject attacker = null );
		public delegate FieldObject SpellTargetEventHandler ( Card Spell , FieldObject Me, FieldObject Target );
		public delegate void SpellEventHandler ( Card Spell , FieldObject Me );
		public delegate FieldObject ActiveTargetEventHandler ( FieldObject User, FieldObject Me, FieldObject Target );
		public delegate int DamagingEventHandler ( FieldObject DamageGiver, FieldObject DamageTaker, int DamageAmount,FieldObject Me );
		public delegate int DamagedEventHandler ( FieldObject DamageTaker, FieldObject DamageGiver, int DamageAmount, FieldObject Me );
		public delegate void TurnStartEventHandler ( FieldObject Me );
		public delegate void TurnEndEventHandler ( FieldObject Me);
        public delegate void CardWentToGraveEventHandler(Card BuriedCard, FieldObject Me);
        public delegate void RetrieveTrigger(Card RetrievedCard, FieldObject Me);


        public AttackEventHandler Attack = delegate { };
        public DefendEventHandler Defend = delegate { };
        public MoveEventHandler Move = delegate { };
        public SummonEventHandler Summon = delegate { };
        public LeaveEventHandler Leave = delegate { };
        public DeathEventHandler Death = delegate { };
        public ReverseEventHandler Reverse = delegate { };
        public SpellTargetEventHandler SpellTarget = new SpellTargetEventHandler(
            (Card Spell, FieldObject Me, FieldObject Target) =>
            {
                return Target;
            });
        public SpellEventHandler Spell = delegate { };
        public ActiveTargetEventHandler Active = new ActiveTargetEventHandler(
            (FieldObject User, FieldObject Me, FieldObject Target) =>
            {
                return Target;
            });
        public DamagingEventHandler Damaging = new DamagingEventHandler(
            (FieldObject DamageGiver, FieldObject DamageTaker, int DamageAmount, FieldObject Me) =>
            {
                return DamageAmount;
            });
        public DamagedEventHandler Damaged = new DamagedEventHandler(
            (FieldObject DamageTaker, FieldObject DamageGiver, int DamageAmount, FieldObject Me) =>
            {
                return DamageAmount;
            });
        public TurnStartEventHandler TurnStart = delegate { };
        public TurnEndEventHandler TurnEnd = delegate { };
        public CardWentToGraveEventHandler CardWentToGrave = delegate { };
        public RetrieveTrigger Retrieve = delegate { };

		// event caused by player ( click, targeting etc )

		public delegate void TargetingBattleCry( FieldObject Target , FieldObject thisObject );
		public delegate void NonTargetingBattleCry( FieldObject thisObject );
		
		public TargetingBattleCry TargetBattleCry;
        public NonTargetingBattleCry NonTargetBattleCry = delegate { };

        public List<Buff> BuffList = new List<Buff>();

        protected FieldObject(int Cost, int GraveCost, CardTypes CardType, string ImagePath, string Name, int Ap = -1, int MaxHp = -1, bool IsToken = false, string text = "", bool recyclable = true)
        {

            this.Name = Name;
            this.ImagePath = ImagePath;
            this.CardType = CardType;
            this.PrintedCost = CurrentCost = Cost;
            this.PrintedGraveCost = CurrentGraveCost = GraveCost;
            this.PrintedAp = CurrentAp = Ap;
            this.PrintedHp = CurrentMaxHp = CurrentHp = MaxHp;
            this.IsToken = IsToken;
            this.Text = text;
			this.Recyclable = recyclable;
			AttackValidityCheck += BasicAttackValidityCheck;
			BattleCryTargetValidityCheck += BasicBattleCryTargetValidityCheck;
        }

        
        public static int FindPosition(FieldObject fieldObject)
        {
            for (int i = 0; i < 5; i++)
            {
                if (fieldObject.Controller == null)
                    Debug.LogError("Eror: This fieldobject has no controller!");
                if (fieldObject.Controller.Field[i] == fieldObject)
                    return i;
            }
            Debug.LogError("ERROR: Wrong usage of FindPosition by " + fieldObject.Name + ".");
            return -1;
        }

        public int GetPosition()
        {
            return FindPosition(this);
        }


		public delegate bool AttackValidityCheckTrigger( FieldObject AttackTarget, FieldObject thisObject );
		
		public AttackValidityCheckTrigger AttackValidityCheck;
				
		private bool BasicAttackValidityCheck (FieldObject AttackTarget, FieldObject thisObject) 
		{
			if (this.CardType != CardTypes.monster)
				Debug.LogError("ERROR: Attack Validity Check Called in Non-Monster FieldObject");
			return (AttackTarget.CardType != CardTypes.empty);
		}

		public delegate bool BattleCryTargetValidityCheckTrigger( FieldObject Target, FieldObject thisObject );
		
		public BattleCryTargetValidityCheckTrigger BattleCryTargetValidityCheck;
		
		private bool BasicBattleCryTargetValidityCheck (FieldObject Target, FieldObject thisObject) 
		{
			if (this.CardType != CardTypes.monster)
				Debug.LogError("ERROR: Target Validity Check Called in Non-Monster FieldObject");
            Debug.Log(Target.Name);
			return (VChecks.IsMonsterNotMyetery(Target) || VChecks.isPlayer(Target));
		}

        public void DeleteAllBuffs()
        {
            for (int i = 0; i < BuffList.Count; i++)
            {
                BuffList[i].Terminate();
            }
        }

		public  FieldObject FieldObjectCopy ( FieldObject CopyingCard )
		{
			for (int i = 0; i < CopyingCard.BuffList.Count; i++) 
			{
				CopyingCard.BuffList[i].BuffCopy( this );
			}
			
			this.Active = CopyingCard.Active;
			this.Attack = CopyingCard.Attack;
			this.CardWentToGrave = CopyingCard.CardWentToGrave;
			this.CardType = CopyingCard.CardType;
			//this.Controller = CopyingCard.Controller;
			this.Counters = CopyingCard.Counters;
			this.CurrentAp = CopyingCard.CurrentAp;
			this.CurrentCost = CopyingCard.CurrentCost;
			this.CurrentGraveCost = CopyingCard.CurrentGraveCost;
			this.CurrentHp = CopyingCard.CurrentHp;
            this.CurrentMaxHp = CopyingCard.CurrentMaxHp;
			this.Damaged = CopyingCard.Damaged;
			this.Damaging = CopyingCard.Damaging;
			this.Death = CopyingCard.Death;
			this.Defend = CopyingCard.Defend;
			//this.LinkedCard = (FieldObject)Activator.CreateInstance (CopyingCard.LinkedCard.GetType ());
			this.ImagePath = CopyingCard.ImagePath;
			this.PrintedAp = CopyingCard.PrintedAp;
			this.Leave = CopyingCard.Leave;
			this.Move = CopyingCard.Move;
			this.Name = CopyingCard.Name;
//			this.NonTargetEvent = CopyingCard.NonTargetEvent;
			//this.Opponent = CopyingCard.Opponent;
			this.PrintedCost = CopyingCard.PrintedCost;
			this.PrintedGraveCost = CopyingCard.PrintedGraveCost;
			this.PrintedHp = CopyingCard.PrintedHp;
            this.TargetBattleCry = CopyingCard.TargetBattleCry;
			this.Retrieve = CopyingCard.Retrieve;
			this.Reverse = CopyingCard.Reverse;
			this.Spell = CopyingCard.Spell;
			this.SpellTarget = CopyingCard.SpellTarget;
			this.Summon = CopyingCard.Summon;
			this.Text = CopyingCard.Text;
			this.TurnEnd = CopyingCard.TurnEnd;
			this.TurnStart = CopyingCard.TurnStart;
			this.Recyclable = CopyingCard.Recyclable;
			
			
			return this;
		}
		
	}

	public class Player : FieldObject
	{
		public int MaxResource;
		public int Resource;
		
		public List<Card> Hand;
		public List<FieldObject> Field;
        public List<Card> Grave;
		public bool Turn;
        public bool IsPlayer1inNetwork = false;

        public int MaxResGain = 4;

        public Player()
            : base(0, 0, CardTypes.player, "empty", "플레이어", 0, 45)
        {
            MaxResource = 20;
            Resource = 0;
			
            Hand = new List<Card>();
            Field = new List<FieldObject>();
			Grave = new List<Card>();
            Turn = false;
            Controller = this;
        }

        public int getNetworkID() { return IsPlayer1inNetwork ? 1 : 2; }
	}

    public static class VChecks
    {
        public static bool IsMonsterOrMystery(FieldObject target)
        {
            return (target.CardType == CardTypes.monster || target.CardType == CardTypes.mystery);
        }

        public static bool IsMonsterNotMyetery(FieldObject target)
        {
            return target.CardType == CardTypes.monster;
        }

        public static bool isPlayer(FieldObject target)
        {
            return target.CardType == CardTypes.player;
        }

        public static bool IsMine(FieldObject thisObject, FieldObject target)
        {
            return (target.Controller == thisObject.Controller);
        }

        public static bool IsMine(Card thisCard, FieldObject target)
        {
            return (target.Controller == thisCard.Controller);
        }


    }

    public enum BuffStyle
    {
        Positive,
        Negative,
        Mixed
    }

    /// <summary>
    /// 지울 수 있는 버프에 관련된 클래스. 생성하고 나서 Initialize()를 켜기만 하면 작동하기 시작합니다. 종결시킬 때는 반드시 Terminate()를 부르거나 주체에서 DeleteAllBuff()를 써야 합니다.
    /// </summary>
    public class Buff
    {

        public string Description;
        public string Title;
        public readonly FieldObject BuffTarget;
        public delegate void TerminateHandler(Buff thisBuff);
        public delegate void InitiateHandler(Buff thisBuff);
        BuffStyle BuffStyle;
        TerminateHandler terminate;
        InitiateHandler initialize;
        bool terminated = false;
        bool expireAtEOT;

        public Buff(FieldObject buffTarget, InitiateHandler initialize, TerminateHandler terminate, bool expireAtEOT = false, BuffStyle buffStyle = BuffStyle.Mixed, string description = "", string title = "")
        {
            BuffTarget = buffTarget;
            Description = description;
            Title = title;
            this.initialize = initialize;
            this.terminate = terminate;
            this.expireAtEOT = expireAtEOT;
            this.BuffStyle = buffStyle;
        }

        ~Buff()
        {
            if (!terminated)
                Debug.Log("GC destroyed a buff that's not terminated! The buff's target was " + BuffTarget.Name + " and the buff was " + Title);
        }

        public void Initialize()
        {
            terminated = false;
            if (!BuffTarget.BuffList.Contains(this))
                BuffTarget.BuffList.Add(this);
            initialize(this);

            if (expireAtEOT)
            {
                BuffTarget.TurnEnd += ExpireEOT;
            }
        }

        public void Terminate()
        {
            if (terminated)
                Debug.LogError("ERROR: Tried to terminate an already terminated buff!");
            terminated = true;
            BuffTarget.BuffList.Remove(this);
            terminate(this);
        }

        void ExpireEOT(FieldObject thisObject)
        {
            BuffTarget.TurnEnd -= ExpireEOT;
            Terminate();
        }

		public void BuffCopy( FieldObject Target )
		{
            Buff CopiedBuff = new Buff(Target, this.initialize, this.terminate, this.expireAtEOT, this.BuffStyle, this.Description, this.Title);
            Target.BuffList.Add(CopiedBuff);
		}
    }

    public class StatBuff : Buff
    {
        public StatBuff(FieldObject buffTarget, int deltaAp, int deltaHp, bool expireAtEOT = false, BuffStyle buffStyle = BuffStyle.Mixed, string description = "", string title = "")
            : base(buffTarget,
            (thisBuff) =>
            {
                thisBuff.BuffTarget.CurrentAp += deltaAp;
                thisBuff.BuffTarget.CurrentMaxHp += deltaHp;
                thisBuff.BuffTarget.CurrentHp += deltaHp;
                if (thisBuff.BuffTarget.CurrentHp <= 0)
                    GameAction.DestroyFieldObject(thisBuff.BuffTarget);
            },
            (thisBuff) =>
            {
                thisBuff.BuffTarget.CurrentAp -= deltaAp;
                thisBuff.BuffTarget.CurrentMaxHp -= deltaHp;
                thisBuff.BuffTarget.CurrentHp = Math.Min(thisBuff.BuffTarget.CurrentHp, thisBuff.BuffTarget.CurrentMaxHp);
            },
            expireAtEOT, buffStyle, description, title)
        {
        }

    }
}



