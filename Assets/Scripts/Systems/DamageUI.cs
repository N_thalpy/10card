﻿using UnityEngine;
using System.Collections;

public class DamageUI : MonoBehaviour {

	float TimePassed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 DamagePos = this.transform.position;
		TimePassed += Time.deltaTime;
		DamagePos.Set (DamagePos.x, DamagePos.y + Time.deltaTime * 0.25f, DamagePos.z);
		if (TimePassed > 0.75f) 
		{
			Object.Destroy( this.gameObject );
		}
		this.transform.position = DamagePos;
	}
}
