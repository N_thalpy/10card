﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Base;

public class DataBox : MonoBehaviour {

	public List<Card> Deck;

	void Start()
	{
		Deck = new List<Card>();
		DontDestroyOnLoad (this);
		Base.Data.DataInit ();
		Application.LoadLevel ("MainTitle");
	}
}
