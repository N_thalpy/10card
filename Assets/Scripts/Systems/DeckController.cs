﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Base;

public class DeckController : MonoBehaviour {

	List<Card> TotalList;
	List<Card> DeckList;

	public GameObject PickDeckShower;

	int Reader;
	int MaxReader
    {
        get
        {
            if (Status == CardListStatus.Gold)
                return GoldDictionary.Count / CardListShow.Length;
            else
                return NormalDictionary.Count / CardListShow.Length;
        }
    }

    enum CardListStatus
    {
        Normal,
        Gold
    }
    private CardListStatus _Status;
    private CardListStatus Status
    {
        get
        {
            return _Status;
        }
        set
        {
            if (value == _Status)
                return;

            _Status = value;

            Reader = 0;
            CardListShowUpdater();
        }
    }

	public enum SortType
	{
		Name,
		Cost,
	}

	public GameObject[] CardListShow;
	public GameObject InfoObject;
	public GameObject[] DeckListShow;
	public GameObject Cover;
	List<Card> NormalDictionary;
    List<Card> GoldDictionary;
	List<List<Card>> PresetList;

	Card thisCard; 
	bool inDeck;
	bool PresetDeck = false;
	GameObject DataBox;



	// Use this for initialization
	void Start () 
	{
		TotalList = new List<Card> ();
		DeckList = new List<Card> ();

        NormalDictionary = Base.Data.NormalDictionary;
        GoldDictionary = Base.Data.GoldDictionary;

		Reader = 0;
		inDeck = false;
		DataBox = GameObject.Find ("DataBox");
		ChangeCardSort(SortType.Cost);
		Debug.Log (NormalDictionary.Count);
        Debug.Log (GoldDictionary.Count);
		DeckInit ();

        Status = CardListStatus.Gold;
		PresetMaker ();
	}

	public void ChangeCardSort(SortType sort)
	{
		switch (sort)
		{
		case SortType.Cost:
			NormalDictionary.Sort((Card lhs, Card rhs) =>
			{
				if (lhs.CurrentCost == rhs.CurrentCost)
					return lhs.Name.CompareTo(rhs.Name);
				else
					return lhs.CurrentCost.CompareTo(rhs.CurrentCost);
			});
			GoldDictionary.Sort((Card lhs, Card rhs) =>
			{
				if (lhs.CurrentCost == rhs.CurrentCost)
					return lhs.Name.CompareTo(rhs.Name);
				else
					return lhs.CurrentCost.CompareTo(rhs.CurrentCost);
			});
			Reader = 0;
			CardListShowUpdater();
			break;

		case SortType.Name:
			NormalDictionary.Sort((Card lhs, Card rhs) => { return lhs.Name.CompareTo(rhs.Name); });
			GoldDictionary.Sort((Card lhs, Card rhs) => { return lhs.Name.CompareTo(rhs.Name); });
			Reader = 0;
			CardListShowUpdater();
			break;
		}
	}

	public void SortButton1()
	{
		ChangeCardSort (SortType.Cost);
	}

	public void SortButton2()
	{
		ChangeCardSort (SortType.Name);
	}

	void CardListShowUpdater()
	{
		int Adder = Reader * CardListShow.Length;

        if (Status == CardListStatus.Normal)
        {
            for (int i = 0; i < CardListShow.Length; i++)
            {
                Card Empty = new EmptyCard();
                CardListShow[i].GetComponent<UIChanger>().PlayerHand = Empty;
            }
            for (int i = 0; i < CardListShow.Length; i++)
            {
                if (i + Adder >= NormalDictionary.Count)
                {
                    break;
                }
                CardListShow[i].GetComponent<UIChanger>().PlayerHand = NormalDictionary[i + Adder];
            }
        }
        else if (Status == CardListStatus.Gold)
        {
			if ( PresetDeck == false )
			{
            	for (int i = 0; i < CardListShow.Length; i++)
            	{
	                Card Empty = new EmptyCard();
                	CardListShow[i].GetComponent<UIChanger>().PlayerHand = Empty;
            	}
            	for (int i = 0; i < CardListShow.Length; i++)
            	{
	                if (i + Adder >= GoldDictionary.Count)
    	            {
                    	break;
                	}
                	CardListShow[i].GetComponent<UIChanger>().PlayerHand = GoldDictionary[i + Adder];
				}
            }
			else
			{
				for (int i = 0; i < CardListShow.Length; i++)
				{
					Card Empty = new EmptyCard();
					CardListShow[i].GetComponent<UIChanger>().PlayerHand = Empty;
				}
				for ( int i = 0; i < PresetList.Count; i++ )
				{
					CardListShow[i].GetComponent<UIChanger>().PlayerHand = PresetList[i][0];
				}
			}
        }
	}

	void DeckInit()
	{
		DeckListShowUpdater ();
	}

	void DeckListShowUpdater()
	{
		for (int i = 0; i < DeckListShow.Length; i ++) 
		{
			Card Empty = new EmptyCard();
			DeckListShow[i].GetComponent<UIChanger>().PlayerHand = Empty;
		}
		for (int i = 0; i < DeckList.Count; i ++) 
		{
			DeckListShow[i].GetComponent<UIChanger>().PlayerHand = DeckList[i];
		}
	}

	public void PresetDeckButton()
	{
		if (PresetDeck == false) {
			PickDeckShower.SetActive( true );
			PresetDeck = true;
			Status = CardListStatus.Gold;
			CardListShowUpdater ();
		} 
		else 
		{
			PickDeckShower.SetActive( false );
			PresetDeck = false;
			Status = CardListStatus.Gold;
			for ( int i = 0; i < DeckList.Count; i++ )
			{
				if ( DeckList[i].Recyclable == false )
				{
					Status = CardListStatus.Normal;
				}
			}
			CardListShowUpdater ();
		}
	}

	void DeckAdd( Card AddingCard )
	{
		if (PresetDeck == true) 
		{
			return;
		}
		if (AddingCard == null) 
		{
			return;
		}
        bool SameCardChecker = false;
        for (int i = 0; i < DeckList.Count; i++)
        {
            if (DeckList[i] == AddingCard)
            {
                SameCardChecker = true;
            }
        }
        if (SameCardChecker != true && DeckList.Count < DeckListShow.Length) 
		{
			DeckList.Add (AddingCard);
			DeckList = new List<Card> (DeckList.OrderBy(x => x.Recyclable == true));

			DeckListShowUpdater ();
			inDeck = true;

			if (AddingCard.Recyclable == false)
				Status = CardListStatus.Normal;
		}
	}

	void DeckRemove ( Card DeletingCard )
	{
		DeckList.Remove (DeletingCard);
		DeckListShowUpdater ();
		inDeck = false;

        if (DeletingCard.Recyclable == false)
            Status = CardListStatus.Gold;
	}

	public void DeckEditButtonClicked()
	{
		if (inDeck == true) 
        {
			DeckRemove( InfoObject.GetComponent<InfoObject>().PlayerHand );
		} else 
		{
			DeckAdd( InfoObject.GetComponent<InfoObject>().PlayerHand );
		}
	}

	public void PresetDeckSearch( Card MainCard )
	{
		bool find = false;
		Debug.Log (PresetList.Count);
		for ( int i = 0; i < PresetList.Count; i++ )
		{
			if ( PresetList[i][0].Name == MainCard.Name )
			{
				DeckChanger( PresetList[i] );
				find = true;
			}
		}
		if (find == false) 
		{
			while ( DeckList.Count > 0 )
			{
				DeckList.RemoveAt(0);
			}
		}
	}

	public void DeckChanger( List<Card> Changer )
	{
		while (DeckList.Count > 0) 
		{
			DeckList.RemoveAt(0);
		}

		for (int i = 0; i < Changer.Count; i++) 
		{
			DeckList.Add ( Changer[i] );
		}
		DeckListShowUpdater ();
	}

	public void ButtonPush0()
	{
		InfoObject.GetComponent<InfoObject>().PlayerHand = CardListShow[0].GetComponent<UIChanger> ().PlayerHand;
		inDeck = false;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[0].GetComponent<UIChanger> ().PlayerHand );
		}
	}
	public void ButtonPush1()
	{
		InfoObject.GetComponent<InfoObject>().PlayerHand = CardListShow[1].GetComponent<UIChanger> ().PlayerHand;
		inDeck = false;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[1].GetComponent<UIChanger> ().PlayerHand );
		}
	}
	public void ButtonPush2()
	{
		InfoObject.GetComponent<InfoObject>().PlayerHand = CardListShow[2].GetComponent<UIChanger> ().PlayerHand;
		inDeck = false;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[2].GetComponent<UIChanger> ().PlayerHand );
		}
	}
	public void ButtonPush3()
	{
		InfoObject.GetComponent<InfoObject>().PlayerHand = CardListShow[3].GetComponent<UIChanger> ().PlayerHand;
		inDeck = false;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[3].GetComponent<UIChanger> ().PlayerHand );
		}
	}
	public void ButtonPush4()
	{
		InfoObject.GetComponent<InfoObject>().PlayerHand = CardListShow[4].GetComponent<UIChanger> ().PlayerHand;
		inDeck = false;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[4].GetComponent<UIChanger> ().PlayerHand );
		}
	}
	public void ButtonPush5()
	{
		InfoObject.GetComponent<InfoObject>().PlayerHand = CardListShow[5].GetComponent<UIChanger> ().PlayerHand;
		inDeck = false;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[5].GetComponent<UIChanger> ().PlayerHand );
		}
	}
	public void ButtonPush6()
	{
		InfoObject.GetComponent<InfoObject>().PlayerHand = CardListShow[6].GetComponent<UIChanger> ().PlayerHand;
		inDeck = false;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[6].GetComponent<UIChanger> ().PlayerHand );
		}
	}
	public void ButtonPush7()
	{
		InfoObject.GetComponent<InfoObject>().PlayerHand = CardListShow[7].GetComponent<UIChanger> ().PlayerHand;
		inDeck = false;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[7].GetComponent<UIChanger> ().PlayerHand );
		}
	}
	public void ButtonPush8()
	{
		InfoObject.GetComponent<InfoObject>().PlayerHand = CardListShow[8].GetComponent<UIChanger> ().PlayerHand;
		inDeck = false;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[8].GetComponent<UIChanger> ().PlayerHand );
		}
	}
	public void ButtonPush9()
	{
		InfoObject.GetComponent<InfoObject>().PlayerHand = CardListShow[9].GetComponent<UIChanger> ().PlayerHand;
		inDeck = false;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[9].GetComponent<UIChanger> ().PlayerHand );
		}
	}
	public void ButtonPush10()
	{
		InfoObject.GetComponent<InfoObject>().PlayerHand = CardListShow[10].GetComponent<UIChanger> ().PlayerHand;
		inDeck = false;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[10].GetComponent<UIChanger> ().PlayerHand );
		}
	}
	public void ButtonPush11()
	{
		InfoObject.GetComponent<InfoObject>().PlayerHand = CardListShow[11].GetComponent<UIChanger> ().PlayerHand;
		inDeck = false;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[11].GetComponent<UIChanger> ().PlayerHand );
		}
	}
	public void ButtonPush12()
	{
		InfoObject.GetComponent<InfoObject>().PlayerHand = CardListShow[12].GetComponent<UIChanger> ().PlayerHand;
		inDeck = false;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[12].GetComponent<UIChanger> ().PlayerHand );
		}
	}
	public void ButtonPush13()
	{
		InfoObject.GetComponent<InfoObject>().PlayerHand = CardListShow[13].GetComponent<UIChanger> ().PlayerHand;
		inDeck = false;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[13].GetComponent<UIChanger> ().PlayerHand );
		}
	}
	public void ButtonPush14()
	{
		InfoObject.GetComponent<InfoObject>().PlayerHand = CardListShow[14].GetComponent<UIChanger> ().PlayerHand;
		inDeck = false;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[14].GetComponent<UIChanger> ().PlayerHand );
		}
	}

	public void DeckButtonPush0()
	{
		InfoObject.GetComponent<InfoObject> ().PlayerHand = DeckListShow [0].GetComponent<UIChanger> ().PlayerHand;
		inDeck = true;
		Cover.SetActive (false);
		if (PresetDeck == true) 
		{
			PresetDeckSearch( CardListShow[0].GetComponent<UIChanger> ().PlayerHand );
		}
	}

	public void DeckButtonPush1()
	{
		InfoObject.GetComponent<InfoObject> ().PlayerHand = DeckListShow [1].GetComponent<UIChanger> ().PlayerHand;
		inDeck = true;
		Cover.SetActive (false);
	}

	public void DeckButtonPush2()
	{
		InfoObject.GetComponent<InfoObject> ().PlayerHand = DeckListShow [2].GetComponent<UIChanger> ().PlayerHand;
		inDeck = true;
		Cover.SetActive (false);
	}

	public void DeckButtonPush3()
	{
		InfoObject.GetComponent<InfoObject> ().PlayerHand = DeckListShow [3].GetComponent<UIChanger> ().PlayerHand;
		inDeck = true;
		Cover.SetActive (false);
	}

	public void DeckButtonPush4()
	{
		InfoObject.GetComponent<InfoObject> ().PlayerHand = DeckListShow [4].GetComponent<UIChanger> ().PlayerHand;
		inDeck = true;
		Cover.SetActive (false);
	}

	public void DeckButtonPush5()
	{
		InfoObject.GetComponent<InfoObject> ().PlayerHand = DeckListShow [5].GetComponent<UIChanger> ().PlayerHand;
		inDeck = true;
		Cover.SetActive (false);
	}

	public void DeckButtonPush6()
	{
		InfoObject.GetComponent<InfoObject> ().PlayerHand = DeckListShow [6].GetComponent<UIChanger> ().PlayerHand;
		inDeck = true;
		Cover.SetActive (false);
	}

	public void DeckButtonPush7()
	{
		InfoObject.GetComponent<InfoObject> ().PlayerHand = DeckListShow [7].GetComponent<UIChanger> ().PlayerHand;
		inDeck = true;
		Cover.SetActive (false);
	}

	public void DeckButtonPush8()
	{
		InfoObject.GetComponent<InfoObject> ().PlayerHand = DeckListShow [8].GetComponent<UIChanger> ().PlayerHand;
		inDeck = true;
		Cover.SetActive (false);
	}

	public void DeckButtonPush9()
	{
		InfoObject.GetComponent<InfoObject> ().PlayerHand = DeckListShow [9].GetComponent<UIChanger> ().PlayerHand;
		inDeck = true;
		Cover.SetActive (false);
	}

	public void DeckPageUp ()
	{
		if (Reader < MaxReader) 
		{
			Reader++;
			CardListShowUpdater();
		}
	}

	public void DeckPageDown ()
	{
		if (Reader > 0 ) 
		{
			Reader--;
			CardListShowUpdater();
		}
	}

	public void Save()
	{
		DataBox = GameObject.Find ("DataBox");
		DataBox.GetComponent<DataBox> ().Deck = DeckList;
		if (DeckList.Count < DeckListShow.Length) {
			Debug.Log ("cant save");
		} else 
		{
			Debug.Log ("Deck Saved");
		}
	}

	public void GameStart()
	{
		if (DataBox.GetComponent<DataBox> ().Deck.Count < DeckListShow.Length) 
		{
			Debug.Log ("Cant Start");
		} 
		else 
		{
			Debug.Log ("Game Start");
			Application.LoadLevel ("MainGame");
		}
	}

	public void PresetMaker()
	{
		PresetList = new List<List<Card>> ();
		List<Card> DoomList = new List<Card> ()
		{			 
			new DoomLordCard (),
			new CorpseHarvesterCard(),
			new FleshGolemCard(),
			new BlindingFlareCard(),
			new MoltenGiantCard(),
			new PitLordCard(),
			new RitualDaggerCard(),
			new SpellbreakerCard(),
			new ThrowTwoHandedWeaponCard(),
			new LivingBloodCard()
		};
		PresetList.Add (DoomList);

		List<Card> MagicianList = new List<Card> ()
		{		
			new MagicianCard(),
			new CorpseFeedingDoppelgangerCard(),
			new	PortalTrapCard(),
			new	ForestShadeCard(),
			new	SpellbreakerCard(),
			new	WebTrapCard(),
			new	IntotheMistCard (),
			new	LethalTrapCard(),
			new	PinkDolphinCard(),
			new AssassinCard()
		};
		PresetList.Add (MagicianList);

		
		List<Card> MagicEatEvil = new List<Card> ()
		{
			new MagiceatevilCard(),
			new BlindingFlareCard(),
			new ManaDrakeCard(),
			new WizardsDuelCard(),
			new DustWhirlwindCard(),
			new CorpseHarvesterCard(),
			new TreasuryCard(),
			new PitLordCard(),
			new ExpansionCard(),
			new ChaosCard()
		};
		PresetList.Add (MagicEatEvil);

		List<Card> FireLord = new List<Card> ()
		{
			new TartarostheFlameLordCard(), 
			new FlourishingManaCard(), 
			new PitLordCard(), 
			new SpellbreakerCard(), 
			new CorpseMarionetteCard(), 
			new MoltenGiantCard(), 
			new DevotedGuardianCard(), 
			new TearingScarsCard(), 
			new ThrowTwoHandedWeaponCard(),
			new CorpseHarvesterCard(),
		};
		PresetList.Add (FireLord);

		List<Card> DoomDevil = new List<Card> ()
		{
			new DoomdevilCard(),
			new BlindSeerCard(), 
			new ManaDrakeCard(), 
			new ManaDevourerCard(), 
			new ChaliceofOblivionCard(), 
			new DustWhirlwindCard(), 
			new UnstableGolemCard(), 
			new ElfArcherCard(), 
			new HerosTrialCard(),
			new HousefairyCard()
		};
		PresetList.Add (DoomDevil);

		List<Card> GreatWizard = new List<Card> ()
		{
			new GreatwizardCard(),
			new FlourishingManaCard(),
			new BlindingFlareCard(),
			new ChaliceofOblivionCard(),
			new WizardsDuelCard(),
			new SpellbreakerCard(),
			new PitLordCard(),
			new FleshGolemCard(),
			new ThrowTwoHandedWeaponCard(),
			new AssassinCard()
		};
		PresetList.Add (GreatWizard);

		List<Card> Summonbreaker = new List<Card> () 
		{ 
			new SummonbreakerCard(), 
			new MasterImposterCard(), 
			new AsceticCard(), 
			new MeteorCard(), 
			new LootHoarderCard(), 
			new SpellReaverCard(), 
			new BelieveroftheTruthCard(), 
			new AngelsHornCard(), 
			new ThreateningCard(), 
			new BlackKnightCard() 
		}; 
		PresetList.Add (Summonbreaker); 

		List<Card> Soulfire = new List<Card> () 
		{ 
			new SoulfireCard(), 
			new BalanceCard(), 
			new FlourishingManaCard(), 
			new ColdsnapCard(), 
			new ForestShadeCard(), 
			new LordoftheWoodsCard(), 
			new AssassinCard(), 
			new TerrorintheDarkCard(), 
			new SpellbreakerCard(), 
			new ExpansionCard() 
		}; 
		PresetList.Add (Soulfire); 
		
		List<Card> Goz = new List<Card> () 
		{ 
			new GozCard(), 
			new AvatarofHatredCard(), 
			new GuardianKnightCard(), 
			new ChaliceofOblivionCard(), 
			new CorpseFeedingDoppelgangerCard(), 
			new ThrowTwoHandedWeaponCard(), 
			new ThreateningCard(), 
			new BlackKnightCard(), 
			new SpellbreakerCard(), 
			new HeavyInfantryCard() 
		}; 
		PresetList.Add (Goz);

	}

}
