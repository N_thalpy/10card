﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Base;

public class EffectController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		EffectList.Init ();
		Effect.EFC = this.GetComponent<EffectController> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		float nowTime = Time.time;
		for (int i = 0; i < EffectList.EFL.Count; i ++) 
		{
			if ( EffectList.EFL[i].DelayTime < nowTime && EffectList.EFL[i].isActvate == false )
			{
				Debug.Log( "EffectStart!");
				EffectList.EFL[i].Activation();
			}
			if ( EffectList.EFL[i].DelayTime + EffectList.EFL[i].RemainTime < nowTime )
			{
				EffectList.EFL.Remove ( EffectList.EFL[i] );
			}
		}
	}

	public float EffectLastTime()
	{
		float maxTime = Time.time;
		for ( int i = 0; i < EffectList.EFL.Count; i ++ )
		{
			if ( EffectList.EFL[i].DelayTime + EffectList.EFL[i].RemainTime > maxTime )
			{
				maxTime = EffectList.EFL[i].DelayTime + EffectList.EFL[i].RemainTime;
			}
		}
		return maxTime;
	}
}
