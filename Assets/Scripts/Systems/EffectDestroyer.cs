﻿using UnityEngine;
using System.Collections;

public class EffectDestroyer : MonoBehaviour {
	public Vector3 StartPosition;
	public Vector3 EndPosition;
	float CreatedTime = 0;
	public float RemainTime;
	float DeadTime = 0;

	// Use this for initialization
	void Start () {
		CreatedTime = Time.time;
		DeadTime = CreatedTime + RemainTime;
		Debug.Log (CreatedTime);
		Debug.Log (DeadTime);
	}
	
	// Update is called once per frame
	void Update () 
	{
		this.transform.position = Vector3.Lerp (StartPosition, EndPosition, (Time.time - CreatedTime) / RemainTime);
		if (Time.time > DeadTime ) 
		{
			Destroy ( this.gameObject );
		}
	}
}
