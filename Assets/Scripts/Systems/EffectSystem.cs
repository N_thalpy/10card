﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base {

	public static class EffectList
	{
		public static List<Effect> EFL;

		public static void Init()
		{
			EFL = new List<Effect>();
		}
	}

	public class Effect{

		public static EffectController EFC;

		public float RemainTime;
		public float DelayTime;
		Vector3 Start;
		Vector3 End;
		string EffectName;
		public bool isActvate;
		public int DamageAmount = 0;
		//enum EffectTypes { NonProjectile, Projectile };
		//EffectTypes EffectType;

		public Effect ( float RemainTime, float DelayTime, FieldObject Start, FieldObject End, string EffectName )
		{
			this.RemainTime = RemainTime;
			this.DelayTime = DelayTime;
			this.Start = FieldObjectPosition( Start );
			this.End = FieldObjectPosition( End );
			this.EffectName = EffectName;
			isActvate = false;
		}

		public static Effect AddEffect( Effect ThisEffect )
		{
			EffectList.EFL.Add (ThisEffect);
			return ThisEffect;
		}

		public GameObject Activation ()
		{
			GameObject EffectObject = Resources.Load ("Effect/" + EffectName) as GameObject;
			EffectObject = (GameObject)GameObject.Instantiate (EffectObject);

			if (EffectName == "MessageBox") 
			{
				EffectObject.GetComponentInChildren<UILabel>().text = "-"+((int)DamageAmount).ToString();
				EffectObject.GetComponent<EffectDestroyer> ().RemainTime = 2.0f;
			}
			else 
			{
				EffectObject.GetComponent<EffectDestroyer> ().RemainTime = RemainTime;
			}

			EffectObject.GetComponent<EffectDestroyer> ().StartPosition = Start;
			EffectObject.GetComponent<EffectDestroyer> ().EndPosition = End;
			EffectObject.transform.position = Start;
			isActvate = true;
			return EffectObject;
		}

		Vector3 FieldObjectPosition( FieldObject ThisObject)
		{
			Vector3 ReturnVector;
			ReturnVector = Base.GameAction.MainSystem.FieldToUI (ThisObject).transform.position;
			return ReturnVector;
		}
	}


}
