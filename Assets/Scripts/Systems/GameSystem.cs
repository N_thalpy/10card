﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Base;
using Newtonsoft.Json;

public class GameSystem : MonoBehaviour
{

    // Use this for initialization

    static int HandNum = 8;
    static int FieldNum = 5;

	public GameObject Shadow;
	public GameObject Win;
	public GameObject Lose;
	public GameObject WaitingLabel;

    public Player MePlayer;
    public Player OpponentPlayer;

    public GameObject StartButton;
    public GameObject ConnectButton;
    public GameObject TurnEndButton;

    public GameObject[] MyHand;
    public GameObject[] OpponentHand;
	public GameObject PlayerMask;

    public GameObject[] MyField;
    public GameObject[] OpponentField;

	public GameObject[] LoggingIcon;

	public GameObject P1Face;
	public GameObject P2Face;

	public GameObject AudioObject;

    public UILabel MyHP;
    public UILabel MyMana;

    public UILabel OpponentHP;
    public UILabel OpponentMana;


    public GameObject UIParent;

    public bool InfoHelper = false;
    public bool gameStart = false;
    public bool FirstDraw = false;
    public bool Clicked;
    public bool isConnected = false;
    public bool Reverse = false;
	public bool isEffect = false;

    public NetworkConnect ClientConnect;

    public GameObject InfoViewer;
    public FieldObject SelectedObject;
    public FieldObject SelectedObjectBefore;
    public Card SelectedCard;
    public Card SelectedCardBefore;
    public enum PhaseType { OpponentField, ControllerField, AllField, Hand, AllObject };

    List<Card> NormalDictionary;
    List<Card> GoldDictionary;

    public int NetworkOrder;

    public Dictionary<int, NetworkMessage> MessageQueue;
    public Queue<int> RandomQueue;
    //public Dictionary<int, Action<int>> RandomActionQueue;


	IEnumerator LogAdder()
	{
		while (true) 
		{
			while (LogSystem.LogStack.Count > 5) 
			{
				LoggingAdd( LogSystem.LogStack[5] );
				LogSystem.LogStack.RemoveAt(0);
				yield return new WaitForSeconds (0.8f);
			}
			yield return null;
		}
	}

	public void UIChange()
	{
		for (int i = 0; i < MyField.Length; i++) 
		{
			MyField[i].GetComponent<UIChanger>().PlayerField = MePlayer.Field[i];
		}
		
		for (int i = 0; i < OpponentField.Length; i++) 
		{
			OpponentField[i].GetComponent<UIChanger>().PlayerField = OpponentPlayer.Field[i];
		}
	}

	void LoggingAdd( LogData AddingData )
	{
		float TimeKeeper = Time.time;
		float TimeGap = 0.4f;
		for (int i = 1; i < 5; i++) 
		{
			LoggingIcon[i].GetComponent<LogSprite>().StartPosition = LoggingIcon[i].transform.localPosition;
			LoggingIcon[i].GetComponent<LogSprite>().EndPosition = LoggingIcon[i-1].transform.localPosition;
			LoggingIcon[i].GetComponent<LogSprite>().Move = true;
			LoggingIcon[i].GetComponent<LogSprite>().StartTime = TimeKeeper;
			LoggingIcon[i].GetComponent<LogSprite>().EndTime = TimeKeeper + TimeGap;
		}
		LoggingIcon [0].transform.position = LoggingIcon [5].transform.position;

		LoggingIcon [5].GetComponent<LogSprite> ().ThisCard = AddingData.Card;
		LoggingIcon [5].GetComponent<LogSprite> ().ThisField = AddingData.FieldObject;
		LoggingIcon [5].GetComponent<LogSprite> ().StartPosition = LoggingIcon [5].transform.localPosition;
		LoggingIcon [5].GetComponent<LogSprite> ().EndPosition = LoggingIcon [4].transform.localPosition;
		LoggingIcon[5].GetComponent<LogSprite>().Move = true;
		LoggingIcon[5].GetComponent<LogSprite>().StartTime = TimeKeeper + TimeGap;
		LoggingIcon[5].GetComponent<LogSprite>().EndTime = TimeKeeper + TimeGap*2;

		GameObject TempIcon = LoggingIcon [0];
		for (int i = 0; i < 5; i++) 
		{
			LoggingIcon[i] = LoggingIcon[i+1];
		}
		LoggingIcon [5] = TempIcon;
	}

	public void LogButtonClick0 ()
	{
		InfoReader (LoggingIcon [0].GetComponent<LogSprite> ().ThisCard, LoggingIcon [0].GetComponent<LogSprite> ().ThisField);
	}

	public void LogButtonClick1 ()
	{
		InfoReader (LoggingIcon [1].GetComponent<LogSprite> ().ThisCard, LoggingIcon [1].GetComponent<LogSprite> ().ThisField);
	}

	public void LogButtonClick2 ()
	{
		InfoReader (LoggingIcon [2].GetComponent<LogSprite> ().ThisCard, LoggingIcon [2].GetComponent<LogSprite> ().ThisField);
	}

	public void LogButtonClick3 ()
	{
		InfoReader (LoggingIcon [3].GetComponent<LogSprite> ().ThisCard, LoggingIcon [3].GetComponent<LogSprite> ().ThisField);
	}

	public void LogButtonClick4 ()
	{
		InfoReader (LoggingIcon [4].GetComponent<LogSprite> ().ThisCard, LoggingIcon [4].GetComponent<LogSprite> ().ThisField);
	}

	public void LogButtonClick5 ()
	{
		InfoReader (LoggingIcon [5].GetComponent<LogSprite> ().ThisCard, LoggingIcon [5].GetComponent<LogSprite> ().ThisField);
	}


    void Start()
    {
		//LogSystem.GetLog (5);
        MePlayer = new Player();
        OpponentPlayer = new Player();
        MePlayer.Opponent = OpponentPlayer;
        OpponentPlayer.Opponent = MePlayer;
        TurnEndButton.SetActive(false);

        Time.timeScale = 0.0f;
        Base.GameAction.MainSystem = this.GetComponent<GameSystem>();

        MePlayer.Field = new List<FieldObject>();
        OpponentPlayer.Field = new List<FieldObject>();
        FieldObject empty;
        for (int i = 0; i < 5; i++)
        {
            empty = new EmptyField();
            empty.Controller = MePlayer;
            empty.Opponent = OpponentPlayer;
            MePlayer.Field.Add(empty);
			MyField[i].GetComponent<UIChanger>().PlayerField = empty;
            empty = new EmptyField();
            empty.Controller = OpponentPlayer;
            empty.Opponent = MePlayer;
            OpponentPlayer.Field.Add(empty);
			OpponentField[i].GetComponent<UIChanger>().PlayerField = empty;
        }
		GameObject DataBox = GameObject.Find ("DataBox");
		List<Card> DeckFromData = DataBox.GetComponent<DataBox>().Deck;
		for (int i = 0; i < 10; i++) 
		{
			DeckFromData[i].Controller = MePlayer;
			DeckFromData[i].Opponent = OpponentPlayer;
			DeckFromData[i].LinkedFieldObject.Controller = MePlayer;
			DeckFromData[i].LinkedFieldObject.Opponent = OpponentPlayer;
			MyHand[i].GetComponent<UIChanger>().PlayerHand = DeckFromData[i];
		}
		StartCoroutine ("LogAdder");
		NormalDictionary = Base.Data.NormalDictionary;
        GoldDictionary = Base.Data.GoldDictionary;
    }


    void CardPick()
    {
		GameObject DataBox = GameObject.Find ("DataBox");
		List<Card> DeckFromData = DataBox.GetComponent<DataBox>().Deck;
        for (int i = 0; i < DeckFromData.Count; i++)
        {
            Card PickCard;
            
			PickCard = GetCardFromName ( DeckFromData[i].Name, false );
            PickCard.Controller = MePlayer;
            PickCard.Opponent = OpponentPlayer;
            PickCard.LinkedFieldObject.Controller = MePlayer;
            PickCard.LinkedFieldObject.Opponent = OpponentPlayer;
            MePlayer.Hand.Add(PickCard);

            if (!isConnected)
            {
				PickCard.Controller = MePlayer ;
				PickCard.Opponent =OpponentPlayer ;
                OpponentPlayer.Hand.Add(PickCard);
            }
        }
		if (isConnected) 
		{
			int RandomFace;
			RandomFace = GameAction.RandomInt (0, 6);

			P1Face.GetComponent<UI2DSprite> ().sprite2D = PlayerMask.GetComponent<PlayerData> ().PlayerDataDD [RandomFace + MePlayer.getNetworkID()];
			P2Face.GetComponent<UI2DSprite> ().sprite2D = PlayerMask.GetComponent<PlayerData> ().PlayerDataDD [RandomFace + OpponentPlayer.getNetworkID()];

			P1Face.SetActive (true);
			P2Face.SetActive (true);
		} 
		else 
		{
			P1Face.SetActive(true);
			P2Face.SetActive(true);
		}
		AudioObject.SetActive (true);
		AudioObject.GetComponent<AudioSource> ().Play ();
        /*
        Card testcard = new TESTMonsterCard();
        CardList[7] = testcard;
          */
    }

    void FirstChangeCall()
    {

        //Changer = new CardChange ();

        List<Card> CardList1 = MePlayer.Hand;
        List<Card> CardList2 = OpponentPlayer.Hand;

        for (int i = 0; i < CardList1.Count; i++)
        {
            MyHand[i].GetComponent<UIChanger>().PlayerHand = CardList1[i];
            //OpponentHand[i].GetComponent<UIChanger>().PlayerHand = CardList2[i];
        }

    }


    void StartGame()
    {
        Time.timeScale = 1.0f;
        CardPick();

        FirstChangeCall();
        if (isConnected)
        {
            GetPlayerFromNetwork(1).Turn = true;
            GetPlayerFromNetwork(2).MaxResGain += 2;
            if (GetPlayerFromNetwork(1) == MePlayer)
                TurnEndButton.SetActive(true);
        }
        else
        {
            MePlayer.Turn = true;
            TurnEndButton.SetActive(true);
            OpponentPlayer.MaxResGain += 2;
        }


        TurnStart();

        Trigger.BackToMain();
        gameStart = true;
    }



    // Update is called once per frame

    void TurnStart()
    {
        if (MePlayer.Turn)
        {
            MePlayer.Resource += Mathf.Min(MePlayer.MaxResGain, MePlayer.Hand.Count);
            if (MePlayer.MaxResGain < 10) MePlayer.MaxResGain += 2;

			if(MePlayer.Resource > MePlayer.MaxResource)
				MePlayer.Resource = MePlayer.MaxResource;
        }

        if (OpponentPlayer.Turn)
        {
            OpponentPlayer.Resource += Math.Min(OpponentPlayer.MaxResGain, OpponentPlayer.Hand.Count);
            if (OpponentPlayer.MaxResGain < 10) OpponentPlayer.MaxResGain += 2;

			if(OpponentPlayer.Resource > OpponentPlayer.MaxResource)
				OpponentPlayer.Resource = OpponentPlayer.MaxResource;
        }



        Trigger.TurnStartTrigger();



    }

    void TurnEnd()
    {
        Trigger.TurnEndTrigger();
        for (int i = 0; i < 5; i++)
		{
			if (MePlayer.Turn)
			{
				if ( GameAction.IsFieldObjectMonster(MePlayer.Field[i]) )
				{
					MePlayer.Field[i].Activity = true;
				}
			}
			if (OpponentPlayer.Turn)
			{
				if ( GameAction.IsFieldObjectMonster(OpponentPlayer.Field[i]) )
				{
					OpponentPlayer.Field[i].Activity = true;
				}
			}
		}

		for (int i = 0; i < MePlayer.Grave.Count; i++) 
		{
			if(MePlayer.Grave[i].UsingThisTurn)
				MePlayer.Grave[i].UsingThisTurn = false;
		}

		for (int i = 0; i < OpponentPlayer.Grave.Count; i++)
		{
			if(OpponentPlayer.Grave[i].UsingThisTurn)
				OpponentPlayer.Grave[i].UsingThisTurn = false;
		}

		StopCoroutine ("Summon");
		StopCoroutine ("TargetingSpellPhase");
		StopCoroutine ("TargetingBattleCryPhase");
		StopCoroutine ("NonTargetingSpellPhase");
		StopCoroutine ("MyRetrievePhase");
		StopCoroutine ("ReversePhase");
		StopCoroutine ("BattlePhase");
		StopCoroutine ("MainPhase");
		StartCoroutine ("MainPhase");
	}

    public void StartButtonClick()
    {
        ConnectButton.SetActive(false);
        StartButton.SetActive(false);
        StartGame();

        //StartCoroutine(FirstInitialize());
    }


    public void TurnEndButtonClick()
    {
        if ((gameStart) && !isEffect)
        {
            TurnEnd();
            MePlayer.Turn = !MePlayer.Turn;
            OpponentPlayer.Turn = !OpponentPlayer.Turn;
            if (isConnected)
            {
                ClientConnect.SendTurnEnd(MePlayer, NetworkOrder++);
                Debug.Log("Turn end message sent!");
                TurnEndButton.SetActive(false);
            }
            NetworkOrder = 0;
            MessageQueue = new Dictionary<int, NetworkMessage>();
            TurnStart();
        }
    }

    public Player GetPlayerFromNetwork(int PlayerNo)
    {
        return PlayerNo == 1 == MePlayer.IsPlayer1inNetwork ? MePlayer : OpponentPlayer;
    }

    public Card GetCardFromName(string CardName, bool BelongsToOpponent = true)
    {
        Card clone = null;
		for( int i = 0; i < NormalDictionary.Count; i ++ )
        {
            if (NormalDictionary[i].Name == CardName)
			{
				clone = (Card)Activator.CreateInstance(NormalDictionary[i].GetType());
                break;
            }
        }
        for (int i = 0; i < GoldDictionary.Count; i++)
        {
            if (GoldDictionary[i].Name == CardName)
            {
                clone = (Card)Activator.CreateInstance(GoldDictionary[i].GetType());
                break;
            }
        }
        if (clone == null)
            Debug.LogError("ERROR: Given card name not found in dictionary: " + CardName);
        else
        {
            clone.Controller = BelongsToOpponent ? OpponentPlayer : MePlayer;
            clone.Opponent = BelongsToOpponent ? MePlayer : OpponentPlayer;
            clone.LinkedFieldObject.Controller = clone.Controller;
            clone.LinkedFieldObject.Opponent = clone.Opponent;
        }
        return clone;
    }


    IEnumerator Listen()
    {
		bool isGameEnd = false;
        while (true && !isGameEnd)
        {
            NetworkMessage message = null;

            JsonSerializer serializer = new JsonSerializer();


            /*
            if(ClientConnect.netStream.DataAvailable)
            {
				
                msg = ClientConnect.streamReader.ReadLine();
                Debug.Log("Got a message... : " + msg);
            }
                */
            if (ClientConnect.netStream.DataAvailable)
            {
                message = serializer.Deserialize<NetworkMessage>(new JsonTextReader(ClientConnect.streamReader));
                Debug.Log("Got a message... " + message.ToString());
            }

            if (message != null)
            {
                //Let's do something
                if (message.NetworkOrder == -1)
                {
                    switch (message.Type)
                    {
                        case NetworkMessage.MessageType.PlayerOrder:
                            /// Argument[] Protocol
                            /// 0 ~ 99 : Random Numbers
                            /// 100: Player Order
                            /// *Network Order is -1.

                            MePlayer.IsPlayer1inNetwork = Convert.ToInt32(message.Arguments[100]) == 1;
                           
                            OpponentPlayer.IsPlayer1inNetwork = !MePlayer.IsPlayer1inNetwork;
                            RandomQueue = new Queue<int>();

                            for (int i = 0; i < 100; i++)
                            {
                                RandomQueue.Enqueue(Convert.ToInt32(message.Arguments[i]));
                            }
                            //StartButton.SetActive(true);
                            ClientConnect.SendHand(GameObject.Find("DataBox").GetComponent<DataBox>().Deck, NetworkOrder);
                            break;



                        case NetworkMessage.MessageType.ConnectionCheck:

                            /// Argument[] Protocol
                            /// None
                            /// *Network Order is -1

                            ClientConnect.SendConnect();
                            break;

                        case NetworkMessage.MessageType.SendHand:

                            /// Argument[] Protocol
                            /// 0 ~ 9 : Name of a card
                            /// *Network Order is -1.
                            /// 
                            

                            for (int i = 0; i < 10; i++)
                            {
                                OpponentPlayer.Hand.Add(GetCardFromName((string)message.Arguments[i]));
                            }

                            for (int i = 0; i < OpponentPlayer.Hand.Count; i++)
                            {
                                Debug.Log(OpponentPlayer.Hand[i].Name);
                            }

							WaitingLabel.SetActive (false);
                            StartGame();

                            break;

                        case NetworkMessage.MessageType.SendHandRequest:

                            /// Argument[] Protocol
                            /// None
                            /// *Network Order is -1

                            ClientConnect.SendHand(GameObject.Find("DataBox").GetComponent<DataBox>().Deck, NetworkOrder);

                            break;

						case NetworkMessage.MessageType.GameEndReceive:

							/// Argument[] Protocol
							/// None
							/// *Network Order is -1
							/// If this receievd, Game will End
							isGameEnd = true;
							break;
                    }
                }
                else
                    MessageQueue.Add(message.NetworkOrder, message);
            }

            yield return null;
        }

		ClientConnect.NetworkEnd ();
		yield return new WaitForSeconds (3.0f);
		Application.LoadLevel ("Deck");

    }

    IEnumerator ExecuteMessage()
    {
        while (true)
        {


            NetworkMessage message;
            if (gameStart && MessageQueue.TryGetValue(NetworkOrder, out message))
            {

                Card handCard = null;

                switch (message.Type)
                {
                    case NetworkMessage.MessageType.Summon:
                        /// Argument[] Protocol
                        /// 0: Name of a card
                        /// 1: No. of the player
                        /// 2. Field number
                        /// 3. Is mystery (bool)
                        /// 4. Summon cost

                        Debug.Log("Summon Message Received");

                        OpponentPlayer.Resource -= Convert.ToInt32(message.Arguments[4]);

						

                        for (int i = 0; i < OpponentPlayer.Hand.Count; i++)
                        {
                            if (OpponentPlayer.Hand[i].Name == (string)message.Arguments[0])
                            {
                                handCard = OpponentPlayer.Hand[i];
                                break;
                            }
                        }
                        if (handCard == null)
                            Debug.LogError("Error: Card not found! : " + message.Arguments[0]);


                        handCard.LinkedFieldObject.Summon += (Unit, thisObject) =>
                            {
                                if (Unit == thisObject && thisObject.CardType != CardTypes.mystery)
                                    thisObject.NonTargetBattleCry(thisObject);
                            };

                        handCard.CurrentCost = Convert.ToInt32(message.Arguments[4]);
						handCard.UsingThisTurn = true;

                        OpponentPlayer.Hand.Remove(handCard);
                        GameAction.SummonAtPosition(GetPlayerFromNetwork(Convert.ToInt32(message.Arguments[1])).Field[Convert.ToInt32(message.Arguments[2])],
                            handCard.LinkedFieldObject, (bool)message.Arguments[3]);

                        break;

                    case NetworkMessage.MessageType.Attack:
                        /// Argument[] Protocol
                        /// 0: Attacker's Controller
                        /// 1: Attacker's Location
                        /// 2. Defender's Position - 5 if the player is the defender

                        FieldObject attacker = GetPlayerFromNetwork(Convert.ToInt32(message.Arguments[0])).Field[Convert.ToInt32(message.Arguments[1])];
                        FieldObject defender = Convert.ToInt32(message.Arguments[2]) == 5 ? attacker.Opponent : attacker.Opponent.Field[Convert.ToInt32(message.Arguments[2])];
                        GameAction.Attack(attacker, defender);

                        break;

                    /*
                case "RD":

                    //RandomValueQueue.Add(Int32.Parse(messages[2]), Int32.Parse(messages[1]));
                    Action<int> action;
                    /*
                    if (RandomActionQueue.TryGetValue(NetworkOrder, out action))
                        action(Int32.Parse(messages[1]));
                    else 
                        NetworkOrder--;
                     * *

                    break;
                     * */

                    case NetworkMessage.MessageType.MagicTarget:

                        /// Argument[] Protocol
                        /// 0: Spell's name
                        /// 1: Spell's owner
                        /// 2: Target's owner
                        /// 3: Target's position - 5 if the player is the target


                        for (int i = 0; i < OpponentPlayer.Hand.Count; i++)
                        {
                            if (OpponentPlayer.Hand[i].Name == (string)message.Arguments[0])
                            {
                                handCard = OpponentPlayer.Hand[i];
                                break;
                            }
                        }
                        if (handCard == null)
                            Debug.LogError("Error: Card not found! : " + message.Arguments[0]);

                        OpponentPlayer.Resource -= handCard.CurrentCost;

                        FieldObject target = Convert.ToInt32(message.Arguments[3]) == 5 ? GetPlayerFromNetwork(Convert.ToInt32(message.Arguments[2])) :
                            GetPlayerFromNetwork(Convert.ToInt32(message.Arguments[2])).Field[Convert.ToInt32(message.Arguments[3])];

						handCard.UsingThisTurn = true;
                        OpponentPlayer.Hand.Remove(handCard);

                        target = Trigger.SpellTargetTrigger(handCard, target);
                        handCard.TargetEvent(target, handCard);
						
                        if (handCard.Recyclable == true)
                        {
                            Card newCard = (Card)Activator.CreateInstance(handCard.GetType());
                            newCard.Controller = handCard.Controller;
                            newCard.Opponent = handCard.Opponent;
                            newCard.LinkedFieldObject.Controller = newCard.Controller;
                            newCard.LinkedFieldObject.Opponent = newCard.Opponent;

                            handCard.Controller.Grave.Add(newCard);
                        }

                        break;

                    case NetworkMessage.MessageType.Reverse:

                        /// Argument[] Protocol
                        /// 0: Owner
                        /// 1: Unit's Position

                        FieldObject FlipTarget;
                        FlipTarget = GetPlayerFromNetwork(Convert.ToInt32(message.Arguments[0])).Field[Convert.ToInt32(message.Arguments[1])];
                        FlipTarget.CardType = CardTypes.monster;
                        Trigger.ReverseTrigger(FlipTarget);
                        break;

                    case NetworkMessage.MessageType.BattleCryTarget:

                        /// Argument[] Protocol
                        /// 0: Skill user's owner
                        /// 1: Skill user's name
                        /// 2: Target's owner
                        /// 3: Target's position - 5 if the player is the target
                        /// 4: Skill user's position
                        /// 5: Summon cost
                        
                        string cardName = (string)message.Arguments[1];
                        Player cardOwner = GetPlayerFromNetwork(Convert.ToInt32(message.Arguments[0]));

                        for (int i = 0; i < cardOwner.Hand.Count; i++)
                        {
                            if (cardOwner.Hand[i].Name == cardName)
                                handCard = cardOwner.Hand[i];
                        }
                        if (handCard == null)
                            Debug.LogError("ERROR : Card not found! : " + cardName);

                        FieldObject battleCryTarget = Convert.ToInt32(message.Arguments[3]) == 5 ? GetPlayerFromNetwork(Convert.ToInt32(message.Arguments[2])) :
                            GetPlayerFromNetwork(Convert.ToInt32(message.Arguments[2])).Field[Convert.ToInt32(message.Arguments[3])];

                        handCard.LinkedFieldObject.Summon += (Unit, thisObject) =>
                        {
						if (Unit == thisObject && GameAction.IsFieldObjectOnField(battleCryTarget))
							thisObject.TargetBattleCry(battleCryTarget, thisObject);
                        };
                        handCard.CurrentCost = Convert.ToInt32(message.Arguments[5]);

                        OpponentPlayer.Resource -= Convert.ToInt32(message.Arguments[5]);
                        OpponentPlayer.Hand.Remove(handCard);
                        GameAction.SummonAtPosition(GetPlayerFromNetwork(Convert.ToInt32(message.Arguments[0])).Field[Convert.ToInt32(message.Arguments[4])], handCard.LinkedFieldObject, false);

                        break;


                    case NetworkMessage.MessageType.Retrieve:

                        /// Argument[] Protocol
                        /// 0: Player Number
                        /// 1: Amount of Resource
                        /// 2: Retrieved card's name

                        for (int i = 0; i < OpponentPlayer.Grave.Count; i++)
                        {
                            if (OpponentPlayer.Grave[i].Name == (string)message.Arguments[2])
                            {
                                handCard = OpponentPlayer.Grave[i];
                                break;
                            }
                        }
                        if (handCard == null)
                            Debug.LogError("ERROR: Card to retrieve not found in opponent's grave!");

                        OpponentPlayer.Grave.Remove(handCard);
                        OpponentPlayer.Hand.Add(handCard);
                        OpponentPlayer.Resource -= Convert.ToInt32(message.Arguments[1]);
                        Trigger.EnemyRetrieveTrigger(handCard);
                         
                        Debug.Log("상대가 무덤 회수를 했습니다!");

                        break;

                    case NetworkMessage.MessageType.TurnEnd:

                        /// Argument[] Protocol
                        /// 0: Player Number
                        /// 
                        TurnEnd();
                        MePlayer.Turn = !MePlayer.Turn;
                        OpponentPlayer.Turn = !OpponentPlayer.Turn;
                        //ClientConnect.SendTurnEnd(MePlayer, ++NetworkOrder);
                        NetworkOrder = 0;
                        MessageQueue = new Dictionary<int, NetworkMessage>();
                        //RandomActionQueue = new Dictionary<int, Action<int>>();
                        //RandomValueQueue = new Dictionary<int, int>();
                        TurnStart();
                        TurnEndButton.SetActive(true);

                        break;

                    case NetworkMessage.MessageType.MagicNon_Target:

                        /// Argument[] Protocol
                        /// 0: Spell's Name
                        /// 1: Spell's Owner 

                        for (int i = 0; i < OpponentPlayer.Hand.Count; i++)
                        {
                            if (OpponentPlayer.Hand[i].Name == (string)message.Arguments[0])
                            {
                                handCard = OpponentPlayer.Hand[i];
                                break;
                            }
                        }

                        OpponentPlayer.Resource -= handCard.CurrentCost;

						handCard.UsingThisTurn = true;
                        OpponentPlayer.Hand.Remove(handCard);

                        Trigger.SpellTrigger(handCard);
                        handCard.NonTargetEvent(handCard);


                        if (handCard.Recyclable == true)
                        {
                            Card newCard = (Card)Activator.CreateInstance(handCard.GetType());
                            newCard.Controller = handCard.Controller;
                            newCard.Opponent = handCard.Opponent;
                            newCard.LinkedFieldObject.Controller = newCard.Controller;
                            newCard.LinkedFieldObject.Opponent = newCard.Opponent;

                            handCard.Controller.Grave.Add(newCard);
                        }


                        break;

                    default:
                        Debug.LogError("ERROR: Received Unknown Protocol from Server");

                        break;
                }
                if (message.Type != NetworkMessage.MessageType.TurnEnd) NetworkOrder++;
            }
            yield return null;
        }
    }
    /*
    IEnumerator WaitForRandom(string key)
    {
        do
        {
            yield return null;
        } while (RandomValueQueue.ContainsKey(key));
        
    }
    */
    /*
    IEnumerator RandomWaiter()
    {
        networkSendRandom
        while( flag)
        {
            if ( networkRevieveRandom == true )
            {
                 flag = false;
            }
            yield return null;
        }

    }


    IEnumerator RandomCaller( todo )
    {
        yield return RandomWaiter();
        ExecuteInEditMode Todo;

    }
     * */

    public void ConnectButtonClick()
    {

        ClientConnect = new NetworkConnect();
        isConnected = ClientConnect.init();

        if (isConnected)
        {
            MessageQueue = new Dictionary<int, NetworkMessage>();
            StartCoroutine(Listen());
            StartCoroutine(ExecuteMessage());
            ConnectButton.SetActive(false);
            StartButton.SetActive(false);
            Debug.Log("Waiting for opponent");
        }
		WaitingLabel.SetActive (true);
    }

    public void EyeClicked()
    {
        if (InfoHelper != true)
        {
            InfoHelper = true;
            StopCoroutine("MainPhase");
            StartCoroutine("InfoPhase");
        }
        else
        {
            InfoHelper = false;
            InfoViewer.SetActive(false);
            StartCoroutine("MainPhase");
            StopCoroutine("InfoPhase");
        }

    }

    IEnumerator InfoPhase()
    {
        while (Clicked == false)
        {
            yield return null;
        }
        if (SelectedCard != null)
        {
            if (SelectedCard.CardType != CardTypes.empty && SelectedCard.CardType != CardTypes.mystery)
            {
                InfoReader(SelectedCard, null);
            }
            else if (SelectedCard.CardType == CardTypes.mystery && SelectedCard.Controller == MePlayer)
            {
                InfoReader(SelectedCard, null);
            }
        }
        if (SelectedObject != null)
        {
            if (SelectedObject.CardType != CardTypes.empty && SelectedObject.CardType != CardTypes.mystery)
            {
                InfoReader(null, SelectedObject);
            }
            else if (SelectedObject.CardType == CardTypes.mystery && SelectedObject.Controller == MePlayer)
            {
                InfoReader(null, SelectedObject);
            }
        }
        if (SelectedCard == null && SelectedObject == null)
        {
            InfoViewer.SetActive(false);
        }
        Trigger.BackToInfo();
    }

    public void InfoReader(Card InfoCard, FieldObject InfoObject)
    {
		if (InfoCard != null) 
		{
			InfoViewer.GetComponent<InfoObject>().PlayerHand = InfoCard;
			InfoViewer.GetComponent<InfoObject>().PlayerField = null;
		} 
		else if (InfoObject != null) 
		{
			InfoViewer.GetComponent<InfoObject>().PlayerField = InfoObject;
			InfoViewer.GetComponent<InfoObject>().PlayerHand = null;
		}
		if (InfoCard != null || InfoObject != null) 
		{
			InfoViewer.SetActive(true);
		}
    }



    public void SelectionPhase(PhaseType Types)
    {
        Clicked = false;
        UIButton Button;
        switch (Types)
        {
            case PhaseType.ControllerField:
                for (int i = 0; i < FieldNum; i++)
                {
                    Button = MyField[i].GetComponent<UIButton>();
                    Button.SetState(UIButtonColor.State.Hover, true);
                }
                break;
            case PhaseType.OpponentField:
                for (int i = 0; i < FieldNum; i++)
                {
                    Button = OpponentField[i].GetComponent<UIButton>();
                    Button.SetState(UIButtonColor.State.Hover, true);
                }
                break;
            case PhaseType.AllField:
                for (int i = 0; i < FieldNum; i++)
                {
                    Button = MyField[i].GetComponent<UIButton>();
                    Button.SetState(UIButtonColor.State.Hover, true);
                    Button = OpponentField[i].GetComponent<UIButton>();
                    Button.SetState(UIButtonColor.State.Hover, true);
                }
                break;
            case PhaseType.AllObject:
                for (int i = 0; i < HandNum; i++)
                {
                    Button = MyHand[i].GetComponent<UIButton>();
                    Button.SetState(UIButtonColor.State.Hover, true);
                }
                for (int i = 0; i < FieldNum; i++)
                {
                    Button = MyField[i].GetComponent<UIButton>();
                    Button.SetState(UIButtonColor.State.Hover, true);
                }
                for (int i = 0; i < FieldNum; i++)
                {
                    Button = OpponentField[i].GetComponent<UIButton>();
                    Button.SetState(UIButtonColor.State.Hover, true);
                }
                break;
            case PhaseType.Hand:
                for (int i = 0; i < HandNum; i++)
                {
                    Button = MyHand[i].GetComponent<UIButton>();
                    Button.SetState(UIButtonColor.State.Hover, true);
                }
                break;
        }
    }
    // Trigger Like Funciton

	public void Player1Face()
	{
		SelectedObject = MePlayer;
		Clicked = true;
	}

	public void Player2Face()
	{
		SelectedObject = OpponentPlayer;
		Clicked = true;
	}

    public void ButtonClickField00()
    {
        SelectedObject = MePlayer.Field[0];
        Clicked = true;
    }

    public void ButtonClickField01()
    {
        SelectedObject = MePlayer.Field[1];
        Clicked = true;
    }

    public void ButtonClickField02()
    {
        SelectedObject = MePlayer.Field[2];
        Clicked = true;
    }

    public void ButtonClickField03()
    {
        SelectedObject = MePlayer.Field[3];
        Clicked = true;
    }

    public void ButtonClickField04()
    {
        SelectedObject = MePlayer.Field[4];
        Clicked = true;
    }

    public void ButtonClickField10()
    {
        SelectedObject = OpponentPlayer.Field[0];
        Clicked = true;
    }

    public void ButtonClickField11()
    {
        SelectedObject = OpponentPlayer.Field[1];
        Clicked = true;
    }

    public void ButtonClickField12()
    {
        SelectedObject = OpponentPlayer.Field[2];
        Clicked = true;
    }

    public void ButtonClickField13()
    {
        SelectedObject = OpponentPlayer.Field[3];
        Clicked = true;
    }

    public void ButtonClickField14()
    {
        SelectedObject = OpponentPlayer.Field[4];
        Clicked = true;
    }

    public void ButtonClickCard00()
    {
        SelectedCard = MyHand[0].GetComponent<UIChanger>().PlayerHand;
        Clicked = true;
    }
    public void ButtonClickCard01()
    {
        SelectedCard = MyHand[1].GetComponent<UIChanger>().PlayerHand;
        Clicked = true;
    }
    public void ButtonClickCard02()
    {
        SelectedCard = MyHand[2].GetComponent<UIChanger>().PlayerHand;
        Clicked = true;
    }
    public void ButtonClickCard03()
    {
        SelectedCard = MyHand[3].GetComponent<UIChanger>().PlayerHand;
        Clicked = true;
    }
    public void ButtonClickCard04()
    {
        SelectedCard = MyHand[4].GetComponent<UIChanger>().PlayerHand;
        Clicked = true;
    }
    public void ButtonClickCard05()
    {
        SelectedCard = MyHand[5].GetComponent<UIChanger>().PlayerHand;
        Clicked = true;
    }
    public void ButtonClickCard06()
    {
        SelectedCard = MyHand[6].GetComponent<UIChanger>().PlayerHand;
        Clicked = true;
    }
    public void ButtonClickCard07()
    {
        SelectedCard = MyHand[7].GetComponent<UIChanger>().PlayerHand;
        Clicked = true;
    }
	public void ButtonClickCard08()
	{
		SelectedCard = MyHand[8].GetComponent<UIChanger>().PlayerHand;
		Clicked = true;
	}
	public void ButtonClickCard09()
	{
		SelectedCard = MyHand[9].GetComponent<UIChanger>().PlayerHand;
		Clicked = true;
	}

    IEnumerator TargetingBattleCryPhase(Card User)
    {
		//int Count = 0;
		for ( int i = 0; i < 5; i ++ )
		{
			if ( SelectedCard.LinkedFieldObject.BattleCryTargetValidityCheck( MePlayer.Field[i], SelectedCard.LinkedFieldObject ) == true )
			{
				MyField[i].GetComponent<UIChanger>().SelectionMarker.SetActive ( true );
		//		Count++;
			}
			if ( SelectedCard.LinkedFieldObject.BattleCryTargetValidityCheck( OpponentPlayer.Field[i], SelectedCard.LinkedFieldObject ) == true )
			{
				OpponentField[i].GetComponent<UIChanger>().SelectionMarker.SetActive ( true );
		//		Count++;
			}
		}
        while (Clicked == false)
        {
            yield return null;
        }
        if (SelectedObject != null)
        {
            if (!User.LinkedFieldObject.BattleCryTargetValidityCheck(SelectedObject, User.LinkedFieldObject))
                SelectedObject = null;
			//User.Controller.Resource -= User.CurrentCost;
            /*
            if (User.CardType == CardTypes.player)
            {
                SelectedObject = Trigger.ActiveTrigger(User, SelectedObject);
            }
             * */
            //User.TargetEvent(SelectedObject, User);
        }
        //Trigger.BackToMain();
    }

    IEnumerator TargetingSpellPhase(Card User)
    {
        while (Clicked == false)
        {
            yield return null;
        }
        if (SelectedObject != null && User.TargetValidityCheck(SelectedObject, User))
        {
            if (User.CardType != CardTypes.magic)
                Debug.LogError("Error: TargetingSpellPhase Called by a non-magic Card");
			User.Controller.Resource -= User.CurrentCost;
            if (isConnected)
            {
                ClientConnect.SendSpellTarget(User, SelectedObject, NetworkOrder++);
            }
            ReplaceCardInHandWithEmpty(User);
            SelectedObject = Trigger.SpellTargetTrigger(User, SelectedObject);
            
			if ( User.TargetValidityCheck( SelectedObject , User ) == true )
			{
            	User.TargetEvent(SelectedObject, User);
			}

            if (User.Recyclable)
            {
                Card newCard = (Card)Activator.CreateInstance(User.GetType());
                newCard.Controller = User.Controller;
                newCard.Opponent = User.Opponent;
                newCard.LinkedFieldObject.Controller = newCard.Controller;
                newCard.LinkedFieldObject.Opponent = newCard.Controller;

				newCard.UsingThisTurn = true;

                User.Controller.Grave.Add(newCard);
            }

			CardSort();

			if(Effect.EFC.EffectLastTime() - Time.time >=  0)
			{
				isEffect = true;
				yield return new WaitForSeconds( Effect.EFC.EffectLastTime() - Time.time);
			}

			isEffect = false;

            
        }
        else if (SelectedObject == null)
            Debug.Log("You must select a field object!");
        else
            Debug.Log("Invalid Target.");

        Trigger.BackToMain();
    }

    IEnumerator NonTargetingSpellPhase(Card User)
    {
		Card UserBefore = User;
		User = null;
		SelectedCard = null;
        while (Clicked == false)
        {
            yield return null;
        }

		if( UserBefore == SelectedCard )
		{
	        SelectedCard.Controller.Resource -= SelectedCard.CurrentCost;
	        if (isConnected)
	        {
	            ClientConnect.SendSpellNonTarget(SelectedCard, NetworkOrder++);
	        }
	        //SelectedObject = Trigger.SpellTargetTrigger(User, SelectedObject);
	        ReplaceCardInHandWithEmpty(SelectedCard);
	        Trigger.SpellTrigger(SelectedCard);

	        SelectedCard.NonTargetEvent(SelectedCard);

	        if (SelectedCard.Recyclable == true)
	        {
	            Card newCard = (Card)Activator.CreateInstance(SelectedCard.GetType());
	            newCard.Controller = SelectedCard.Controller;
	            newCard.Opponent = SelectedCard.Opponent;
	            newCard.LinkedFieldObject.Controller = newCard.Controller;
	            newCard.LinkedFieldObject.Opponent = newCard.Opponent;

				newCard.UsingThisTurn = true;

	            SelectedCard.Controller.Grave.Add(newCard);
	        }

			CardSort();

			if(Effect.EFC.EffectLastTime() - Time.time >=  0)
			{
				isEffect = true;
				yield return new WaitForSeconds( Effect.EFC.EffectLastTime() - Time.time);
			}

			isEffect = false;
		}
        CardSort();
        Debug.Log("Moved to spell phase");

        Trigger.BackToMain();
    }

    IEnumerator MyRetrievePhase (Card User)
    {
		Card UserBefore = User;
		User = null;
		SelectedCard = null;
        while (Clicked == false)
        {
            yield return null;
        }

		if (( UserBefore == SelectedCard ) && ( !UserBefore.UsingThisTurn ) && (UserBefore.Controller.Resource >= UserBefore.CurrentGraveCost))
		{
        	MePlayer.Resource -= SelectedCard.CurrentGraveCost;
        	SelectedCard.Controller.Grave.Remove(SelectedCard);
        	SelectedCard.Controller.Hand.Add(SelectedCard);
        	if (isConnected == true)
        	{
	            ClientConnect.SendRetrieve(SelectedCard.Controller, SelectedCard.CurrentGraveCost, SelectedCard, NetworkOrder++);
        	}

			Trigger.RetrieveTrigger(SelectedCard);

			if(Effect.EFC.EffectLastTime() - Time.time >=  0)
			{
				isEffect = true;
				yield return new WaitForSeconds( Effect.EFC.EffectLastTime() - Time.time);
			}

			isEffect = false;

        	CardSort();

		}
        Trigger.BackToMain();
    }

    IEnumerator ReversePhase(FieldObject SelectObject)
    {
		FieldObject SelectedObjectBefore = SelectObject;
		FieldToUI(SelectedObject).GetComponent<UIChanger> ().ReverseMarker.SetActive (true);
		SelectObject = null;

        while (Clicked == false)
        {
            yield return null;
        }

		if (SelectedObjectBefore == SelectedObject) 
		{
			GameAction.Reverse (SelectedObject);
			if (isConnected == true)
			{
				ClientConnect.SendReverse(SelectedObject, NetworkOrder++);
			}
		}
		Trigger.BackToMain();
    }

    public void EnemyRetrieve (Card User)
    {
        FieldObject Me;

        for (int i = 0; i < 5; i++)
        {
            Me = GameAction.MainSystem.MePlayer.Field[i];
            Me.Retrieve(User, Me);
            Me = GameAction.MainSystem.OpponentPlayer.Field[i];
            Me.Retrieve(User, Me);
        }

        Card MeHand;

        for (int i = 0; i < GameAction.MainSystem.MePlayer.Hand.Count; i++)
        {
            MeHand = GameAction.MainSystem.MePlayer.Hand[i];
            MeHand.Retrieve(User, MeHand);
        }
        for (int i = 0; i < GameAction.MainSystem.OpponentPlayer.Hand.Count; i++)
        {
            MeHand = GameAction.MainSystem.OpponentPlayer.Hand[i];
            if (MeHand == null)
                Debug.Log(i);
            MeHand.Retrieve(User, MeHand);
        }

        Trigger.BackToMain();
    }

    public void ReplaceCardInHandWithEmpty(Card CardToBeReplaced)
    {
        if (!CardToBeReplaced.Controller.Hand.Remove(CardToBeReplaced))
            Debug.LogError("Error: Couldn't find card to be replaced with empty in its controller's hand.");

        CardSort();
        return;
    }
    public void CardSort()
    {
        Card empty = new EmptyCard();
        empty.Controller = MePlayer;
        empty.Opponent = OpponentPlayer;
        for (int i = 0; i < MePlayer.Hand.Count; i++)
        {
            MyHand[i].GetComponent<UIChanger>().PlayerHand = MePlayer.Hand[i];
        }
        //Debug.Log ("Success1");
        for (int j = 0; j < MePlayer.Grave.Count; j++)
        {
            MyHand[j + MePlayer.Hand.Count].GetComponent<UIChanger>().PlayerHand = MePlayer.Grave[j];
        }
        //Debug.Log ("Success2");
        for (int k = MePlayer.Hand.Count + MePlayer.Grave.Count; k < 10; k++)
        {
            MyHand[k].GetComponent<UIChanger>().PlayerHand = empty;
        }
        //Debug.Log ("Success3");
        /*for (int j =  MePlayer.Hand.Count; j < MePlayer.Hand.Count + MePlayer.Grave.Count; j ++) 
        {
            MyHand[j] = MePlayer.Hand[i];
        }*/
    }

	public void UISort()
	{
		Card empty = new EmptyCard();
		List<Card> ListForUI = new List<Card>();

		for (int i = 0; i < MyHand.Length; i++)
		{
			ListForUI.Add( MyHand[i].GetComponent<UIChanger>().PlayerHand );
		}
		for (int i = 0; i < ListForUI.Count; i++) 
		{
			if ( ListForUI[i].CardType == CardTypes.empty )
			{
				ListForUI.RemoveAt(i);
				i--;
			}
		}
	}



    IEnumerator Summon(Card User)
    {
		Card SelectedBefore = User;

		for (int i = 0; i < MyField.Length; i++) 
		{
			if ( MePlayer.Field[i].CardType == CardTypes.empty && Reverse == false )
			{
				MyField[i].GetComponent<UIChanger>().SelectionMarker.SetActive( true );
				MyField[i].GetComponent<UIChanger>().ReverseMarker.SetActive( false );
			}
			else if ( MePlayer.Field[i].CardType == CardTypes.empty && Reverse == true )
			{
				MyField[i].GetComponent<UIChanger>().ReverseMarker.SetActive( true );
				MyField[i].GetComponent<UIChanger>().SelectionMarker.SetActive( false );
				CardToUI( User).GetComponent<UIChanger>().SelectionMarker.SetActive (false );
				CardToUI( User).GetComponent<UIChanger>().ReverseMarker.SetActive ( true );
			}
		}

        Debug.Log("Summon Start");
		SelectedCard = null;
        while (Clicked == false)
        {
            //Debug.Log("Summon Running");
            yield return null;
        }

        if (SelectedObject != null && SelectedObject.CardType == CardTypes.empty && SelectedObject.Controller == MePlayer)
        {
			SelectedCard = SelectedBefore;
			for (int i = 0; i < MyField.Length; i++) 
			{
				MyField[i].GetComponent<UIChanger>().SelectionMarker.SetActive( false );
			}
			for (int i = 0; i < OpponentField.Length; i++) 
			{
				OpponentField[i].GetComponent<UIChanger>().SelectionMarker.SetActive( false );
			}
            //아 코드 개판되는 것 같네요. 으으으으;;
            FieldObject selO = SelectedObject;
            if (!Reverse && SelectedCard.LinkedFieldObject.TargetBattleCry != null)
            {
                Debug.Log("Choose Battlecry Target");
				FieldObject TempObject = FieldToUI( SelectedObject ).GetComponent<UIChanger>().PlayerField;
				FieldToUI( SelectedObject ).GetComponent<UIChanger>().PlayerField = SelectedCard.LinkedFieldObject;
				CardToUI( SelectedCard ).GetComponent<UIChanger>().PlayerHand = new EmptyCard();
				UISort();
				int Count = 0;
				for( int i = 0; i < 5; i++ )
				{
					if ( User.LinkedFieldObject.BattleCryTargetValidityCheck( MePlayer.Field[i], User.LinkedFieldObject ) == true ) Count++;
					if ( User.LinkedFieldObject.BattleCryTargetValidityCheck( OpponentPlayer.Field[i], User.LinkedFieldObject ) == true ) Count++;
				}

				if ( User.LinkedFieldObject.BattleCryTargetValidityCheck( MePlayer, User.LinkedFieldObject ) == true ) Count++;
				if ( User.LinkedFieldObject.BattleCryTargetValidityCheck( OpponentPlayer, User.LinkedFieldObject ) == true ) Count++;

				if ( Count > 0 )
				{
					Trigger.TargetingBattleCry(User);
				}

                while (!Clicked)
                    yield return null;

				if ( FieldToUI( SelectedCard.LinkedFieldObject ) != null )
				{
					FieldToUI( SelectedCard.LinkedFieldObject ).GetComponent<UIChanger>().PlayerField = TempObject;
				}

                if (SelectedObject == null)
                {
                    Debug.Log("Battlecry Targeting Failed");
                    Trigger.BackToMain();
                    
                    yield break; // 코루틴을 강제로 탈출시키는 명령어.
                }
                else
                {
                    FieldObject target = SelectedObject;
                    if (isConnected)
                    {
                        ClientConnect.SendBattleCryTarget(SelectedCard, target, selO, NetworkOrder++);
                    }
                    SelectedCard.LinkedFieldObject.Summon += (Unit, thisObject) =>
                        {
                            if (Unit == thisObject && GameAction.IsFieldObjectOnField(target))
                                thisObject.TargetBattleCry(target, thisObject);
                        };
                    SelectedObject = selO;
                }

            }
            else
                if (isConnected)
                {
                    ClientConnect.SendSummon(User, SelectedObject.GetPosition(), Reverse, SelectedCard.CurrentCost, NetworkOrder++);
                }

            Debug.Log("Summoned");

            
            MePlayer.Resource -= SelectedCard.CurrentCost;

            SelectedCard.LinkedFieldObject.Summon += (Unit, thisObject) =>
            {
                if (Unit == thisObject && thisObject.CardType != CardTypes.mystery)
                    thisObject.NonTargetBattleCry(thisObject);
            };

            ReplaceCardInHandWithEmpty(User);
            GameAction.SummonAtPosition(SelectedObject, User.LinkedFieldObject, Reverse);

			if(Effect.EFC.EffectLastTime() - Time.time >=  0)
			{
				isEffect = true;
				yield return new WaitForSeconds( Effect.EFC.EffectLastTime() - Time.time);
			}

			isEffect = false;

            Trigger.BackToMain();


            /*
            for ( int i = 0 ; i < 5; i ++ )
            {
                if ( Player1.Field[i] == SelectedObject )
                {
                    Debug.Log ("Changed");
                    Changer.FieldChange( User.Content, P1Field[i] );
                    Player1.Field[i] = User.Content;
                    Player1.Field[i].Controller = Player1;
                    Player1.Field[i].Opponent = Player2;
                    if(isConnected)
                        ClientConnect.SendSummon(User,i);
                    Trigger.SummonTrigger(Player1.Field[i]);
                }
                /*if ( Player2.Field[i] == SelectedObject )
                {
                    Debug.Log ("Changed");
                    Changer.FieldChange( User.Content, P2Field[i] );
                    Player2.Field[i] = User.Content;
                    Player2.Field[i].Controller = Player2;
                    Player2.Field[i].Opponent = Player1;
                    Trigger.SummonTrigger(Player2.Field[i]);
                }
            }
        */

        }
        else if (SelectedBefore == SelectedCard)
        {
            Reverse = true;
            Debug.Log("Reverse Summoning!");
            Trigger.Summoning(User);
        }
        else
        {
            Debug.Log("Summon Failed Due to Invalid Placement - You need to place in one of your empty field slots.");
            Trigger.BackToMain();
        }


    }

    /*IEnumerator GravePick( )
    {
        Debug.Log ("Grave Pick Start");
        while  ( Clicked == false )
        {
            yield return null;
        }
        if (SelectedCard.CardType != CardTypes.empty || SelectedCard.Controller.Resource > SelectedCard.CurrentGraveCost )
        {
            if (isConnected)
            {
                ClientConnect.SendGraveDraw(SelectedCard.Controller, NetworkOrder);
                NetworkOrder += 1;
            }
            //Trigger.RetrieveTrgger , In here I have to call Retrive Trigger 
            //CardList.Add
            for ( int i = 0; i < MePlayer.Hand.Count; i ++ )
            {
                if ( MePlayer.Hand[i].CardType == CardTypes.empty )
                {
                    MePlayer.Hand[i] = SelectedCard;
                }

            }
        }
        Trigger.BackToMain ();
        //WStartCo
    }*/

    public void DamageMarker(FieldObject DamagedObject, int Damage)
    {
        Debug.Log("Damage Marker Say");
        GameObject DamageMarkOrigin = Resources.Load<GameObject>("DamageMarker");
        GameObject DamageMark = (GameObject)Instantiate(DamageMarkOrigin);
        DamageMark.transform.SetParent(UIParent.transform);
        DamageMark.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        DamageMark.GetComponent<UILabel>().text = Damage.ToString();
        if (DamagedObject.CardType == CardTypes.player)
        {
            return;
        }
        if (DamagedObject.Controller == MePlayer)
        {
            Debug.Log("Damage Marker MePlayer Position");
            Debug.Log(MyField[DamagedObject.GetPosition()].transform.localPosition);
            DamageMark.transform.localPosition = MyField[DamagedObject.GetPosition()].transform.localPosition;
        }
        if (DamagedObject.Controller == OpponentPlayer)
        {
            Debug.Log("Damage Marker OpponentPlayer Position");
            DamageMark.transform.localPosition = OpponentField[DamagedObject.GetPosition()].transform.localPosition;
        }
    }

    IEnumerator BattlePhase()
    {
        Debug.Log("BattlePhase");
        while (Clicked == false)
        {
            yield return null;
        }
        if (SelectedObject != null && SelectedObject.CardType != CardTypes.empty)
        {
            if (SelectedObject.Controller != SelectedObjectBefore.Controller && SelectedObjectBefore.AttackValidityCheck(SelectedObject, SelectedObjectBefore))
            {
                Debug.Log("Attack Start");
                if (isConnected)
                    ClientConnect.SendAttack(SelectedObjectBefore, SelectedObject, NetworkOrder++);
                GameAction.Attack(SelectedObjectBefore, SelectedObject);
                Trigger.BackToMain();
            }
            else
            {
                SelectedObject = null;
                Trigger.BackToMain();
            }
        }
        else
            Trigger.BackToMain();
    }

	public GameObject CardToUI ( Card thisCard )
	{
		for (int i = 0; i < MyHand.Length; i++) 
		{
			if ( MyHand[i].GetComponent<UIChanger>().PlayerHand == thisCard )
			{
				return MyHand[i];
			}
		}
		Debug.Log ("Something Wrong");
		return null;
	}

	public GameObject FieldToUI ( FieldObject thisField )
	{
		for (int i = 0; i < 5; i++) 
		{
			if ( MyField[i].GetComponent<UIChanger>().PlayerField == thisField )
			{
				return MyField[i];
			}
			if ( OpponentField[i].GetComponent<UIChanger>().PlayerField == thisField )
			{
				return OpponentField[i];
			}
		}
		if (thisField == MePlayer) 
		{
			return P1Face;
		}
		else if (thisField == OpponentPlayer) 
		{
			return P2Face;
		}
		Debug.Log ("Something Wrong");
		return null;
	}
	

    IEnumerator MainPhase()
    {
        Debug.Log("MainPhase");
        Reverse = false;
		CardSort();
		for (int i = 0; i < MyHand.Length; i ++) 
		{
			MyHand[i].GetComponent<UIChanger>().SelectionMarker.SetActive( false );
			MyHand[i].GetComponent<UIChanger>().ReverseMarker.SetActive( false );
		}
		for (int i = 0; i < MyField.Length; i++) 
		{
			MyField[i].GetComponent<UIChanger>().SelectionMarker.SetActive( false );
			MyField[i].GetComponent<UIChanger>().ReverseMarker.SetActive(false);
		}
		for (int i = 0; i < OpponentField.Length; i++) 
		{
			OpponentField[i].GetComponent<UIChanger>().SelectionMarker.SetActive( false );
			OpponentField[i].GetComponent<UIChanger>().ReverseMarker.SetActive(false);
		}

		for (int i = 0; i < MyField.Length; i++) 
		{
			MyField[i].GetComponent<UIChanger>().PlayerField = MePlayer.Field[i];
		}

		for (int i = 0; i < OpponentField.Length; i++) 
		{
			OpponentField[i].GetComponent<UIChanger>().PlayerField = OpponentPlayer.Field[i];
		}
		InfoReader ( null, null );
        while (Clicked == false)
        {
            //Debug.Log("Mainphase Running");
            yield return null;
        }
		InfoReader ( SelectedCard, SelectedObject );
        if (!MePlayer.Turn)
        {
            Debug.Log("It's not your turn!");
            Trigger.BackToMain();
        }

        if (SelectedCard != null)
        {
            // 나중에 무덤 회수 작업도 이쪽 프로세스에서 하려면 이걸 좀 바꿔야 함.
            /*if (SelectedCard.CardType !=CardTypes.empty && SelectedCard.Grave == true && SelectedCard.GraveCost <=MePlayer.Resource )
            {
                MePlayer.Resource -= SelectedCard.GraveCost;
                MePlayer.Hand.Add(SelectedCard);
                MePlayer.Grave.Remove(SelectedCard);
                CardSort();
            }*/
			CardToUI( SelectedCard ).GetComponent<UIChanger>().SelectionMarker.SetActive( true );

            if (GameAction.IsCardInGrave(SelectedCard) == true)
            {
				Trigger.RetrieveCoroutineStart(SelectedCard);
            }
            else if (SelectedCard.CardType != CardTypes.empty && SelectedCard.Controller == MePlayer && SelectedCard.CurrentCost <= MePlayer.Resource)
            {
                if (SelectedCard.CardType == CardTypes.magic)
                {
                    if (SelectedCard.NonTargetEvent != null)
                    {
                        if (SelectedCard.CardType != CardTypes.magic)
                            Debug.LogError("Error: NontargetSpell Called by a non-magic Card");

                        Trigger.NonTargetingSpell(SelectedCard);
                    }
                    else if (SelectedCard.TargetEvent != null)
                    {
                        //Trigger.TargetingAction( SelectedCard.TargetEvent );				
						for ( int i = 0; i < 5; i ++ )
						{
							if ( SelectedCard.TargetValidityCheck( SelectedCard.Controller.Field[i], SelectedCard ) == true )
							{
								FieldToUI( 	SelectedCard.Controller.Field[i] ).GetComponent<UIChanger>().SelectionMarker.SetActive( true );
							}
                            if (SelectedCard.TargetValidityCheck(SelectedCard.Opponent.Field[i], SelectedCard) == true)
                            {
                                FieldToUI(SelectedCard.Opponent.Field[i]).GetComponent<UIChanger>().SelectionMarker.SetActive(true);
                            }
						}

                        Trigger.TargetingSpell(SelectedCard);
                    }
                    else
                    {
                        Reverse = true;
                        Trigger.Summoning(SelectedCard);
                    }
                }
                else if (SelectedCard.CardType == CardTypes.monster)
                {
                    Trigger.Summoning(SelectedCard);
                }
            }
            else if (SelectedCard.CardType == CardTypes.empty)
            {
                Debug.LogError("Player attempted to use an empty card! This should never happen!");
                Trigger.BackToMain();
            }
            else if (SelectedCard.Controller != MePlayer)
            {
                Debug.LogError("Player tried to use opponent's card! This should never happen!");
                Trigger.BackToMain();
            }
            else
            {
                DisplayMessage("자원이 모자랍니다.");
                Debug.Log("Not enough minerals");
                Trigger.BackToMain();
            }
        }
        else if (SelectedObject != null && SelectedObject.Controller == MePlayer)
        {
		    if (SelectedObject.CardType == CardTypes.mystery && SelectedObject.Activity == true )
            {
                Trigger.MyReverseTrigger(SelectedObject);
            }
            if (SelectedObject.Activity == true && SelectedObject.CurrentAp > 0)
            {
                SelectedObjectBefore = SelectedObject;
				SelectedObject = null;
				Trigger.BackToBattle();
            }
            else
            {
                Debug.Log("Nothing to do with it.");
				SelectedObject = null;
                Trigger.BackToMain();
            }
        }
		else
		{
			Debug.Log("Nothing to do with it.");
			SelectedObject = null;
			Trigger.BackToMain();
		}
    }

    public void GameEnd(Player Winner)
    {
		Shadow.SetActive (true);
        if ( MePlayer == Winner) 
		{
			Win.SetActive (true);
			Debug.Log (Winner.Name + "이(가) 게임에서 이겼습니다!");
			//LogData.
		}
        else
		{
			Lose.SetActive (true);
            Debug.Log("비겼어요!");
		}

		if (isConnected) 
			ClientConnect.SendGameEnd();
    }


    void Update()
    {
        if (!gameStart) return;

        MyHP.text = MePlayer.CurrentHp.ToString();
        MyMana.text = MePlayer.Resource.ToString();
        OpponentHP.text = OpponentPlayer.CurrentHp.ToString();
        OpponentMana.text = OpponentPlayer.Resource.ToString();
    }

    void DisplayMessage(string message)
    {
        //시프가 약합니다.
    }
}
