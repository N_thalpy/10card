﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Base;
using UnityEngine;

public class LogData
{
    public enum ActionType
    {
        Summon,
        Spell,
        Reverse,
    }
    public readonly ActionType Action;
    public readonly Card Card;
    public readonly FieldObject FieldObject;

    public LogData(ActionType action, Card card)
    {
        Card c = new EmptyCard();

        this.Action = action;
        this.Card = c.CardCopy(card);
		this.Card.Controller = card.Controller;
    }
    public LogData(ActionType action, FieldObject fieldObject)
    {
        FieldObject f = new EmptyField();

        this.Action = action;
        this.FieldObject = f.FieldObjectCopy(fieldObject);
		this.FieldObject.Controller = fieldObject.Controller;
    }
}