﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Base;

public class LogSprite : MonoBehaviour {

	public Vector3 StartPosition;
	public Vector3 EndPosition;

	public bool Move = false;
	public float StartTime = 0.0f;
	public float EndTime = 0.0f;

	public FieldObject ThisField = new EmptyField();
	public Card ThisCard = new EmptyCard();
	public GameObject CoverImage;
	public UI2DSprite SlotImage;

	void Update () {

		if ( ThisField != null ) this.GetComponent<UI2DSprite>().sprite2D = Resources.Load<Sprite> ("Cards/" + ThisField.ImagePath);
		if ( ThisCard != null ) this.GetComponent<UI2DSprite>().sprite2D = Resources.Load<Sprite> ("Cards/" + ThisCard.ImagePath);

		Player Controller = null;
		
		if (ThisField != null && ThisField.CardType != CardTypes.empty ) Controller = ThisField.Controller;
		if (ThisCard != null)	Controller = ThisCard.Controller;

		if (ThisCard != null) 
		{
			CoverImage.SetActive (false);
		}
		if (ThisField != null && ( ThisField.CardType == CardTypes.mystery  ))
		{
			CoverImage.SetActive (true);
		} 
		else if (ThisField != null && ThisField.CardType != CardTypes.mystery)
		{
			CoverImage.SetActive (false);
		}

		if (Controller == GameAction.MainSystem.MePlayer) {
			SlotImage.gameObject.SetActive( true );
			SlotImage.GetComponent<UI2DSprite> ().sprite2D = Resources.Load<Sprite> ("CardEffect/selection_green");
		} else if (Controller == GameAction.MainSystem.OpponentPlayer) {
			SlotImage.gameObject.SetActive( true );
			SlotImage.GetComponent<UI2DSprite> ().sprite2D = Resources.Load<Sprite> ("CardEffect/selection_red");
		} 
		else 
		{
			SlotImage.gameObject.SetActive( false );
		}

		if (Move == true) 
		{
			float ThisTime = Time.time;
			if ( ThisTime >= EndTime ) 
			{
				ThisTime = EndTime;
				this.transform.localPosition = (EndPosition-StartPosition)*(ThisTime-StartTime)/(EndTime - StartTime)+StartPosition ;
				Move = false;
			}
			this.transform.localPosition = (EndPosition-StartPosition)*(ThisTime-StartTime)/(EndTime - StartTime)+StartPosition;
		}
	}
}
