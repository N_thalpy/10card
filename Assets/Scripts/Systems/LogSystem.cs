﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Base;
using UnityEngine;

public class LogSystem
{
    public readonly static List<LogData> LogStack = new List<LogData>()
	{
		new LogData(LogData.ActionType.Summon, new EmptyCard()),
		new LogData(LogData.ActionType.Summon, new EmptyCard()),
		new LogData(LogData.ActionType.Summon, new EmptyCard()),
		new LogData(LogData.ActionType.Summon, new EmptyCard()),
		new LogData(LogData.ActionType.Summon, new EmptyCard())
	};

    public static void ResetLog()
    {
        LogStack.Clear();
    }
    public static void Log(LogData.ActionType action, Card card)
    {
        LogStack.Add(new LogData(action, card));
        Debug.Log(String.Format("{0}, {1}", action, card));
    }
    public static void Log(LogData.ActionType action, FieldObject fieldObject)
    {
        LogStack.Add(new LogData(action, fieldObject));
        Debug.Log(String.Format("{0}, {1}", action, fieldObject));
    }
    public static LogData[] GetLog(int logNumber = 30)
    {
        LogData[] data = new LogData[logNumber];
        for (int i = 0; i < logNumber; i++)
        {
            if (LogStack.Count > i)
                data[i] = LogStack[LogStack.Count - 1 - i];
        }

        return data;
    }
}