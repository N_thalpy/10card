﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Text;
using System.Net.Sockets;
using Base;
using Newtonsoft.Json;

public class NetworkConnect {

	public System.IO.StreamWriter streamWriter;
	public System.IO.StreamReader streamReader;
	public TcpClient Client_Socket;
	public NetworkStream netStream;

	public bool init()
	{
		try
		{
           
            Client_Socket = new TcpClient("125.178.150.120", 2100);
			//Client_Socket = new TcpClient("localhost", 2100);
		}
		catch
		{
			Debug.Log("Failed to connect to server");
			return false;
		}

		netStream = Client_Socket.GetStream();
		streamWriter = new System.IO.StreamWriter(netStream);
		streamReader = new System.IO.StreamReader(netStream);
		
		Debug.Log("Connected to Server");

		return true;
	}

	public void SendtoServer(NetworkMessage message)
    {

        JsonSerializer serializer = new JsonSerializer();
        try
        {
            serializer.Serialize(streamWriter, message);
            streamWriter.Flush();
        }
        catch
        {
            Debug.Log("Message Send Failed");
        }
 

         
    }

	//서버에 자신의 패를 넘깁니다.

	public void SendHand(List<Card> HandList,  int order_)
	{
        /// Argument[] Protocol
        /// 0 ~ 9 : Name of a card
        /// *Network Order is -1.

        System.Object[] arguments = new System.Object[10];
		for (int i = 0; i < HandList.Count; i ++) 
		{
            arguments[i] = HandList[i].Name;
		}

		Debug.Log ("Send Hand");

        SendtoServer(new NetworkMessage(NetworkMessage.MessageType.SendHand, arguments));
	}


	//서버에 소환한다는 메시지를 보내면서 카드 이름, 낸 플레이어, 낸 위치를 보냅니다.
	public void SendSummon(Card User, int OwnerFieldNumber, bool isMystery, int summonCost, int order_)
    {
        /// Argument[] Protocol
        /// 0: Name of a card
        /// 1: No. of the player
        /// 2. Field number
        /// 3. Is mystery (bool)
        /// 4. Summon cost

        SendtoServer(new NetworkMessage(NetworkMessage.MessageType.Summon,
            new System.Object[5] { User.Name, User.Controller.getNetworkID(), OwnerFieldNumber, isMystery, summonCost }, order_));
	}

	//서버에 공격 명령을 전달합니다. 공격자의 공격력, 공격자의 위치, 방어자의 체력을 보냅니다.
	public void SendAttack(FieldObject Attacker, FieldObject Defender, int order_)
	{
        /// Argument[] Protocol
        /// 0: Attacker's Controller
        /// 1: Attacker's Location
        /// 2. Defender's Position - 5 if the player is the defender


		int AtrLocation = Attacker.GetPosition ();
		int PlayerNum = Attacker.Controller.getNetworkID();



        SendtoServer(new NetworkMessage(NetworkMessage.MessageType.Attack,
            new System.Object[3] { PlayerNum, AtrLocation, Defender.CardType == CardTypes.player ? 5 : Defender.GetPosition() }, order_));
         
	}

	//서버에 랜덤 넘버를 요청합니다. 범위의 가장 작은 숫자와 큰 숫자를 보냅니다.
    /// <summary>
    /// 사용되지 않는 함수입니다. 쓰지 마세요.
    /// </summary>
	public void SendRandom(int First, int Last, int order_)
	{
        /// Unused
		string FirstString = First.ToString ();
		string LastString = Last.ToString ();
		
		string SendMsg = "RD" + "/" + FirstString + "/" + LastString + "/" + order_.ToString();
		
		//SendtoServer (SendMsg);
	}

	//서버에 타겟팅 마법 카드 사용을 전달합니다. 마법카드의 이름, 타겟의 이름, 타겟의 체력, 타겟의 위치를 보냅니다.
	public void SendSpellTarget(Card User, FieldObject Target, int order_)
	{
        /// Argument[] Protocol
        /// 0: Spell's name
        /// 1: Spell's owner
        /// 2: Target's owner
        /// 3: Target's position - 5 if the player is the target
        
		string MgName = User.Name;
		int MgOwner = User.Controller.getNetworkID();
		int TgOwner = Target.Controller.getNetworkID();
        int TgLocation = Target.CardType == CardTypes.player ? 5 : Target.GetPosition();

        SendtoServer(new NetworkMessage(NetworkMessage.MessageType.MagicTarget,
            new System.Object[4] { MgName, MgOwner, TgOwner, TgLocation }, order_));
	}

    public void SendSpellNonTarget(Card User, int order_)
    {
        /// Argument[] Protocol
        /// 0: Spell's Name
        /// 1: Spell's Owner 
        string MgName = User.Name;
        int MgOwner = User.Controller.getNetworkID();


        SendtoServer(new NetworkMessage(NetworkMessage.MessageType.MagicNon_Target,
            new System.Object[2] { MgName, MgOwner }, order_));
    }

	//서버에 리버스 카드의 내용을 전달합니다. 낸 플레이어, 카드 이름을 보냅니다.
	public void SendReverse(FieldObject User, int order_)
	{
        /// Argument[] Protocol
        /// 0: Owner
        /// 1: Unit's Position
       
		int CdPos = User.GetPosition();
		int CdOwner = User.Controller.getNetworkID();

        SendtoServer(new NetworkMessage(NetworkMessage.MessageType.Reverse,
            new System.Object[2] { CdOwner, CdPos }, order_));
	}

	//서버에 선택형 전투의 함성이 있는 하수인의 소환을 전달합니다. 스킬 사용자의 주인 및 위치, 타겟의 주인 및 위치를 보냅니다.
	public void SendBattleCryTarget(Card User, FieldObject Target, FieldObject locToSummon, int order_)
	{
        /// Argument[] Protocol
        /// 0: Skill user's owner
        /// 1: Skill user's name
        /// 2: Target's owner
        /// 3: Target's position - 5 if the player is the target
        /// 4: Skill user's position
        /// 5: Summon cost
        
		string CdName = User.Name;
		int CdOwner = User.Controller.getNetworkID();
		int TgOwner = Target.Controller.getNetworkID();
        int TgLocation = Target.CardType == CardTypes.player ? 5 : Target.GetPosition();
        int CdLocation = locToSummon.GetPosition();
        int cost = User.CurrentCost;

        SendtoServer(new NetworkMessage(NetworkMessage.MessageType.BattleCryTarget,
            new System.Object[6] { CdOwner, CdName, TgOwner, TgLocation, CdLocation, cost }, order_));
	}

	//서버에 묘지 드로우를 전달합니다. 드로우 한 플레이어 넘버를 보냅니다.
	public void SendRetrieve(Player player_, int resource_ , Card card, int order_)
	{
        /// Argument[] Protocol
        /// 0: Player Number
        /// 1: Amount of Resource
        /// 2: Retrieved card's name

        int PyNum = player_.getNetworkID();



        SendtoServer(new NetworkMessage(NetworkMessage.MessageType.Retrieve,
            new System.Object[3] { PyNum, resource_, card.Name }, order_));
	}

	//서버에 턴 종료를 전달합니다. 턴 종료한 플레이어 넘버를 보냅니다.
	public void SendTurnEnd(Player SendPlayer, int order_)
	{
        /// Argument[] Protocol
        /// 0: Player Number
		int Playernum = SendPlayer.getNetworkID();



        SendtoServer(new NetworkMessage(NetworkMessage.MessageType.TurnEnd,
            new System.Object[1] { Playernum }, order_));
	}

	public void SendGameEnd()
	{
		SendtoServer(new NetworkMessage(NetworkMessage.MessageType.GameEnd,
		                                new System.Object[0]));
	}


	public void SendConnect()
	{
        /// Argument[] Protocol
        /// None
        /// *Network Order is -1
        SendtoServer(new NetworkMessage(NetworkMessage.MessageType.ConnectionCheck,
            new System.Object[0]));
	}

	public void NetworkEnd()
	{
		netStream.Close();
		streamReader.Close ();
		streamWriter.Close ();
	}
}
