﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Serialization;

namespace Base
{ 
     /*class NetworkMessage
    {
        public int NetworkOrder;

        public static NetworkMessage ParseMessage(string message)
        {
            return null;
        }

    }*/

    [Serializable]
    public class NetworkMessage
    {
        public enum MessageType
        {
            PlayerOrder,
            ConnectionCheck,
            SendHand,
            Summon,
            Attack,
            TurnEnd,
            MagicTarget,
            Reverse,
            BattleCryTarget,
            Retrieve,
            MagicNon_Target,
            SendHandRequest,
			GameEnd,
			GameEndReceive,
        }

        public int NetworkOrder;
        public MessageType Type;
        public System.Object[] Arguments;

        public NetworkMessage(MessageType type, System.Object[] arguments, int networkOrder = -1)
        {
            Type = type;
            NetworkOrder = networkOrder;
            Arguments = arguments;
        }

        public override string ToString()
        {
            try
            {
                string res = Type.ToString() + ": /";
                for (int i = 0; i < Arguments.Length; i++)
                {
                    res += Arguments[i].ToString() + "/";
                }
                return res;
            }
            catch
            {
                Console.WriteLine("Json ToString Failed");
                return "";
            }

        }
    }


}