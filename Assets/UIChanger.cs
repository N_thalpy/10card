﻿using UnityEngine;
using System.Collections;
using Base;

public class UIChanger : MonoBehaviour {

	public UILabel CardName;

	public UILabel CardCost;
	public UILabel CardGraveCost;

	public UILabel CardAp;
	public UILabel CardHp;

	public UIButton Frame;
	public UI2DSprite Image;

	public UI2DSprite HpOrb;
	public UI2DSprite ManaOrb;


	public FieldObject PlayerField;
	public GameObject SelectionMarker;
	public GameObject ReverseMarker;
	public Card PlayerHand;
	public GameObject Cover;

	public bool ifDeck = false;

	public void LabelChange()
	{
		if (PlayerHand != null) 
		{
			if ( PlayerHand.CardType == CardTypes.monster )
            {
                if (CardAp.text != PlayerHand.LinkedFieldObject.CurrentAp.ToString())
                {
                    CardAp.text = PlayerHand.LinkedFieldObject.CurrentAp.ToString();
                }
                if (CardHp.text != PlayerHand.LinkedFieldObject.CurrentHp.ToString())
                {
                    CardHp.text = PlayerHand.LinkedFieldObject.CurrentHp.ToString();
                }
            }
			else if ( PlayerHand.CardType == CardTypes.magic )
			{
				CardAp.text = "";
				CardHp.text = "";
			}
			if ( CardGraveCost.text != PlayerHand.CurrentGraveCost.ToString() )
			{
				CardGraveCost.text = PlayerHand.CurrentGraveCost.ToString();
				if ( PlayerHand.Recyclable == false ) 
				{
					CardGraveCost.fontSize = 30;
					CardGraveCost.text = "∞";
				}
				else
				{
					CardGraveCost.fontSize = 16;
					CardGraveCost.text = PlayerHand.CurrentGraveCost.ToString();
				}
			}
			if ( CardCost.text != PlayerHand.CurrentCost.ToString() )
			{
				CardCost.text = PlayerHand.CurrentCost.ToString();
			}
			if ( CardName.text != PlayerHand.Name )
			{
				CardName.text = PlayerHand.Name;
			}

			if ( PlayerHand.CardType == CardTypes.empty || PlayerHand.CardType == CardTypes.mystery )
			{
				CardAp.gameObject.SetActive( false );
				CardHp.gameObject.SetActive( false );
				CardGraveCost.gameObject.SetActive( false );
				CardCost.gameObject.SetActive( false );
				CardName.gameObject.SetActive( false );
				HpOrb.gameObject.SetActive( false );
				ManaOrb.gameObject.SetActive( false );
			}

			if ( PlayerHand.CardType != CardTypes.empty )
			{
				CardAp.gameObject.SetActive( true );
				CardHp.gameObject.SetActive( true );
				CardGraveCost.gameObject.SetActive( true );
				CardCost.gameObject.SetActive( true );
				CardName.gameObject.SetActive( true );
				HpOrb.gameObject.SetActive( true );
				ManaOrb.gameObject.SetActive( true );
			}
		}
		if (PlayerField != null) 
		{
			if ( CardAp.text != PlayerField.CurrentAp.ToString() )
			{
				CardAp.text = PlayerField.CurrentAp.ToString();
			}
			if ( CardHp.text != PlayerField.CurrentHp.ToString() )
			{
				CardHp.text = PlayerField.CurrentHp.ToString();
			}
			if ( CardGraveCost.text != PlayerField.CurrentGraveCost.ToString() )
			{
				CardGraveCost.text = PlayerField.CurrentGraveCost.ToString();
				if ( PlayerField.Recyclable == false ) 
				{
					CardGraveCost.fontSize = 30;
					CardGraveCost.text = "∞";
				}
				else
				{
					CardGraveCost.fontSize = 16;
					CardGraveCost.text = PlayerField.CurrentGraveCost.ToString();
				}
			}
			if ( CardCost.text != PlayerField.CurrentCost.ToString() )
			{
				CardCost.text = PlayerField.CurrentCost.ToString();
			}
			if ( CardName.text != PlayerField.Name )
			{
				CardName.text = PlayerField.Name;
			}

			if ( PlayerField.CardType == CardTypes.empty || PlayerField.CardType == CardTypes.mystery )
			{
				CardAp.gameObject.SetActive( false );
				CardHp.gameObject.SetActive( false );
				CardGraveCost.gameObject.SetActive( false );
				CardCost.gameObject.SetActive( false );
				CardName.gameObject.SetActive( false );
				HpOrb.gameObject.SetActive( false );
				ManaOrb.gameObject.SetActive( false );
			}
			if ( PlayerField.CardType != CardTypes.empty && PlayerField.CardType != CardTypes.mystery )
			{
				CardAp.gameObject.SetActive( true );
				CardHp.gameObject.SetActive( true );
				CardGraveCost.gameObject.SetActive( true );
				CardCost.gameObject.SetActive( true );
				CardName.gameObject.SetActive( true );
				HpOrb.gameObject.SetActive( true );
				ManaOrb.gameObject.SetActive( true );
			}
		}
	}


	public void ImageChange()
	{
		UnityEngine.Sprite TempSprite;
		if (PlayerHand != null) 
		{
			if ( PlayerHand.CardType == CardTypes.mystery )
			{
				Cover.SetActive(true);
				return;
			}
			else
			{
				Cover.SetActive(false);
			}
			string ImagePath = PlayerHand.ImagePath;
			if ( ImagePath != Image.name )
			{
				TempSprite = Resources.Load<Sprite> ("Cards/" + ImagePath);
				if ( TempSprite == null )
				{
					TempSprite = Resources.Load<Sprite>("Cards/dummy");
				}
				Image.sprite2D = TempSprite;
			}
			if ( PlayerHand.CardType == CardTypes.empty )
			{
				TempSprite = Resources.Load<Sprite> ("Cards/empty");
				if ( TempSprite == null )
				{
					TempSprite = Resources.Load<Sprite>("Cards/dummy");
				}
				var but = Frame.GetComponent<UIButton>();
				but.normalSprite2D = TempSprite;
				but.hoverSprite2D = TempSprite;
				but.pressedSprite2D = TempSprite;
				but.disabledSprite2D = TempSprite;
			}
			else if ( ifDeck == true )
			{
				TempSprite = Resources.Load<Sprite> ("Cards/wi_blue");
				HpOrb.sprite2D = TempSprite;
				TempSprite = Resources.Load<Sprite> ("Cards/wi_black");
				ManaOrb.sprite2D = TempSprite;
				if ( PlayerHand.CardType == CardTypes.monster )
				{
					if ( PlayerHand.Recyclable == true )
					{
						TempSprite = Resources.Load<Sprite> ("Cards/Light");
						var but = Frame.GetComponent<UIButton>();
						but.normalSprite2D = TempSprite;
						but.hoverSprite2D = TempSprite;
						but.pressedSprite2D = TempSprite;
						but.disabledSprite2D = TempSprite;
					}
					if ( PlayerHand.Recyclable == false )
					{
						TempSprite = Resources.Load<Sprite> ("Cards/Gold");
						var but = Frame.GetComponent<UIButton>();
						but.normalSprite2D = TempSprite;
						but.hoverSprite2D = TempSprite;
						but.pressedSprite2D = TempSprite;
						but.disabledSprite2D = TempSprite;
					}
				}
				else if ( PlayerHand.CardType == CardTypes.magic )
				{
					if ( PlayerHand.Recyclable == true )
					{
						TempSprite = Resources.Load<Sprite> ("Cards/LightMagic");
						var but = Frame.GetComponent<UIButton>();
						but.normalSprite2D = TempSprite;
						but.hoverSprite2D = TempSprite;
						but.pressedSprite2D = TempSprite;
						but.disabledSprite2D = TempSprite;
					}
					if ( PlayerHand.Recyclable == false )
					{
						TempSprite = Resources.Load<Sprite> ("Cards/GoldMagic");
						var but = Frame.GetComponent<UIButton>();
						but.normalSprite2D = TempSprite;
						but.hoverSprite2D = TempSprite;
						but.pressedSprite2D = TempSprite;
						but.disabledSprite2D = TempSprite;
					}
				}
			}
			else if ( ifDeck == false )
			{
				if ( GameAction.IsCardInGrave(PlayerHand) == true )
				{
					TempSprite = Resources.Load<Sprite> ("Cards/dk_blue");
					HpOrb.sprite2D = TempSprite;
					TempSprite = Resources.Load<Sprite> ("Cards/dk_black");
					ManaOrb.sprite2D = TempSprite;
					if ( PlayerHand.CardType == CardTypes.monster )
					{
						TempSprite = Resources.Load<Sprite> ("Cards/Dark");
						var but = Frame.GetComponent<UIButton>();
						but.normalSprite2D = TempSprite;
						but.hoverSprite2D = TempSprite;
						but.pressedSprite2D = TempSprite;
						but.disabledSprite2D = TempSprite;
					}
					else if ( PlayerHand.CardType == CardTypes.magic )
					{
						TempSprite = Resources.Load<Sprite> ("Cards/DarkMagic");
						var but = Frame.GetComponent<UIButton>();
						but.normalSprite2D = TempSprite;
						but.hoverSprite2D = TempSprite;
						but.pressedSprite2D = TempSprite;
						but.disabledSprite2D = TempSprite;
					}
				}
				else
				{
					TempSprite = Resources.Load<Sprite> ("Cards/wi_blue");
					HpOrb.sprite2D = TempSprite;
					TempSprite = Resources.Load<Sprite> ("Cards/wi_black");
					ManaOrb.sprite2D = TempSprite;
					if ( PlayerHand.CardType == CardTypes.monster )
					{
						if ( PlayerHand.Recyclable == true )
						{
							TempSprite = Resources.Load<Sprite> ("Cards/Light");
							var but = Frame.GetComponent<UIButton>();
							but.normalSprite2D = TempSprite;
							but.hoverSprite2D = TempSprite;
							but.pressedSprite2D = TempSprite;
							but.disabledSprite2D = TempSprite;
						}
						if ( PlayerHand.Recyclable == false )
						{
							TempSprite = Resources.Load<Sprite> ("Cards/Gold");
							var but = Frame.GetComponent<UIButton>();
							but.normalSprite2D = TempSprite;
							but.hoverSprite2D = TempSprite;
							but.pressedSprite2D = TempSprite;
							but.disabledSprite2D = TempSprite;
						}
					}
					else if ( PlayerHand.CardType == CardTypes.magic )
					{
						if ( PlayerHand.Recyclable == true )
						{
							TempSprite = Resources.Load<Sprite> ("Cards/LightMagic");
							var but = Frame.GetComponent<UIButton>();
							but.normalSprite2D = TempSprite;
							but.hoverSprite2D = TempSprite;
							but.pressedSprite2D = TempSprite;
							but.disabledSprite2D = TempSprite;
						}
						if ( PlayerHand.Recyclable == false )
						{
							TempSprite = Resources.Load<Sprite> ("Cards/GoldMagic");
							var but = Frame.GetComponent<UIButton>();
							but.normalSprite2D = TempSprite;
							but.hoverSprite2D = TempSprite;
							but.pressedSprite2D = TempSprite;
							but.disabledSprite2D = TempSprite;
						}
					}
				}
			}
		}
		if (PlayerField != null) 
		{
			if ( PlayerField.CardType == CardTypes.mystery )
			{
				Cover.SetActive(true);
				return;
			}
			else
			{
				Cover.SetActive(false);
			}
			string ImagePath = PlayerField.ImagePath;
			if ( ImagePath != Image.name )
			{
				TempSprite = Resources.Load<Sprite> ("Cards/" + ImagePath);
				if ( TempSprite == null )
				{
					TempSprite = Resources.Load<Sprite>("Cards/dummy");
				}
				Image.sprite2D = TempSprite;
			}
			/*if ( ImagePath != Image.normalSprite2D.name )
			{
				TempSprite = Resources.Load<Sprite> ("Cards/" + ImagePath);
				if ( TempSprite == null )
				{
					TempSprite = Resources.Load<Sprite>("Cards/dummy");
				}
				var but = Image.GetComponent<UIButton>();
				but.normalSprite2D = TempSprite;
				but.hoverSprite2D = TempSprite;
				but.pressedSprite2D = TempSprite;
				but.disabledSprite2D = TempSprite;
				return;
			}*/
			if ( PlayerField.CardType == CardTypes.empty )
			{
				TempSprite = Resources.Load<Sprite> ("Cards/empty");
				if ( TempSprite == null )
				{
					TempSprite = Resources.Load<Sprite>("Cards/dummy");
				}
				var but = Frame.GetComponent<UIButton>();
				but.normalSprite2D = TempSprite;
				but.hoverSprite2D = TempSprite;
				but.pressedSprite2D = TempSprite;
				but.disabledSprite2D = TempSprite;
			}
			else
			{
				if ( PlayerField.Recyclable == true )
				{
					TempSprite = Resources.Load<Sprite> ("Cards/Light");
					var but = Frame.GetComponent<UIButton>();
					but.normalSprite2D = TempSprite;
					but.hoverSprite2D = TempSprite;
					but.pressedSprite2D = TempSprite;
					but.disabledSprite2D = TempSprite;
				}
				if ( PlayerField.Recyclable == false )
				{
					TempSprite = Resources.Load<Sprite> ("Cards/Gold");
					var but = Frame.GetComponent<UIButton>();
					but.normalSprite2D = TempSprite;
					but.hoverSprite2D = TempSprite;
					but.pressedSprite2D = TempSprite;
					but.disabledSprite2D = TempSprite;
				}
			}
		}
	}


	void Update()
	{
		ImageChange ();
		LabelChange ();
		
	}
}
