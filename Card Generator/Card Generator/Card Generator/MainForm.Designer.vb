﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblType = New System.Windows.Forms.Label()
        Me.rdbMonster = New System.Windows.Forms.RadioButton()
        Me.rdbMagic = New System.Windows.Forms.RadioButton()
        Me.lblName = New System.Windows.Forms.Label()
        Me.txtCodeName = New System.Windows.Forms.TextBox()
        Me.txtCardName = New System.Windows.Forms.TextBox()
        Me.lblCardName = New System.Windows.Forms.Label()
        Me.lblCost = New System.Windows.Forms.Label()
        Me.numCost = New System.Windows.Forms.NumericUpDown()
        Me.numGraveCost = New System.Windows.Forms.NumericUpDown()
        Me.lblGraveCost = New System.Windows.Forms.Label()
        Me.lblText = New System.Windows.Forms.Label()
        Me.numDef = New System.Windows.Forms.NumericUpDown()
        Me.lblDef = New System.Windows.Forms.Label()
        Me.numAtk = New System.Windows.Forms.NumericUpDown()
        Me.lblAtk = New System.Windows.Forms.Label()
        Me.txtText = New System.Windows.Forms.TextBox()
        Me.btnCreate = New System.Windows.Forms.Button()
        Me.chkIsToken = New System.Windows.Forms.CheckBox()
        Me.chkStakeout = New System.Windows.Forms.CheckBox()
        Me.chkRecyclable = New System.Windows.Forms.CheckBox()
        CType(Me.numCost, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numGraveCost, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numDef, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numAtk, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblType
        '
        Me.lblType.AutoSize = True
        Me.lblType.Font = New System.Drawing.Font("맑은 고딕", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.lblType.Location = New System.Drawing.Point(12, 72)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(80, 21)
        Me.lblType.TabIndex = 0
        Me.lblType.Text = "카드 종류"
        '
        'rdbMonster
        '
        Me.rdbMonster.AutoSize = True
        Me.rdbMonster.Checked = True
        Me.rdbMonster.Font = New System.Drawing.Font("맑은 고딕", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.rdbMonster.Location = New System.Drawing.Point(12, 96)
        Me.rdbMonster.Name = "rdbMonster"
        Me.rdbMonster.Size = New System.Drawing.Size(96, 21)
        Me.rdbMonster.TabIndex = 1
        Me.rdbMonster.TabStop = True
        Me.rdbMonster.Text = "하수인 카드"
        Me.rdbMonster.UseVisualStyleBackColor = True
        '
        'rdbMagic
        '
        Me.rdbMagic.AutoSize = True
        Me.rdbMagic.Font = New System.Drawing.Font("맑은 고딕", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.rdbMagic.Location = New System.Drawing.Point(114, 96)
        Me.rdbMagic.Name = "rdbMagic"
        Me.rdbMagic.Size = New System.Drawing.Size(83, 21)
        Me.rdbMagic.TabIndex = 2
        Me.rdbMagic.Text = "마법 카드"
        Me.rdbMagic.UseVisualStyleBackColor = True
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Font = New System.Drawing.Font("맑은 고딕", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.lblName.Location = New System.Drawing.Point(12, 9)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(160, 21)
        Me.lblName.TabIndex = 3
        Me.lblName.Text = "카드 이름 (코드에서)"
        '
        'txtCodeName
        '
        Me.txtCodeName.Font = New System.Drawing.Font("맑은 고딕", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.txtCodeName.Location = New System.Drawing.Point(12, 33)
        Me.txtCodeName.Name = "txtCodeName"
        Me.txtCodeName.Size = New System.Drawing.Size(176, 25)
        Me.txtCodeName.TabIndex = 4
        '
        'txtCardName
        '
        Me.txtCardName.Font = New System.Drawing.Font("맑은 고딕", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.txtCardName.Location = New System.Drawing.Point(232, 33)
        Me.txtCardName.Name = "txtCardName"
        Me.txtCardName.Size = New System.Drawing.Size(176, 25)
        Me.txtCardName.TabIndex = 6
        '
        'lblCardName
        '
        Me.lblCardName.AutoSize = True
        Me.lblCardName.Font = New System.Drawing.Font("맑은 고딕", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.lblCardName.Location = New System.Drawing.Point(232, 9)
        Me.lblCardName.Name = "lblCardName"
        Me.lblCardName.Size = New System.Drawing.Size(128, 21)
        Me.lblCardName.TabIndex = 5
        Me.lblCardName.Text = "카드 이름 (실제)"
        '
        'lblCost
        '
        Me.lblCost.AutoSize = True
        Me.lblCost.Font = New System.Drawing.Font("맑은 고딕", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.lblCost.Location = New System.Drawing.Point(214, 72)
        Me.lblCost.Name = "lblCost"
        Me.lblCost.Size = New System.Drawing.Size(42, 21)
        Me.lblCost.TabIndex = 7
        Me.lblCost.Text = "비용"
        '
        'numCost
        '
        Me.numCost.Font = New System.Drawing.Font("맑은 고딕", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.numCost.Location = New System.Drawing.Point(218, 96)
        Me.numCost.Name = "numCost"
        Me.numCost.Size = New System.Drawing.Size(61, 25)
        Me.numCost.TabIndex = 8
        '
        'numGraveCost
        '
        Me.numGraveCost.Font = New System.Drawing.Font("맑은 고딕", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.numGraveCost.Location = New System.Drawing.Point(299, 96)
        Me.numGraveCost.Name = "numGraveCost"
        Me.numGraveCost.Size = New System.Drawing.Size(61, 25)
        Me.numGraveCost.TabIndex = 10
        '
        'lblGraveCost
        '
        Me.lblGraveCost.AutoSize = True
        Me.lblGraveCost.Font = New System.Drawing.Font("맑은 고딕", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.lblGraveCost.Location = New System.Drawing.Point(295, 72)
        Me.lblGraveCost.Name = "lblGraveCost"
        Me.lblGraveCost.Size = New System.Drawing.Size(118, 21)
        Me.lblGraveCost.TabIndex = 9
        Me.lblGraveCost.Text = "무덤 회수 비용"
        '
        'lblText
        '
        Me.lblText.AutoSize = True
        Me.lblText.Font = New System.Drawing.Font("맑은 고딕", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.lblText.Location = New System.Drawing.Point(12, 195)
        Me.lblText.Name = "lblText"
        Me.lblText.Size = New System.Drawing.Size(96, 21)
        Me.lblText.TabIndex = 11
        Me.lblText.Text = "카드 텍스트"
        '
        'numDef
        '
        Me.numDef.Font = New System.Drawing.Font("맑은 고딕", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.numDef.Location = New System.Drawing.Point(97, 153)
        Me.numDef.Name = "numDef"
        Me.numDef.Size = New System.Drawing.Size(61, 25)
        Me.numDef.TabIndex = 15
        '
        'lblDef
        '
        Me.lblDef.AutoSize = True
        Me.lblDef.Font = New System.Drawing.Font("맑은 고딕", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.lblDef.Location = New System.Drawing.Point(93, 129)
        Me.lblDef.Name = "lblDef"
        Me.lblDef.Size = New System.Drawing.Size(58, 21)
        Me.lblDef.TabIndex = 14
        Me.lblDef.Text = "방어력"
        '
        'numAtk
        '
        Me.numAtk.Font = New System.Drawing.Font("맑은 고딕", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.numAtk.Location = New System.Drawing.Point(16, 153)
        Me.numAtk.Name = "numAtk"
        Me.numAtk.Size = New System.Drawing.Size(61, 25)
        Me.numAtk.TabIndex = 13
        '
        'lblAtk
        '
        Me.lblAtk.AutoSize = True
        Me.lblAtk.Font = New System.Drawing.Font("맑은 고딕", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.lblAtk.Location = New System.Drawing.Point(12, 129)
        Me.lblAtk.Name = "lblAtk"
        Me.lblAtk.Size = New System.Drawing.Size(58, 21)
        Me.lblAtk.TabIndex = 12
        Me.lblAtk.Text = "공격력"
        '
        'txtText
        '
        Me.txtText.Font = New System.Drawing.Font("맑은 고딕", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.txtText.Location = New System.Drawing.Point(12, 219)
        Me.txtText.Multiline = True
        Me.txtText.Name = "txtText"
        Me.txtText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtText.Size = New System.Drawing.Size(401, 143)
        Me.txtText.TabIndex = 16
        '
        'btnCreate
        '
        Me.btnCreate.Font = New System.Drawing.Font("맑은 고딕", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.btnCreate.Location = New System.Drawing.Point(338, 368)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(75, 29)
        Me.btnCreate.TabIndex = 17
        Me.btnCreate.Text = "생성"
        Me.btnCreate.UseVisualStyleBackColor = True
        '
        'chkIsToken
        '
        Me.chkIsToken.AutoSize = True
        Me.chkIsToken.Font = New System.Drawing.Font("맑은 고딕", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.chkIsToken.Location = New System.Drawing.Point(218, 135)
        Me.chkIsToken.Name = "chkIsToken"
        Me.chkIsToken.Size = New System.Drawing.Size(65, 24)
        Me.chkIsToken.TabIndex = 18
        Me.chkIsToken.Text = "토큰?"
        Me.chkIsToken.UseVisualStyleBackColor = True
        '
        'chkStakeout
        '
        Me.chkStakeout.AutoSize = True
        Me.chkStakeout.Font = New System.Drawing.Font("맑은 고딕", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.chkStakeout.Location = New System.Drawing.Point(289, 135)
        Me.chkStakeout.Name = "chkStakeout"
        Me.chkStakeout.Size = New System.Drawing.Size(65, 24)
        Me.chkStakeout.TabIndex = 19
        Me.chkStakeout.Text = "매복?"
        Me.chkStakeout.UseVisualStyleBackColor = True
        '
        'chkRecyclable
        '
        Me.chkRecyclable.AutoSize = True
        Me.chkRecyclable.Checked = True
        Me.chkRecyclable.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRecyclable.Font = New System.Drawing.Font("맑은 고딕", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(129, Byte))
        Me.chkRecyclable.Location = New System.Drawing.Point(218, 165)
        Me.chkRecyclable.Name = "chkRecyclable"
        Me.chkRecyclable.Size = New System.Drawing.Size(115, 24)
        Me.chkRecyclable.TabIndex = 20
        Me.chkRecyclable.Text = "재활용 가능?"
        Me.chkRecyclable.UseVisualStyleBackColor = True
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(430, 406)
        Me.Controls.Add(Me.chkRecyclable)
        Me.Controls.Add(Me.chkStakeout)
        Me.Controls.Add(Me.chkIsToken)
        Me.Controls.Add(Me.btnCreate)
        Me.Controls.Add(Me.txtText)
        Me.Controls.Add(Me.numDef)
        Me.Controls.Add(Me.lblDef)
        Me.Controls.Add(Me.numAtk)
        Me.Controls.Add(Me.lblAtk)
        Me.Controls.Add(Me.lblText)
        Me.Controls.Add(Me.numGraveCost)
        Me.Controls.Add(Me.lblGraveCost)
        Me.Controls.Add(Me.numCost)
        Me.Controls.Add(Me.lblCost)
        Me.Controls.Add(Me.txtCardName)
        Me.Controls.Add(Me.lblCardName)
        Me.Controls.Add(Me.txtCodeName)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.rdbMagic)
        Me.Controls.Add(Me.rdbMonster)
        Me.Controls.Add(Me.lblType)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "MainForm"
        Me.Text = "카드 생성기"
        CType(Me.numCost, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numGraveCost, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numDef, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numAtk, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblType As System.Windows.Forms.Label
    Friend WithEvents rdbMonster As System.Windows.Forms.RadioButton
    Friend WithEvents rdbMagic As System.Windows.Forms.RadioButton
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtCodeName As System.Windows.Forms.TextBox
    Friend WithEvents txtCardName As System.Windows.Forms.TextBox
    Friend WithEvents lblCardName As System.Windows.Forms.Label
    Friend WithEvents lblCost As System.Windows.Forms.Label
    Friend WithEvents numCost As System.Windows.Forms.NumericUpDown
    Friend WithEvents numGraveCost As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblGraveCost As System.Windows.Forms.Label
    Friend WithEvents lblText As System.Windows.Forms.Label
    Friend WithEvents numDef As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblDef As System.Windows.Forms.Label
    Friend WithEvents numAtk As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblAtk As System.Windows.Forms.Label
    Friend WithEvents txtText As System.Windows.Forms.TextBox
    Friend WithEvents btnCreate As System.Windows.Forms.Button
    Friend WithEvents chkIsToken As System.Windows.Forms.CheckBox
    Friend WithEvents chkStakeout As System.Windows.Forms.CheckBox
    Friend WithEvents chkRecyclable As System.Windows.Forms.CheckBox

End Class
