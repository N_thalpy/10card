﻿Imports System.IO

Public Class MainForm

    Public OutputPath As String

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click

        Dim cardCode As String
        Dim cardWriter As New StreamWriter(OutputPath + txtCodeName.Text + "Card" + If(rdbMonster.Checked And chkIsToken.Checked, "_TOKEN", "") + ".cs")



        If rdbMonster.Checked Then

            cardCode = String.Format(
                "using System;{0}using UnityEngine;{0}using System.Collections;{0}using System.Collections.Generic;{0}{0}namespace Base{0}{{{0}	public class {1}Card : Card{0}	{{{0}{0}        public {1}Card(){0}            : base({2}, {3}, ""{4}"", CardTypes.monster, new {1}Field(), ""{1}"", ""{5}"", {6}, {7}) {0}		{{ {0}{0}		}}{0}{0}{0}	}}{0}}}{0}",
                New Object() {vbCrLf, txtCodeName.Text, numCost.Value, numGraveCost.Value, txtCardName.Text, txtText.Text.Replace("""", "\"""), chkRecyclable.Checked.ToString().ToLower(), chkStakeout.Checked.ToString().ToLower()})

            Dim fieldCode As String = String.Format(
                "using System;{0}using UnityEngine;{0}using System.Collections;{0}using System.Collections.Generic;{0}{0}namespace Base{0}{{{0}	public class {1}Field : FieldObject{0}	{{{0}{0}		public {1}Field(): base({2}, {3}, CardTypes.monster, ""{1}"", ""{4}"", {5}, {6}, {7}, """", {8}) {0}		{{ {0}{0}		}}{0}{0}{0}	}}{0}}}{0}",
                New Object() {vbCrLf, txtCodeName.Text, numCost.Value, numGraveCost.Value, txtCardName.Text, numAtk.Value, numDef.Value, chkIsToken.Checked.ToString().ToLower(), chkRecyclable.Checked.ToString().ToLower()})

            Dim fieldWriter As New StreamWriter(OutputPath + txtCodeName.Text + "Field" + If(chkIsToken.Checked, "_TOKEN", "") + ".cs")
            fieldWriter.Write(fieldCode)
            fieldWriter.Close()

        Else

            cardCode = String.Format(
                "using System;{0}using UnityEngine;{0}using System.Collections;{0}using System.Collections.Generic;{0}{0}namespace Base{0}{{{0}    public class {1}Card : Card{0}    {{{0}        public {1}Card(){0}            : base({2}, {3}, ""{4}"", CardTypes.magic, new MagicBox(), ""{1}"", ""{5}"", {6}, {7}){0}        {{{0}            {0}        }}{0}    }}{0}}}{0}",
                New Object() {vbLf, txtCodeName.Text, numCost.Value, numGraveCost.Value, txtCardName.Text, txtText.Text.Replace("""", "\""")})

        End If

        cardWriter.Write(cardCode)
        cardWriter.Close()

        MsgBox("성공적으로 생성했습니다.", MsgBoxStyle.Information, "생성 완료")

    End Sub

    Private Sub rdbMonster_CheckedChanged(sender As Object, e As EventArgs) Handles rdbMonster.CheckedChanged
        lblAtk.Enabled = rdbMonster.Checked
        lblDef.Enabled = rdbMonster.Checked
        numAtk.Enabled = rdbMonster.Checked
        numDef.Enabled = rdbMonster.Checked
        chkIsToken.Enabled = rdbMonster.Checked
    End Sub

    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim appPath As String = Application.StartupPath
        Dim index As Integer = appPath.LastIndexOf("8card-feat.-unity-4.6")
        If index > 0 Then
            OutputPath = appPath.Substring(0, index + Len("8card-feat.-unity-4.6")) + "\Assets\Scripts\Cards\"
        End If

    End Sub


End Class
