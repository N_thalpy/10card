﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class DummyField : FieldObject
	{
		public DummyField() : base(1, 1, CardTypes.monster, "dummy", "더미" , 1, 1) { }
	}
}
