﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class EmptyCard : Card
    {
        public EmptyCard() : base(0, 1000, "빈 칸", CardTypes.empty, new EmptyField(), "empty") { }
    }
}
