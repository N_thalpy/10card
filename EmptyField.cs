﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
	public class EmptyField : FieldObject
	{
        public EmptyField() : base(0, 1000, CardTypes.empty, "empty", "빈 칸", 0, 0) { }
	}
}
