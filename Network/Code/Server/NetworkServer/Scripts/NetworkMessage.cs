﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace temp_network.Scripts
{
    class NetworkMessage
    {
        public enum MessageType
        {
            PlayerOrder,
            ConnectionCheck,
            SendHand,
            Summon,
            Attack,
            TurnEnd,
            MagicTarget,
            Reverse,
            SkillTarget,
            Retrieve,
            MagicNon_Target,
            ReceivedSendHand
        }

        public int NetworkOrder;
        public MessageType Type;
        public System.Object[] Arguments;

        public NetworkMessage(MessageType Type_, int NetworkOrder_, System.Object[] Arguments_)
        {
            Type = Type_;
            NetworkOrder = NetworkOrder_;
            Arguments = Arguments_;
        }

        public override string ToString()
        {
            try
            {
                string res = Type.ToString() + ": /";
                for (int i = 0; i < Arguments.Length; i++)
                {
                    res += Arguments[i].ToString() + "/";
                }
                return res;
            }
            catch
            {
                Console.WriteLine("Json ToString Failed");
                return "";
            }
            
        }
    }
}
