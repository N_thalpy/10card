﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace temp_network.Scripts
{
    class Protocol
    {
        static JsonSerializer serializer = new JsonSerializer();

        public bool SummonToClient(NetworkMessage stringMsg, int herenumber_, System.IO.StreamWriter OtherWriter)
        {
            try
            {
                serializer.Serialize(OtherWriter, stringMsg);
                OtherWriter.Flush();
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Summon Message Send Successed");
                return true;
            }
            catch
            {
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Summon Message Send Failed");
                return false;
            }
        }

        public bool ReverseToClient(NetworkMessage stringMsg, int herenumber_, System.IO.StreamWriter OtherWriter)
        {
            try
            {
                serializer.Serialize(OtherWriter, stringMsg);
                OtherWriter.Flush();
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Reverse Message Send Successed");
                return true;
            }
            catch
            {
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Reverse Message Send Failed");
                return false;
            }
        }

        public bool AttackToClient(NetworkMessage stringMsg, int herenumber_, System.IO.StreamWriter OtherWriter)
        {
            try
            {
                serializer.Serialize(OtherWriter, stringMsg);
                OtherWriter.Flush();
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Attack Message Send Successed");
                return true;
            }
            catch
            {
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Attack Message Send Failed");
                return false;
            }
        }

        public bool GraveToClient(NetworkMessage stringMsg, int herenumber_, System.IO.StreamWriter OtherWriter)
        {
            try
            {
                serializer.Serialize(OtherWriter, stringMsg);
                OtherWriter.Flush();
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Grave Message Send Successed");
                return true;
            }
            catch
            {
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Grave Message Send Failed");
                return false;
            }
        }

        public bool SpellTargetToClient(NetworkMessage stringMsg, int herenumber_, System.IO.StreamWriter OtherWriter)
        {
            try
            {
                serializer.Serialize(OtherWriter, stringMsg);
                OtherWriter.Flush();
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Spell Target Message Send Successed");
                return true;
            }
            catch
            {
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Spell Target Message Send Failed");
                return false;
            }
        }

        public bool SkillTargetToClient(NetworkMessage stringMsg, int herenumber_, System.IO.StreamWriter OtherWriter)
        {
            try
            {
                serializer.Serialize(OtherWriter, stringMsg);
                OtherWriter.Flush();
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Skill Target Message Send Successed");
                return true;
            }
            catch
            {
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Skill Target Message Send Failed");
                return false;
            }
        }

        public bool MagicNonTargetToClient(NetworkMessage stringMsg, int herenumber_, System.IO.StreamWriter OtherWriter)
        {
            try
            {
                serializer.Serialize(OtherWriter, stringMsg);
                OtherWriter.Flush();
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Magic NonTarget Message Send Successed");
                return true;
            }
            catch
            {
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Magic NonTarget Message Send Failed");
                return false;
            }
        }

        public bool SendHandToClient(NetworkMessage stringMsg, int herenumber_, System.IO.StreamWriter OtherWriter)
        {
            try
            {
                serializer.Serialize(OtherWriter, stringMsg);
                OtherWriter.Flush();
                Console.WriteLine("Room" + herenumber_.ToString() + " 's SendHand Message Send Successed");
                return true;
            }
            catch
            {
                Console.WriteLine("Room" + herenumber_.ToString() + " 's SendHand Message Send Failed");
                return false;
            }
        }

        public bool TurnEndToClient(NetworkMessage stringMsg, int herenumber_, System.IO.StreamWriter OtherWriter)
        {
            try
            {
                serializer.Serialize(OtherWriter, stringMsg);
                OtherWriter.Flush();
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Turn End Message Send Successed");
                return true;
            }
            catch
            {
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Turn End Message Send Failed");
                return false;
            }
        }

        public bool ConnectDataToClient(int herenumber_, System.IO.StreamWriter SoW)
        {
            
            try
            {
                NetworkMessage temp_message = new NetworkMessage(NetworkMessage.MessageType.ConnectionCheck, -1, new Object[0]);
                serializer.Serialize(SoW, temp_message);
                SoW.Flush();
                return true;
            }
            catch
            {
                Console.WriteLine("Room" + herenumber_.ToString() + " 's Connect Data Send Failed");
                return false;
            }
        }

        public bool ReceivedDataToClient(int herenumber_, System.IO.StreamWriter SoW, bool OpponentCheck)
        {
            if(OpponentCheck)
            {
                try
                {
                    NetworkMessage temp_message = new NetworkMessage(NetworkMessage.MessageType.ReceivedSendHand, -1, new Object[1] { true });
                    serializer.Serialize(SoW, temp_message);
                    SoW.Flush();
                    Console.WriteLine("Room" + herenumber_.ToString() + " 's ReceivedSendHand Data Send Successed");
                    return true;
                }
                catch
                {
                    Console.WriteLine("Room" + herenumber_.ToString() + " 's ReceivedSendHand Data Send Failed");
                    return false;
                }
            }
            else
            {
                try
                {
                    NetworkMessage temp_message = new NetworkMessage(NetworkMessage.MessageType.ReceivedSendHand, -1, new Object[1] { false });
                    serializer.Serialize(SoW, temp_message);
                    SoW.Flush();
                    return true;
                }
                catch
                {
                    Console.WriteLine("Room" + herenumber_.ToString() + " 's ReceivedSendHand Data Send Failed");
                    return false;
                }
            }
           
        }
    }
}
