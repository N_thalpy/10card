﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class TESTBlackBomb : Card
    {

        public TESTBlackBomb()
            : base(1, 1, "TESTBlackBomb", CardTypes.magic, null, "TESTBlackBomb")
        {
             /*
            Summon += (FieldObject Unit, Card Me) =>
                {
                    Debug.Log(Unit.Name + " SUMMON!");
                };

            TurnEnd += (_) =>
            {
                Debug.Log("턴 끝이야? 나도 끼어야지!");
            };

            TurnStart += (_) =>
            {
                Debug.Log("턴이 시작됐다더라");
            };
              */
            

            TargetEvent = new TargetingEvent((FieldObject Target, Card Me) =>
                {
                    Debug.Log(Target.Name + "를 목표로 지정했지! 이제 그건 죽을 거야!");
                    Trigger.MainSystem.DestroyFieldObject(Target);

                }
            );
        }

        public override bool TargetValidityCheck(FieldObject Target)
        {
            return (Target.CardType != CardTypes.empty);
        }

    }
}
