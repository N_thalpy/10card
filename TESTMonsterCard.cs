﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class TESTMonsterCard : Card
    {

        public TESTMonsterCard()
            : base(12, 1, "TEST Monster", CardTypes.monster, new TESTMonsterField(), "TESTMonster")
        {
            Summon += (FieldObject Unit, Card Me) =>
                {
                    Debug.Log(Unit.Name + " SUMMON! - 손의 시험용 괴수");
                };

            TurnEnd += (_) =>
            {
                Debug.Log("턴 끝이야? 나도 끼어야지! - 손의 시험용 괴수");
            };

            TurnStart += (_) =>
            {
                Debug.Log("턴이 시작됐구나! - 손의 시험용 괴수");
            };

            SpellTarget += (a, b, _) =>
                {
                    Debug.Log(_.Name + "가 목표가 됐다더라! - 손의 시험용 괴수");
                    return _;
                };

            Move += (UnitMoved, Me, PositionBefore) =>
            {
                Debug.Log(PositionBefore.GetPosition().ToString() + "번 자리로부터 " + UnitMoved.Name + "가 움직였군. - 손의 시험용 괴수");
            };

            Attack += (FieldObject Attacker, FieldObject Defender, Card Me) =>
            {

                Debug.Log(Attacker.Name + "이  " + Defender.Name + "을 공격하고 있구나! - 손의 시험용 괴수");
                
            };

            Defend += (FieldObject Defender, FieldObject Attacker, Card Me) =>
            {

                Debug.Log(Defender.Name + "이  " + Attacker.Name + "을 막아내고 있구나! - 손의 시험용 괴수");
                
            };

            Damaging += (FieldObject DamageGiver, FieldObject DamageTaker, int DamageAmount, Card Me) =>
            {
                Debug.Log("아, " + DamageGiver.Name + "가 " + DamageTaker.Name + "을 " + DamageAmount.ToString() + "만큼의 위력으로 때리고 있군! - 손의 시험용 괴수");
                return DamageAmount;
            };

            Damaged += (FieldObject DamageTaker, FieldObject DamageGiver, int DamageAmount, Card Me) =>
            {
                Debug.Log("아, " + DamageGiver.Name + "가 " + DamageTaker.Name + "을 " + DamageAmount.ToString() + "만큼의 위력으로 때렸구나!! - 손의 시험용 괴수");
                return DamageAmount;
            };

            Death += (Unit, Me) =>
                {
                    Debug.Log(Unit.Name + "의 죽음에 애도를 표합니다. - 손의 시험용 괴수");
                };
        }


    }
}
