﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Base
{
    public class TESTMonsterField : FieldObject
    {

        public TESTMonsterField()
            : base(12, 1, CardTypes.monster, "TESTMonster", "시험용 괴수", 3, 6)
        {
            
            Attack += (FieldObject Attacker, FieldObject Defender, FieldObject Me) =>
                {
                    if (Me == Attacker)
                    {
                        Debug.Log("전장의 시험용 괴수가 " + Defender.Name + "을 공격하고 있다! - 전장의 시험용 괴수");
                    }
                };

            Defend += (FieldObject Defender, FieldObject Attacker, FieldObject Me) =>
            {
                if (Me == Defender)
                {
                    Debug.Log("전장의 시험용 괴수가 " + Attacker.Name + "을 막아내고 있다! - 전장의 시험용 괴수");
                }
            };
             
            TurnEnd += (Me) =>
                {
                    if (Me.GetPosition() != 0)
                        Trigger.MoveTrigger(Me, Me.Controller.Field[0]);
                    Debug.Log("턴이 끝났다 - 전장의 시험용 괴수");
                };

            Move += (UnitMoved, Me, PositionBefore) =>
                {
                    Debug.Log(PositionBefore.GetPosition().ToString() + "번 자리로부터 " + UnitMoved.Name + "가 움직였군. - 전장의 시험용 괴수");
                };

            TurnStart += (_) =>
            {
                Debug.Log("턴이 시작됐다더라 - 전장의 시험용 괴수");
            };

            SpellTarget += (a, b, _) => 
            {
                Debug.Log(_.Name + "가 목표가 됐다더라! - 전장의 시험용 괴수");
                return _;
            };

            Damaging += (FieldObject DamageGiver, FieldObject DamageTaker, int DamageAmount, FieldObject Me) =>
                {
                    if (Me == DamageGiver)
                        Debug.Log("아, 내가 " + DamageTaker.Name + "을 " + DamageAmount.ToString() + "만큼의 위력으로 때리고 있군! - 전장의 시험용 괴수");
                    return DamageAmount;
                };

            Damaged += (_, __, ___, ____) =>
                {
                    if (_ == ____)
                        Debug.Log("아, 내가 " + __.Name + "에게서 " + ___.ToString() + "만큼의 피해를 받았다! - 전장의 시험용 괴수");
                    return ___;
                };
        }


    }
}
